<?php

/**
 * Do not edit anything in this file unless you know what you're doing
 */

use Roots\Sage\Config;
use Roots\Sage\Container;

/**
 * Helper function for prettying up errors
 * @param string $message
 * @param string $subtitle
 * @param string $title
 */
$sage_error = function ($message, $subtitle = '', $title = '') {
    $title = $title ?: __('Sage &rsaquo; Error', 'sage');
    $footer = '<a href="https://roots.io/sage/docs/">roots.io/sage/docs/</a>';
    $message = "<h1>{$title}<br><small>{$subtitle}</small></h1><p>{$message}</p><p>{$footer}</p>";
    wp_die($message, $title);
};

/**
 * Ensure compatible version of PHP is used
 */
if (version_compare('7.1', phpversion(), '>=')) {
    $sage_error(__('You must be using PHP 7.1 or greater.', 'sage'), __('Invalid PHP version', 'sage'));
}

/**
 * Ensure compatible version of WordPress is used
 */
if (version_compare('4.7.0', get_bloginfo('version'), '>=')) {
    $sage_error(__('You must be using WordPress 4.7.0 or greater.', 'sage'), __('Invalid WordPress version', 'sage'));
}

/**
 * Ensure dependencies are loaded
 */
if (!class_exists('Roots\\Sage\\Container')) {
    if (!file_exists($composer = __DIR__.'/../vendor/autoload.php')) {
        $sage_error(
            __('You must run <code>composer install</code> from the Sage directory.', 'sage'),
            __('Autoloader not found.', 'sage')
        );
    }
    require_once $composer;
}

/**
 * Sage required files
 *
 * The mapped array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 */
array_map(function ($file) use ($sage_error) {
    $file = "../app/{$file}.php";
    if (!locate_template($file, true, true)) {
        $sage_error(sprintf(__('Error locating <code>%s</code> for inclusion.', 'sage'), $file), 'File not found');
    }
}, ['helpers', 'setup', 'filters', 'admin']);

/**
 * Here's what's happening with these hooks:
 * 1. WordPress initially detects theme in themes/sage/resources
 * 2. Upon activation, we tell WordPress that the theme is actually in themes/sage/resources/views
 * 3. When we call get_template_directory() or get_template_directory_uri(), we point it back to themes/sage/resources
 *
 * We do this so that the Template Hierarchy will look in themes/sage/resources/views for core WordPress themes
 * But functions.php, style.css, and index.php are all still located in themes/sage/resources
 *
 * This is not compatible with the WordPress Customizer theme preview prior to theme activation
 *
 * get_template_directory()   -> /srv/www/example.com/current/web/app/themes/sage/resources
 * get_stylesheet_directory() -> /srv/www/example.com/current/web/app/themes/sage/resources
 * locate_template()
 * ├── STYLESHEETPATH         -> /srv/www/example.com/current/web/app/themes/sage/resources/views
 * └── TEMPLATEPATH           -> /srv/www/example.com/current/web/app/themes/sage/resources
 */
array_map(
    'add_filter',
    ['theme_file_path', 'theme_file_uri', 'parent_theme_file_path', 'parent_theme_file_uri'],
    array_fill(0, 4, 'dirname')
);
Container::getInstance()
    ->bindIf('config', function () {
        return new Config([
            'assets' => require dirname(__DIR__).'/config/assets.php',
            'theme' => require dirname(__DIR__).'/config/theme.php',
            'view' => require dirname(__DIR__).'/config/view.php',
        ]);
}, true);

add_action( 'admin_menu', 'company_info_menu' );  

function company_info_menu(){    
  $page_title = 'Company Info';   
  $menu_title = 'Company Info';   
  $capability = 'manage_options';   
  $menu_slug  = 'company-info';   
  $function   = 'company_info_page';  
  $icon_url   = 'dashicons-admin-home';   
  $position   = 4;    add_menu_page( $page_title, $menu_title, $capability, $menu_slug,$function, $icon_url,$position ); 
} 

if( !function_exists("company_info_page") ) { 
  function company_info_page(){ ?>
    <h1 style="font-weight:400; margin: 2em 0 1em;">Website Information</h1>
    <form method="post" action="options.php">
      <?php settings_fields( 'company-info-settings' ); ?>
      <?php do_settings_sections( 'company-info-settings' ); ?>
          <table class="form-table">
            <tbody style="width: 95%; display: flex; flex-wrap: wrap;">
              <tr class="col-one" valign="top">
                <th scope="row" style="width: 100%; padding-bottom: 0;">Google Analytics Key</th>
                <td style="width: 100%; padding: 5px 0px 15px 0px;">
                  <input placeholder="Google Analytics Key" style="width: 100%; height: 3.5em;" type="text" name="company_info_google_analytics_key" value="<?php echo get_option( 'company_info_google_analytics_key' ); ?>"/>
                  <p><?php echo 'get_option("company_info_google_analytics_key")'; ?></p>
                </td>
              </tr>
              <!-- <tr class="col-one" valign="top">
                <th scope="row" style="width: 100%; padding-bottom: 0;">Google API Key</th>
                <td style="width: 100%; padding: 5px 0px 15px 0px;">
                  <input placeholder="Google API Key" style="width: 100%; height: 3.5em;" type="text" name="company_info_google_api_key" value="<?php // echo get_option( 'company_info_google_api_key' ); ?>"/>
                  <p><?php // echo 'get_option("company_info_google_api_key")'; ?></p>
                </td>
              </tr> -->
               <tr class="col-one" valign="top">
                <th scope="row" style="width: 100%; padding-bottom: 0;">Typekit Kit ID</th>
                <td style="width: 100%; padding: 5px 0px 15px 0px;">
                  <input placeholder="Typekit Kit ID" style="width: 100%; height: 3.5em;" type="text" name="company_info_typekit_id" value="<?php echo get_option( 'company_info_typekit_id' ); ?>"/>
                  <p><?php echo 'get_option("company_info_typekit_id")'; ?></p>
                </td>
              </tr>
              <tr class="col-one company_info" valign="top">
                <th scope="row" style="width: 100%; font-size:2em; font-weight: 400; margin-bottom:0.5em; padding-bottom: 0;">Company Information</th>
              </tr>
              <!-- <tr class="col-one" valign="top">
                <th scope="row" style="width: 100%; padding-bottom: 0;">Company Name</th>
                <td style="width: 100%; padding: 5px 0px 15px 0px;">
                  <input placeholder="Company Name" style="width: 100%; height: 3.5em;" type="text" name="company_info_name" value="<?php //echo get_option( 'company_info_name' ); ?>"/>
                  <p><?php // echo 'get_option("company_info_name")'; ?></p>
                </td>
              </tr> -->
              <tr valign="top">
                <th scope="row" style="width: 100%; padding-bottom: 0;">Company Address One</th>
                <td style="width: 100%;  padding:5px 0px 15px 0px;">
                  <input placeholder="Company Address One" style="width: 100%; height: 3.5em;" type="text" name="company_info_address" value="<?php echo get_option( 'company_info_address' ); ?>"/>
                  <p><?php echo 'get_option("company_info_address")'; ?></p>
                </td>
              </tr>
              <tr valign="top">
                <th scope="row" style="width: 100%; padding-bottom: 0;">Company Address Two</th>
                <td style="width: 100%;  padding:5px 0px 15px 0px;">
                  <input placeholder="Company Address Two" style="width: 100%; height: 3.5em;" type="text" name="company_info_address_two" value="<?php echo get_option( 'company_info_address_two' ); ?>"/>
                  <p><?php echo 'get_option("company_info_address_two")'; ?></p>
                </td>
              </tr>
              <tr valign="top">
                <th scope="row" style="width: 100%; padding-bottom: 0;">Company City</th>
                <td style="width: 100%;  padding:5px 0px 15px 0px;">
                  <input placeholder="Company City" style="width: 100%; height: 3.5em;" type="text" name="company_info_city" value="<?php echo get_option( 'company_info_city' ); ?>"/>
                  <p><?php echo 'get_option("company_info_city")'; ?></p>
                </td>
              </tr>
              <tr valign="top">
                <th scope="row" style="width: 100%; padding-bottom: 0;">Company Postal Code</th>
                <td style="width: 100%;  padding:5px 0px 15px 0px;">
                  <input placeholder="Company Postal Code" style="width: 100%; height: 3.5em;" type="text" name="company_info_postal" value="<?php echo get_option( 'company_info_postal' ); ?>"/>
                  <p><?php echo 'get_option("company_info_postal")'; ?></p>
                </td>
              </tr>
              <tr valign="top">
                <th scope="row" style="width: 100%; padding-bottom: 0;">Company Province</th>
                <td style="width: 100%;  padding:5px 0px 15px 0px;">
                  <input placeholder="Company Province" style="width: 100%; height: 3.5em;" type="text" name="company_info_province" value="<?php echo get_option( 'company_info_province' ); ?>"/>
                  <p><?php echo 'get_option("company_info_province")'; ?></p>
                </td>
              </tr>
              <tr valign="top">
                <th scope="row" style="width: 100%; padding-bottom: 0;">Company Country</th>
                <td style="width: 100%;  padding:5px 0px 15px 0px;">
                  <input placeholder="Company Country" style="width: 100%; height: 3.5em;" type="text" name="company_info_country" value="<?php echo get_option( 'company_info_country' ); ?>"/>
                  <p><?php echo 'get_option("company_info_country")'; ?></p>
                </td>
              </tr>
              <tr valign="top">
                <th scope="row" style="width: 100%; padding-bottom: 0;">Company Phone Number</th>
                <td style="width: 100%;  padding:5px 0px 15px 0px;">
                  <input placeholder="Company Phone Number" style="width: 100%; height: 3.5em;" type="text" name="company_info_phone" value="<?php echo get_option( 'company_info_phone' ); ?>"/>
                  <p><?php echo 'get_option("company_info_phone")'; ?></p>
                </td>
              </tr>
              <tr valign="top">
                <th scope="row" style="width: 100%; padding-bottom: 0;">Company Fax Number</th>
                <td style="width: 100%;  padding:5px 0px 15px 0px;">
                  <input placeholder="Company Fax Number" style="width: 100%; height: 3.5em;" type="text" name="company_info_fax" value="<?php echo get_option( 'company_info_fax' ); ?>"/>
                  <p><?php echo 'get_option("company_info_fax")'; ?></p>
                </td>
              </tr> 
              <tr valign="top">
                <th scope="row" style="width: 100%; padding-bottom: 0;">Company Toll Free</th>
                <td style="width: 100%;  padding:5px 0px 15px 0px;">
                  <input placeholder="Company Toll Free" style="width: 100%; height: 3.5em;" type="text" name="company_info_tollfree" value="<?php echo get_option( 'company_info_tollfree' ); ?>"/>
                  <p><?php echo 'get_option("company_info_tollfree")'; ?></p>
                </td>
              </tr> 
              <tr valign="top">
                <th scope="row" style="width: 100%; padding-bottom: 0;">Company Email Address</th>
                <td style="width: 100%;  padding:5px 0px 15px 0px;">
                  <input placeholder="Company Email Address" style="width: 100%; height: 3.5em;" type="text" name="company_info_email" value="<?php echo get_option( 'company_info_email' ); ?>"/>
                  <p><?php echo 'get_option("company_info_email")'; ?></p>
                </td>
              </tr>

               <tr valign="top">
                <th scope="row" style="width: 100%; padding-bottom: 0;">Company Mailing Address</th>
                <td style="width: 100%;  padding:5px 0px 15px 0px;">
                  <input placeholder="Company Mailing Address" style="width: 100%; height: 3.5em;" type="text" name="company_info_mailing_address" value="<?php echo get_option( 'company_info_mailing_address' ); ?>"/>
                  <p><?php echo 'get_option("company_info_mailing_address")'; ?></p>
                </td>
              </tr>
               <tr valign="top">
                <th scope="row" style="width: 100%; padding-bottom: 0;">Company Mailing City</th>
                <td style="width: 100%;  padding:5px 0px 15px 0px;">
                  <input placeholder="Company Mailing City" style="width: 100%; height: 3.5em;" type="text" name="company_info_mailing_city" value="<?php echo get_option( 'company_info_mailing_province' ); ?>"/>
                  <p><?php echo 'get_option("company_info_mailing_city")'; ?></p>
                </td>
              </tr>
               <tr valign="top">
                <th scope="row" style="width: 100%; padding-bottom: 0;">Company Mailing Province</th>
                <td style="width: 100%;  padding:5px 0px 15px 0px;">
                  <input placeholder="Company Mailing Province" style="width: 100%; height: 3.5em;" type="text" name="company_info_mailing_province" value="<?php echo get_option( 'company_info_mailing_province' ); ?>"/>
                  <p><?php echo 'get_option("company_info_mailing_province")'; ?></p>
                </td>
              </tr>
               <tr valign="top">
                <th scope="row" style="width: 100%; padding-bottom: 0;">Company Mailing Postal Code</th>
                <td style="width: 100%;  padding:5px 0px 15px 0px;">
                  <input placeholder="Company Mailing Postal Code" style="width: 100%; height: 3.5em;" type="text" name="company_info_mailing_postal" value="<?php echo get_option( 'company_info_mailing_postal' ); ?>"/>
                  <p><?php echo 'get_option("company_info_mailing_postal")'; ?></p>
                </td>
              </tr>
            </tbody>
          </table>
      <?php submit_button(); ?>
    </form>
  <?php }
}

// add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );
add_action( 'admin_init', 'update_company_info' );

if( !function_exists("update_company_info") ) { 
  function update_company_info() {   
    register_setting( 'company-info-settings', 'company_info_google_api_key' ); 
    register_setting( 'company-info-settings', 'company_info_google_analytics_key' ); 
    register_setting( 'company-info-settings', 'company_info_typekit_id' ); 
    register_setting( 'company-info-settings', 'company_info_name' ); 
    register_setting( 'company-info-settings', 'company_info_address' ); 
    register_setting( 'company-info-settings', 'company_info_address_two' ); 
    register_setting( 'company-info-settings', 'company_info_city' ); 
    register_setting( 'company-info-settings', 'company_info_postal' ); 
    register_setting( 'company-info-settings', 'company_info_country' ); 
    register_setting( 'company-info-settings', 'company_info_province' ); 
    register_setting( 'company-info-settings', 'company_info_phone' ); 
    register_setting( 'company-info-settings', 'company_info_fax' ); 
    register_setting( 'company-info-settings', 'company_info_tollfree' ); 
    register_setting( 'company-info-settings', 'company_info_email' ); 
    register_setting( 'company-info-settings', 'company_info_mailing_address' ); 
    register_setting( 'company-info-settings', 'company_info_mailing_city' ); 
    register_setting( 'company-info-settings', 'company_info_mailing_province' ); 
    register_setting( 'company-info-settings', 'company_info_mailing_postal' ); 
  } 
}

if( !function_exists("company_info") ) {    
function company_info($content)   {     
$extra_info = get_option( 'company_info' );     
return $content . $extra_info;   }  
add_filter( 'the_content', 'company_info' );  }

add_action('admin_head', 'my_admin_styles');

function my_admin_styles() {
   echo 
   '<style>
        .toplevel_page_company-info .form-table tr {
            margin: 0 0.5%;
            flex: 0 0 32%;
            display: flex;
            flex-wrap: wrap;
        }
        @media(min-width:1300px){
            .toplevel_page_company-info .form-table tr {
                flex: 0 0 24%;
            }
        }
        .toplevel_page_company-info .form-table tr.col-one {
            margin: 0 0.5%;
            flex: 0 0 49%;
        }
        .toplevel_page_company-info .form-table tr input{
            height:auto !important;
            padding:1.1em 1em;
        }
        .toplevel_page_company-info .form-table tr th{
             display: none;
         }
        .toplevel_page_company-info .form-table tr td{
            padding:1.2em; 
            margin-bottom:0;
        }
        .toplevel_page_company-info .form-table tr td p{
            font-size: 0.6em;
            text-align: center;
            background: lightgray;
        }
    </style>';
}

function copyright() {
?>

  <div class="copyright">
      <p>
        Copyright &copy;<?= date('Y'); ?>

      <?php

        if(!empty(get_option('company_info_company'))) {
          echo "<span>".stripslashes(get_option('company_info_company')).".</span> ";
        } else {
          echo "<span>".get_bloginfo('name').".</span> ";
        }

      ?>
      <!-- <span>All rights reserved.</span> -->
      </p>

  </div><!--/.copyright-->

<?php
}

function privacy() {
    if (has_nav_menu('footer_navigation')) :
      wp_nav_menu(['theme_location' => 'footer_navigation', 'menu_class' => 'menu-footer nolist']);
    endif;
}

add_action('footer_credit', __NAMESPACE__ .'\\credit');
function credit() {
?>
  <div class="text-right jac-tech">
    <a href="http://jac.co" rel="external" title="JAC. We Create." id="jac">
      <?= @file_get_contents(get_template_directory()."/assets/images/by-jac.svg"); ?>
    </a>
  </div><!-- /.jac-tech -->
<?php
}

function update_custom_terms($post_id) {

  // only update terms if it's a project post
  if ( 'page' != get_post_type($post_id)) {
    return;
  }

  // don't create or update terms for system generated posts
  if (get_post_status($post_id) == 'auto-draft') {
    return;
  }
    
  /*
  * Grab the post title and slug to use as the new 
  * or updated term name and slug
  */
  $term_title = get_the_title($post_id);
  $term_slug = get_post( $post_id )->post_name;

  /*
  * Check if a corresponding term already exists by comparing 
  * the post ID to all existing term descriptions. 
  */
  $existing_terms = get_terms('category', array(
    'hide_empty' => false,
    'post_type' => 'slide',
    )
  );

  foreach($existing_terms as $term) {
    if ($term->description == $post_id) {
      //term already exists, so update it and we're done
      wp_update_term($term->term_id, 'category', array(
        'name' => $term_title,
        'post_type' => 'slide',
        'slug' => $term_slug
        )
      );
      return;
    }
  }

  /* 
  * If we didn't find a match above, this is a new post, 
  * so create a new term.
  */
  wp_insert_term($term_title, 'category', array(
    'slug' => $term_slug,
    'post_type' => 'slide',
    // 'description' => $post_id
    )
  );
}

//run the update function whenever a post is created or edited
add_action('save_post', 'update_custom_terms');

function popup_search() {
    if(get_field('search_modal_colour','options')!="") {
      $searchcolor = get_field('search_modal_colour','options');
    } 
    if(get_field('search_modal_opacity','options')!="") {
      $searchopacity = get_field('search_modal_opacity','options');
    }
  ?>
  <div class="popup-wrap popup-wrap-search">
    <div class="close-bg js-popup-search" style="background-color: <?= $searchcolor?>; opacity: <?= $searchopacity?>;"></div>
    <div class="popup-modal">
      <h2>Search
        <?php
          if(!empty(get_option('company_info_name'))) {
            echo "<span>".stripslashes(get_option('company_info_name'))."</span> ";
          } else {
            echo "<span>".get_bloginfo('name')."</span> ";
          }
        ?>
      </h2>
      <?php get_search_form(); ?>
      <div class="popup-cancel js-popup-search">
        <p>Cancel</p>
      </div>
    </div><!--/.popup-modal-->
  </div><!--/.popup-wrap-search-->
<?php } // popup_search

add_action( 'wp_footer', 'popup_search' );

function get_id_by_slug($page_slug) {
    $page = get_page_by_path($page_slug);
    if ($page) {
        return $page->ID;
    } else {
        return null;
    }
} 

/**
 * menu order quick change
 */
//ajax

// column
add_action('manage_page_posts_custom_column', __NAMESPACE__ . '\\manage_custom_column', 10, 2);
function manage_custom_column( $column_name, $post_id ) {
  $post = get_post($post_id);
  switch ($column_name) {
    case 'image':
      $post_thumbnail_id = get_post_thumbnail_id($post_id);
      if ($post_thumbnail_id) {
        $post_thumbnail_img = wp_get_attachment_image_src($post_thumbnail_id);
        $post_full_img = wp_get_attachment_image_src($post_thumbnail_id, 'full');
        echo '<a href="' . $post_full_img[0] . '" class="js-popup"><img src="' . $post_thumbnail_img[0] . '" /></a>';
      }
      break;
    case 'order' :
      echo $post->menu_order;
      break;
  }
}
// add image and order columns to page
add_filter('manage_pages_columns', function ($posts_columns) {
  return array_merge(array_splice( $posts_columns, 0, 1 ), ['image' => ''], $posts_columns, ['order' => 'Order']);
});

// Update CSS within in Admin
function admin_style() {
  wp_enqueue_style('admin-styles', get_template_directory_uri().'/admin.css');
}

add_action('admin_enqueue_scripts', 'admin_style');

add_action('wp_head','primary_color');
function primary_color() {
    if (!empty(get_field('primary_colour','options'))) {
      $primaryColor = get_field('primary_colour','options');
    } else {
      $primaryColor = "#ac2b28;";
    }
    if (!empty(get_field('secondary_colour','options'))) {
      $secondaryColor = get_field('secondary_colour','options');
    } else {
      $secondaryColor = "#9b9b9b;";
    }
    if (!empty(get_field('tertiary_colour','options'))) {
      $tertiaryColor = get_field('tertiary_colour','options');
    } else {
      $tertiaryColor = "#ac2b28;";
    }
  ?>

  <style type="text/css">

    .primary { 
      color: <?php echo $primaryColor; ?>; 
    }
    
    .sub-menu,
    .bg-primary { 
      background-color: <?php echo $primaryColor; ?>; 
    }
    .border-primary { 
      border-color: <?php echo $primaryColor; ?>; 
    }
    .menu-fixed #menu-primary .menu-item-has-children:hover, 
    .hover-primary:hover { 
      color: <?php echo $primaryColor; ?>; 
    }
    .hover-bg-primary:hover { 
      background-color: <?php echo $primaryColor; ?>; 
    }
    
    footer .copyright li a:after,
    .menu-fixed .topbar li a:after,
    .menu-fixed .bottombar .menu-item-has-children .sub-menu li a:after,
    .menu-fixed .bottombar .menu-item a:before{
      background-color: white;
    }
    .slick-dots li{
      border-color: <?php echo $primaryColor; ?>;
      background-color: transparent;
    }
    .slick-dots .slick-active{
      border-color: <?php echo $primaryColor; ?>;
      background-color: <?php echo $primaryColor; ?>;
    }

    .gform_wrapper form .gform_footer input{
      border-color: <?php echo $primaryColor; ?> !important;
      background-color: <?php echo $primaryColor; ?> !important;
    }

    .gform_wrapper form .gform_footer input:hover{
      background-color: white !important;
      color: <?php echo $primaryColor; ?> !important;
    }

    .footer-top .nav-mid ul li a:hover,
    .slick-arrow:before{
      color: <?php echo $primaryColor; ?>;
    }

    .secondary { color: <?php echo $secondaryColor; ?>; }
    .bg-secondary { background-color: <?php echo $secondaryColor; ?>; }
    .border-secondary { border-color: <?php echo $secondaryColor; ?>; }

    .tertiary { color: <?php echo $tertiaryColor; ?>; }
    .bg-tertiary { background-color: <?php echo $tertiaryColor; ?>; }
    .border-tertiary { border-color: <?php echo $tertiaryColor; ?>; }

    .fancybox-button:not([disabled])>div,
    .button.primary {
      border-color: <?php echo $primaryColor; ?>;
      background-color: <?php echo $primaryColor; ?>;
      color: white;
    }

    .button.secondary {
      border-color: <?php echo $secondaryColor; ?>;
      background-color: <?php echo $secondaryColor; ?>;
      color: white;
    }

    .button.tertiary {
      border-color: <?php echo $tertiaryColor; ?>;
      background-color: <?php echo $tertiaryColor; ?>;
      color: white;
    }

    .button.primary:hover, 
    .button.primary:hover span,
    .button.primary:focus {
      background: transparent;
      color: <?php echo $primaryColor; ?>;
      border-color: <?php echo $primaryColor; ?>;
    }

    .button.secondary:hover, 
    .button.secondary:hover span,
    .button.secondary:focus{
      background: transparent;
      color: <?php echo $secondaryColor; ?>;
      border-color: <?php echo $secondaryColor; ?>;
    } 

    .button.tertiary:hover, 
    .button.tertiary:hover span,
    .button.tertiary:focus{
      background: transparent;
      color: <?php echo $tertiaryColor; ?>;
      border-color: <?php echo $tertiaryColor; ?>;
    }
  </style>

  <?php
}