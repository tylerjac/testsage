/*eslint no-mixed-spaces-and-tabs: */
export default {
  init() {
    // JavaScript to be fired on all pages
    if ($('.padding_on_menu').length) {
      var $fixedheight = $('.menu-fixed').height();
      $('.wrapper').css('padding-top', $fixedheight + 'px');
      var heightpx = $fixedheight + 'px';
      $('.padding_on_menu').css('height', 'calc(100vh - ' + heightpx + ')');
      $(window).on('resize', function () {
        var $fixedheight = $('.menu-fixed').height();
        $('.wrapper').css('padding-top', $fixedheight + 'px');
        var heightpx = $fixedheight + 'px';
        $('.padding_on_menu').css('height', 'calc(100vh - ' + heightpx + ')');
      });
    }

    if ($('.padding_on_menu_breadcrumb').length) {
      var $fixedheightbreadcrumb = $('.menu-fixed').height();
      $('.wrapper').css('padding-top', $fixedheightbreadcrumb + 'px');
      $(window).on('resize', function () {
        var $fixedheightbreadcrumb = $('.menu-fixed').height();
        $('.wrapper').css('padding-top', $fixedheightbreadcrumb + 'px');
      });
    }

    if ($('.video-popup-checker').length) {
      $('#myModal').css('display', 'flex');
      $('body').css('overflow', 'hidden');
    }
    var num = 125; //number of pixels before modifying styles
    $(window).bind('scroll', function () {
      if ($(window).scrollTop() > num) {
        $('body').addClass('body-scrolled');
      } else {
        $('body').removeClass('body-scrolled');
      }
    });

    $('.dropdown-menu').removeClass('slider-dropdown');
    $('.menu-side .bar-container').click(function () {
      $('.dropdown-menu').toggleClass('slider-dropdown');
      $('.holder-dropdown').toggleClass('show');
      $(this).toggleClass('open-dropdown');
    });

    $('.holder-dropdown .button-close').click(function () {
    $('.dropdown-menu').removeClass('slider-dropdown');
    $('.holder-dropdown').removeClass('show');
    $('.menu-side .bar-container').removeClass('open-dropdown');
    });

    jQuery('#down').click(function() {
        jQuery('html, body').animate({
          scrollTop: jQuery('.content-section').offset().top,
        }, 1000);
    });

    // $('.sub-arrow').click(function () {
    //   $(this).find('i').toggleClass('turn-arrow');
    //   $(this).siblings('.sub-pages').toggleClass('tap');
    // });

    // SLICK HEROES

    // Load some Google Fonts asynchrously 
    // Typekit Web Font Loader https://github.com/typekit/webfontloader// https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js loads latest 1.x version
    window.WebFontConfig = {  google: { 
                                families: [ 
                                  'Raleway:300,400,400i,700',   // Add your fonts here
                                  'Open+Sans:300,400,400i,700', // Add your fonts here
                                ],
                              },
                           };
    (function() {  var wf = document.createElement('script');  
      wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';  
      wf.type = 'text/javascript';  wf.async = 'true';  
      var s = document.getElementsByTagName('script')[0];  
      s.parentNode.insertBefore(wf, s);
    })();
  },
  finalize: function () {
    // JavaScript to be fired on all pages, after page specific JS is fired

    var $body = $('body');
    // var $fixedheight = $('.menu-fixed').height();
    // $('.wrapper').css('padding-top', $fixedheight + 'px');
    // var heightpx = $fixedheight + 'px';
    // $('.banner-image').css('height', 'calc(100vh - ' + heightpx + ')');
    //Toggle Filter Options

     if ($('.padding_on_menu').length) {
      var $fixedheight = $('.menu-fixed').height();
      $('.wrapper').css('padding-top', $fixedheight + 'px');
      var heightpx = $fixedheight + 'px';
      $('.padding_on_menu').css('height', 'calc(100vh - ' + heightpx + ')');
      $(window).on('resize', function () {
        var $fixedheight = $('.menu-fixed').height();
        $('.wrapper').css('padding-top', $fixedheight + 'px');
        var heightpx = $fixedheight + 'px';
        $('.padding_on_menu').css('height', 'calc(100vh - ' + heightpx + ')');
      });
    }

    if ($('.padding_on_menu_breadcrumb').length) {
      var $fixedheightbreadcrumb = $('.menu-fixed').height();
      $('.wrapper').css('padding-top', $fixedheightbreadcrumb + 'px');
      $(window).on('resize', function () {
        var $fixedheightbreadcrumb = $('.menu-fixed').height();
        $('.wrapper').css('padding-top', $fixedheightbreadcrumb + 'px');
      });
    }

    $('.js-filter-expand').click(function () {
      $('.js-filter-expander').toggleClass('expanded');
    });

    //Toggle menu
    $('.js-menu-open').click(function () {
      $('body').toggleClass('menu-off-canvas-active');
      $body.removeClass('fb-messenger-active');
      $body.removeClass('popup-active-feedback');
    });

    $('.js-menu-open-left').click(function () {
      $body.toggleClass('menu-off-canvas-active');
      $body.toggleClass('left');
      $body.removeClass('fb-messenger-active');
      $body.removeClass('popup-active-feedback');
    });

    //Toggle Feedback
    $('.js-popup-feedback').click(function () {
      $body.toggleClass('popup-active-feedback');
      $body.removeClass('fb-messenger-active');
    });

    //Toggle FB Messenger
    $('.js-fb-messenger').click(function () {
      $body.toggleClass('fb-messenger-active');
      $body.removeClass('menu-off-canvas-active');
    });

    //Toggle Search
    $('.js-popup-search').click(function (e) {
      e.preventDefault();
      $('.popup-wrap-search input').focus();
      $body.toggleClass('popup-active-search');
    });

  
    $('.global-modal').addClass('active');
    if($('.global-modal.active')[0]) {
      $('body').css('overflow', 'hidden');
    }

    $('.global-close').click(function () {
      $('.global-modal').removeClass('active');
      $('body').css('overflow', 'auto');
    });
    
    $(document).keyup(function (e) {
      if (e.keyCode === 27) {
        // escape key maps to keycode `27`
        $body.removeClass(
          'body-is-scrolled menu-off-canvas-active popup-active-search fb-messenger-active popup-active-feedback left'
        );
      }
    });

    //Toggle accordion links
    $(document).on('click', '.js-accordion-title', function (e) {
      e.preventDefault();
      $(this)
        .closest('.js-accordion')
        .toggleClass('expanded')
        .siblings()
        .removeClass('expanded');
    });

    $('#awards-slider').slick({
      mobileFirst: true,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
      arrows: false,
      dots: true,
      draggable: true,
      adaptiveHeight: true,
      pauseOnHover: true,
      responsive: [
        {
          breakpoint: 1250,
          settings: {
            slidesToShow: 4,
          },
        },
        {
          breakpoint: 1000,
          settings: {
            slidesToShow: 3,
          },
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
          },
        },
        {
          breakpoint: 100,
          settings: {
            slidesToShow: 1,
          },
        },
      ],
    });

    $('#arrowSlider').slick({
      mobileFirst: true,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      prevArrow:'<span class="slick-prev"><i class="fas fa-chevron-left"></i></span>',
      nextArrow:'<span class="slick-next"><i class="fas fa-chevron-right"></i></span>',
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 3,
            dots: false,
            arrows: true,
          },
        },
        {
          breakpoint: 1100,
          settings: {
            slidesToShow: 2,
            dots: false,
            arrows: true,
          },
        },
        {
          breakpoint: 800,
          settings: {
            slidesToShow: 2,
            dots: false,
            arrows: true,
          },
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            dots: true,
            arrows: false,
          },
        },
        {
          breakpoint: 0,
          settings: {
            dots: true,
            arrows: false,
            slidesToShow: 1,
          },
        },
      ],
    });

    $('[data-fancybox]').fancybox();

    $('#js-slider-header').slick({
      mobileFirst: true,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      prevArrow:'<span class="slick-prev"><i class="fas fa-chevron-left"></i></span>',
      nextArrow: '<span class="slick-next"><i class="fas fa-chevron-right"></i></span>',
      dots: true,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            dots: true,
            arrows: true,
          },
        },
        {
          breakpoint: 600,
          settings: {
            dots: true,
            arrows: false,
          },
        },
        {
          breakpoint: 0,
          settings: {
            dots: true,
            arrows: false,
          },
        },
      ],
    });
  },
};
