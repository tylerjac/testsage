$(document).ready(function () {
    $('#js-splash-header').slick({
        mobileFirst: true,
        infinite: false,
        arrows: true,
        prevArrow:'<span class="slick-prev"><i class="fal fa-arrow-up"></i></span>',
        nextArrow: '<span class="slick-next"><i class="fal fa-arrow-down"></i></span>',
        dots: true,
        focusOnSelect: true,
        autoplay: false,
        centerMode: true,
        vertical: true,
        verticalSwiping: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [
          {
            breakpoint: 799,
            settings: {
                dots: true,
                arrows: true,
            },
          },
          {
            breakpoint: 798,
            settings: {
                dots: true,
                arrows: false,
            },
          },
          {
            breakpoint: 0,
            settings: {
                dots: true,
                arrows: false,
            },
          },
        ],
    });
});