<!doctype html>
<html class="splash-page no-js" lang="">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Under Development | <?= get_bloginfo(); ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?= get_stylesheet_directory_uri() .'/views/splash/splash.css';?>" />
    <script src="<?php echo get_stylesheet_directory_uri() .'/views/splash/jquery-min.js';?>"></script>
    <script src="<?= get_stylesheet_directory_uri() .'/views/splash/slick-min.js';?>"></script>
    <script src="<?= get_stylesheet_directory_uri() .'/views/splash/splash.js';?>"></script>
    <script src="https://use.typekit.net/<?= get_option('company_info_typekit_id') ?>.js"></script>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.15.2/css/all.css" integrity="sha384-yJpxAFV0Ip/w63YkZfDWDTU6re/Oc3ZiVqMa97pi8uPt92y0wzeK3UFM2yQRhEom" crossorigin="anonymous">
    <script type='text/javascript' src="<?php echo content_url() .'/plugins/gravityforms/js/jquery.json-1.3.js'?>"></script>
    <script type='text/javascript' src="<?php echo content_url() .'/plugins/gravityforms/js/gravityforms.js'?>"></script>
    <script type='text/javascript' src="<?php echo content_url() .'/plugins/gravityforms/js/jquery.textareaCounter.plugin.js'?>"></script>
    <link rel="stylesheet" href="<?php echo content_url() .'/plugins/gravityforms/legacy/css/formsmain.min.css'?>" type="text/css" media="all"/>
    <link rel="stylesheet" href="<?php echo content_url() .'/plugins/gravityforms/legacy/css/formreset.min.css'?>" type="text/css" media="all"/>
    <link rel="stylesheet" href="<?php echo content_url() .'/plugins/gravityforms/legacy/css/readyclass.min.css'?>" type="text/css" media="all"/>
    <link rel="stylesheet" href="<?php echo content_url() .'/plugins/gravityforms/legacy/css/browsers.min.css'?>" type="text/css" media="all"/>
    <style>
      <?php 
        if(get_field('primary_colour','options')){
          $primaryColor = get_field('primary_colour','options');
        } else{
          $primaryColor = '#a46497';
        }
        if(get_field('form_button_background_color','options')){
          $formbackgroundcolor = get_field('form_button_background_color','options');
          $formtexthovercolor = get_field('form_button_background_color','options');
        } else{
           $formbackgroundcolor = 'transparent';
           $formtexthovercolor = 'black';
        }
        if(get_field('form_button_text_color','options')){
          $formtextcolor = get_field('form_button_text_color','options');
        } else{
           $formtextcolor = 'white';
        }
        if(get_field('arrow_color','options')){
          $arrowColor = get_field('arrow_color','options');
        } else{
          $arrowColor = 'white';
        }
        
        if(get_field('arrow_background_color','options')){
          $arrowBackground = get_field('arrow_background_color','options');
        } else{
          $arrowBackground = 'transparent';
        }
        
        if(get_field('arrow_border_color','options')){
          $arrowBorder = get_field('arrow_border_color','options');
        } else{
          $arrowBorder = 'white';
        }

        if(get_field('arrow_background_color_hover','options')){
          $arrowBackgroundHover = get_field('arrow_background_color_hover','options');
        } else{
          $arrowBackgroundHover = 'white';
        }

        if(get_field('arrow_color_hover','options')){
          $arrowColorHover = get_field('arrow_color_hover','options');
        } else{
          $arrowColorHover = '#ab2c29';
        }

        if(get_field('arrow_border_color_hover','options')){
          $arrowBorderHover = get_field('arrow_border_color_hover','options');
        } else{
          $arrowBorderHover = 'white';
        }

        if(get_field('dots_color','options')){
          $dotsColor = get_field('dots_color','options');
        } else{
          $dotsColor = 'white';
        }

        if(get_field('dots_background_color','options')){
          $dotsBackground = get_field('dots_background_color','options');
        } else{
          $dotsBackground = 'transparent';
        }


        if(get_field('dots_border_color','options')){
          $dotsBorder = get_field('dots_border_color','options');
        } else{
          $dotsBorder = 'white';
        }

        if(get_field('form_input_border_color','options')){
          $FormBorder = get_field('form_input_border_color','options');
        } else{
          $FormBorder = 'white';
        }

        if(get_field('form_input_background_color','options')){
          $FormBackground = get_field('form_input_background_color','options');
        } else{
          $FormBackground = 'transparent';
        }

        if(get_field('form_input_color','options')){
          $FormInputColor = get_field('form_input_color','options');
        } else{
          $FormInputColor = 'white';
        }
      ?>

      html.splash-page .banner_splash #js-splash-header .slick-arrow{
        border-color: <?= $arrowBorder ?>;
        background-color: <?= $arrowBackground ?>;
      }
      html.splash-page .banner_splash #js-splash-header .slick-arrow i{
        color: <?= $arrowColor ?>;
      }

      html.splash-page .banner_splash #js-splash-header .slick-arrow:hover{
        border-color: <?= $arrowBorderHover ?>;
        background-color: <?= $arrowBackgroundHover ?>;
      }
      html.splash-page .banner_splash #js-splash-header .slick-arrow:hover i{
        color: <?= $arrowColorHover ?>;
      }

      html.splash-page .banner_splash #js-splash-header .slick-dots li{
        border-color: <?= $dotsBorder ?>;
        background-color: <?= $dotsBackground ?>;
      }

      html.splash-page .banner_splash #js-splash-header .slick-dots li.slick-active:before{
        background-color: <?= $dotsColor ?>;
      }

      html.splash-page .banner_splash #js-splash-header .gform_wrapper li select, 
      html.splash-page .banner_splash #js-splash-header .gform_wrapper li input, 
      html.splash-page .banner_splash #js-splash-header .gform_wrapper li textarea{
        background-color: <?= $FormBackground?> !important;
        border-color: <?= $FormBorder?> !important;
        color: <?= $FormInputColor ?> !important;
      }

      ::-webkit-input-placeholder { /* Edge */
        color: <?= $FormInputColor ?> !important;
      }

      :-ms-input-placeholder { /* Internet Explorer 10-11 */
        color: <?= $FormInputColor ?> !important;
      }

      ::placeholder {
        color: <?= $FormInputColor ?> !important;
      }

      html.splash-page .banner_splash #js-splash-header .col .slide-box h1 span{
        color: <?= $primaryColor ?>;
      }
      html.splash-page .gform_footer {
        margin:0;
      }
      html.splash-page .gform_footer input{
        background-color: <?= $formbackgroundcolor?>;
        color: <?= $formtextcolor ?>;
        cursor: pointer;
        padding: 0.8em 2em;
        border:2px solid <?= $formtextcolor ?>;
        line-height: 1em !important;
      }
      html.splash-page .gform_footer input:hover{
        background-color: <?= $formtextcolor ?>;
        color: <?= $formtexthovercolor ?>;
        border-color: <?= $formtextcolor ?>;
      }

    </style>
    <?php // wp_head() ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js" integrity="sha512-pHVGpX7F/27yZ0ISY+VVjyULApbDlD0/X0rgGbTqCE7WFW5MezNTWG/dnhtbBuICzsd0WQPgpE4REBLv+UqChw==" crossorigin="anonymous"></script>
  </head>
  <body>
    <div class="wrapper">
      <?php
        $logo = get_field('logo','options');
        if(get_field('slides','options')!="") {
          $slides = get_field('slides','options');
        }
        if(!empty($slides)){?>
          <section class="banner_splash">
            <div id="js-splash-header">
              <?php
                foreach($slides as $slide){
                  $slidebackground = $slide['slide_background_color'];
                  $slideopacity = $slide['slide_background_opacity'];
                  ?>
                  <div class="col">
                    <img class="col-img" src="<?= $slide['background_image'] ?>"/>
                    <div class="overlay-box" style="opacity: <?= $slideopacity?>; background-color: <?= $slidebackground?>;"></div>
                    <div class="slide-box">
                      <?php 
                        if($slide['hide_logo'] != 1){?>
                          <img style="max-width: 100%; height: auto;" class="logo" src="<?= $logo ?>" alt="logo"/>
                        <?php }
                        if($slide['title']){?>
                          <h1> <?php echo $slide['title'] ?></h1>
                        <?php }
                        if($slide['subtitle']){?>
                          <?php echo wpautop($slide['subtitle']) ?>
                        <?php }
                        if($slide['content']){?>
                          <?php echo do_shortcode($slide['content']) ?>
                        <?php }
                        if($slide['button']){?>
                          <a <?php if($slide['button_background_colour']){?> style="background-color: <?= $slide['button_background_colour']?>; color: <?= $slide['button_text_colour'] ?>"  <?php } ?> class="button" href="<?= $slide['button']['url']?>" target="<?= $slide['button']['target'] ?>"><?= $slide['button']['title'] ?></a>
                        <?php }
                      ?>
                    </div>
                  </div>
                  <?php
                }
              ?>
            </div>
          </section>
          <?php
        }
      ?>
    </div><!--/.wrapper-->
  </body>
</html>

