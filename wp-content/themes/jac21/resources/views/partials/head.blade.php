<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.15.2/css/all.css" integrity="sha384-yJpxAFV0Ip/w63YkZfDWDTU6re/Oc3ZiVqMa97pi8uPt92y0wzeK3UFM2yQRhEom" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 
  <?php
    $flexible_content = get_field('page_content');
    if($flexible_content){
      foreach($flexible_content as $content) {
        if($content['acf_fc_layout'] == 'carousel_gallery'){?>
          <link rel="stylesheet" href="<?= get_stylesheet_directory_uri() .'/views/fancybox/jquery.fancybox.min.css';?>" />
        <?php }
      }
    }
    $popupselector = get_field('popup_selector');
    if($popupselector){
      $podPost = pods('popup', $popupselector->ID); 
      if($podPost->field('button_background_color') || $podPost->field('button_color')){
        if($podPost->field('button_background_color')){
          $buttonbackcolor = $podPost->field('button_background_color');
        }else{
          $buttonbackcolor = $primary;
        } 
        if($podPost->field('button_color')){
          $buttoncolor = $podPost->field('button_color');
        }else{
          $buttoncolor = '#FFFFFF';
        }?>
        <style>
          #global-popup .content-div .gform_footer input[type="submit"],
          #global-popup .content-div a.button,
          #global-popup .content-div button{
            background-color: <?= $buttonbackcolor ?> !important;
            color: <?= $buttoncolor ?> !important;
            border: 1px solid <?= $buttonbackcolor ?> !important;
            width: 12em !important;
          }
          #global-popup .content-div .gform_footer input[type="submit"]:hover,
          #global-popup .content-div a.button:hover,
          #global-popup .content-div button:hover{
            background-color: <?= $buttoncolor ?> !important;
            border-color: <?= $buttoncolor ?> !important;
            color: <?= $buttonbackcolor ?> !important;
          }
        </style>
        <?php
      }
    }
  ?>
  @php wp_head() @endphp
</head>
