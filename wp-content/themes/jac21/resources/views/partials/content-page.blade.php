@if($page_contents)
	@foreach($page_contents as $content)
		
		@if($content['layout'] == 'hero_section')
			@include('partials.sections.hero-section')
		@endif
		
		@if($content['layout'] == 'breadcrumbs_banner')
			@include('partials.sections.breadcrumbs')
		@endif

		@if($content['layout'] == 'carousel_gallery')
			@include('partials.sections.carousel-gallery')
		@endif

		@if($content['layout'] == 'featured_post_selector')
			@include('partials.sections.featured_post_selector')
		@endif
        
		@if($content['layout'] == 'contact_form_with_map')
			@include('partials.sections.contact-form-map')
		@endif

		@if($content['layout'] == 'video_banner')
			@include('partials.sections.video_banner')
		@endif

		@if($content['layout'] == 'header_slider')
			@include('partials.sections.header_slider')
		@endif

		@if($content['layout'] == 'sitemap_block')
			@include('partials.sections.sitemap_block')
		@endif

	@endforeach
@elseif(get_the_content != "")
	<div class="padding_on_menu_breadcrumb">
		<div class="container section">
			{!! the_content() !!}
		</div>
	</div>
@endif