@if(get_field('linkage_link'))
@php $link = get_field('linkage_link'); @endphp
@elseif(get_field('redirect_link'))
@php $link = get_field('redirect_link'); @endphp
@else
@php $link = get_permalink(); @endphp
@endif

@if(get_field('redirect_link_ntab')==1 || get_field('new_link_window')==1)
@php $linkNT = 'target=_blank'; @endphp
@endif
<article @php post_class() @endphp>
  <div class="bg-gray-light bg-img" href="{{ get_permalink() }}">
      @if(get_post_thumbnail_id( get_the_ID()))
      <img src="{{wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) )}}" alt="{!! $post->post_title !!}" class="lozad">
      @else
      <i class="fal fa-camera-retro"></i>
      @endif
      @php the_excerpt() @endphp
  </div>
    <!-- @if (get_post_type() === 'post')
      @include('partials/entry-meta')
    @endif -->
  <div class="entry-summary bg-primary border-primary">
    <h2 class="text-center entry-title"><a class="hover-primary white" {{$linkNT}} href="{{ $link }}">{!! get_the_title() !!}</a></h2>
    @php the_excerpt() @endphp
  </div>
</article>
