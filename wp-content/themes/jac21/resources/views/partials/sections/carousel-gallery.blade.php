@php $content = $content['data']; @endphp
@if(isset($content['hide_section']) && $content['hide_section']!=1)

@if( is_array($content['gallery']) && !empty($content['gallery']) )
	@if($content['slider_navigation'] == 'dots')
<section data-aos="fade-up" class="content-section awards-carousel">
  <div class="grid-container" style="padding: 0;">
    <div class="owl-carousel owl-theme" id="awards-slider">
      @foreach($content['gallery'] as $img)
	  <div class="item">
        <a data-fancybox="gallery" data-caption="<?php echo $img['alt']; ?>" href="{{$img['url']}}" data-lity><img src="{{$img['url']}}" class="lozad" alt="@php echo($img['alt']!='' ? $img['alt'] : 'Foundation Sage Theme'); @endphp"></a>
      </div>
      @endforeach
    </div>
  </div>
</section>
	@elseif($content['slider_navigation'] == 'arrow')
<section data-aos="fade-up" class="content-section culture-bx-slider">
  <div class="grid-container" style="padding: 0;">
    <div class="owl-carousel owl-theme" id="arrowSlider">
      @foreach($content['gallery'] as $img)
	  <div class="item">
		 <a data-fancybox="gallery" data-caption="<?php echo $img['alt']; ?>" href="{{$img['url']}}"><img src="{{$img['url']}}" class="lozad" alt="@php echo($img['alt']!='' ? $img['alt'] : 'Foundation Sage Theme'); @endphp"></a>
      </div> 
      @endforeach               
    </div>
  </div>            
</section>
	@endif
@endif

@endif