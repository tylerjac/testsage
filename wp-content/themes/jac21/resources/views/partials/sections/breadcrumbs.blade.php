@php $content = $content['data']; @endphp
<?php 
    if($content['padding_on_menu_breadcrumb'] == 1){
        $classpadding = 'padding_on_menu_breadcrumb';
    }; 
?>
@if(isset($content['hide_section']) && $content['hide_section']!=1)
<section class="content-section breadcrumbs <?= $classpadding ?>">
  <img src="{{$content['background_image']}}" alt="{{$content['page_title']}} - {{get_bloginfo()}}">
  @if(trim($content['page_title'])!="" || trim($content['sub_title'])!="")
  <div class="breadcrumbs-info">
    @if(trim($content['sub_title'])!="")
    <h6>{!! $content['sub_title'] !!}</h6>
    @endif
	@if(trim($content['page_title'])!="")
	<h2>{!! $content['page_title'] !!}</h2>
	@endif
  </div>
  @endif
</section>
@endif