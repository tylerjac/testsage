@php $content = $content['data']; @endphp
@if(isset($content['hide_section']) && $content['hide_section']!=1)
<section class="content-section content-section connect">
	{!! $content['background_map'] !!}
	<div class="container">
        <div class="connect-info">
            @if(trim($content['location'])!="" || trim($content['contact'])!="" || trim($content['hours'])!="")
            <div class="connect-info-left">
                @if(trim($content['location'])!="")
                <div class="connect-info-left-row">
                    <span>LOCATION</span>
                    {!! $content['location'] !!}
                </div>
                @endif
                @if(trim($content['contact'])!="")
                <div class="connect-info-left-row">
                    <span>CONTACT</span>
                    {!! $content['contact'] !!}
                </div>
                @endif
                @if(trim($content['hours'])!="")
                <div class="connect-info-left-row">
                    <span>HOURS</span>
                    {!! $content['hours'] !!}
                </div>
                @endif
            </div>
            @endif

            <div class="connect-info-right">
                <div class="container title-black">
                    @if(trim($content['heading'])!="")
                    <h3 class="primary">{!! $content['heading'] !!}</h3>
                    @endif
                    @if(trim($content['sub_heading'])!="")
                    <p>{!! $content['sub_heading'] !!}</p>
                    @endif
                    @if(trim($content['form'])!="")
                        {!! $content['form'] !!}
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
@endif