<section class="content-section sitemap">
    <div class="row" style="max-width: 60em;">
        @foreach($sitemap as $loopX)
            @if(is_array($loopX['child_pages']) && !empty($loopX['child_pages']))
                <div class="sitemap-descendants colbar-full">
                    <h4 class="row border-tertiary full align-middle"><div class="arrow-right border-primary"></div><a class="relative tertiary font-futura"href="{{$loopX['parent_page']['url']}}" @if($loopX['parent_page']['target']) target="_blank" @endif>{!! $loopX['parent_page']['title'] !!}</a></h4>
                    @if(is_array($loopX['child_pages']) && !empty($loopX['child_pages']))
                        <ul class="row full border-tertiary">
                            @foreach($loopX['child_pages'] as $subX)
                            <li class="colbar-3"><div class="arrow-right border-primary"></div><a class="relative tertiary font-futura" href="{{$subX['sub_page']['url']}}" @if($subX['sub_page']['target']) target="_blank" @endif>{!! $subX['sub_page']['title'] !!}</a></li>
                            @endforeach
                        </ul>
                    @endif
                </div>
            @else
                <div class="sitemap-descendants colbar-4">
                    <h4 class="row full align-middle"><div class="arrow-right border-primary"></div><a class="relative tertiary font-futura"href="{{$loopX['parent_page']['url']}}" @if($loopX['parent_page']['target']) target="_blank" @endif>{!! $loopX['parent_page']['title'] !!}</a></h4>
                </div>
            @endif
		@endforeach
    </div>            
</section>