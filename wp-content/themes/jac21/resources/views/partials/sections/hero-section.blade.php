@php $content = $content['data']; @endphp
<?php 
    if($content['padding_on_menu'] == 1){
        $classpadding = 'padding_on_menu';
    };
?>
@if(isset($content['hide_section']) && $content['hide_section']!=1)
    <section class="banner <?= $classpadding?>" style="background-image: url({{$content['background_image']}});">
        @if($content['content_align']=='Left')
            <div class="grid-container hero-info row align-middle align-left left">
                @if(trim($content['main_heading'])!="" || trim($content['sub_heading'])!="")
                <div class="hero-info-block">
                    <div class="ui-slide-in">
                    <div class="text-wrapper">
                        @if(trim($content['main_heading'])!="")
                        <div class="line"><h1>{!! $content['main_heading'] !!}</h1></div>
                        @endif
                        @if(trim($content['sub_heading'])!="")
                        <div class="text-left line"><span>{!! $content['sub_heading'] !!}</span></div>
                        @endif
                        @if(is_array($content['button']) && !empty($content['button']))
                        <div class="text-left btn-custom">
                            <a href="{{$content['button']['url']}}" class="button primary" data-text="{{$content['button']['title']}}" @if($content['button']['target']!='') target="_blank" @endif ><span>{{$content['button']['title']}}</span></a>
                        </div>
                        @endif
                    </div>
                    </div>
                </div>
                @endif
            </div> 
        @endif
        @if($content['content_align']=='Right')
            <div class="grid-container hero-info row align-middle align-right right">
                @if(trim($content['main_heading'])!="" || trim($content['sub_heading'])!="")
                <div class="hero-info-block">
                    <div class="ui-slide-in">
                    <div class="text-wrapper">
                        @if(trim($content['main_heading'])!="")
                        <div class="text-right line"><h1>{!! $content['main_heading'] !!}</h1></div>
                        @endif
                        @if(trim($content['sub_heading'])!="")
                        <div class="text-right line"><span>{!! $content['sub_heading'] !!}</span></div>
                        @endif
                        @if(is_array($content['button']) && !empty($content['button']))
                        <div class="text-right btn-custom">
                            <a href="{{$content['button']['url']}}" class="button primary" data-text="{{$content['button']['title']}}" @if($content['button']['target']!='') target="_blank" @endif ><span>{{$content['button']['title']}}</span></a>
                        </div>
                        @endif
                    </div>
                    </div>
                </div>
                @endif
            </div> 
        @endif
        @if($content['content_align']=='Center')
            <div class="grid-container hero-info align-middle align-center center">
                @if(trim($content['main_heading'])!="" || trim($content['sub_heading'])!="")
                <div class="hero-info-block">
                    <div class="ui-slide-in">
                    <div class="text-wrapper">
                        @if(trim($content['main_heading'])!="")
                        <div class="text-center line"><h1>{!! $content['main_heading'] !!}</h1></div>
                        @endif
                        @if(trim($content['sub_heading'])!="")
                        <div class="text-center line">
                            <span>{!! $content['sub_heading'] !!}</span>
                        </div>
                        @endif
                        @if(is_array($content['button']) && !empty($content['button']))
                        <div class="text-center btn-custom">
                            <a href="{{$content['button']['url']}}" class="button primary" data-text="{{$content['button']['title']}}" @if($content['button']['target']!='') target="_blank" @endif ><span>{{$content['button']['title']}}</span></a>
                        </div>
                        @endif
                    </div>
                    </div>
                </div>
                @endif
            </div> 
        @endif
        @if($content['show_scroll_down'] == 1)
        <div class="down-arrow bounce">
            <a class="fa fa-arrow-down fa-2x" href="javascript:void(0);" id="down"></a>
        </div>
        @endif
    </section>
@endif