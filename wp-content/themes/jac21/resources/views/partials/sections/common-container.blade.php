@php $content = $content['data']; @endphp
@if(isset($content['hide_section']) && $content['hide_section']!=1)
@if(trim($content['content'])!="")
<section class="content-section general-content">
    <div class="grid-container">
	{!! $content['content'] !!}
	</div>
</section>
@endif
@endif