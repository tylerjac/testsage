@php $content = $content['data']; @endphp
@if(isset($content['hide_section']) && $content['hide_section']!=1)
    <section class="content-section featured-post-grid bg-gray-lighter">
        <div class="grid-container full">
            <div class="row">
                @foreach($content['featured_post'] as $post)
                    @php $podPost = pods($post->post_type, $post->ID); $linkNT = ''; @endphp
                    @if($podPost->field('external_link')==1 || $podPost->field('linkage_link')) 
                        @if($podPost->field('redirect_link'))
                        @php $link = $podPost->field('redirect_link'); @endphp
                        @elseif($podPost->field('linkage_link'))
                        @php $link = $podPost->field('linkage_link'); @endphp
                       @endif

                        @if($podPost->field('redirect_link_ntab')==1 || $podPost->field('new_link_window')==1)
                        @php $linkNT = 'target=_blank'; @endphp
                        @endif

                        @if($podPost->field('button_text') != "")
                        @php $buttonText = $podPost->field('button_text'); @endphp
                        @endif
                    @else 
                        @php $link = get_the_permalink($post->ID); @endphp
                    @endif
                    <div class="column large-6 medium-12">
                        <div class="post-grid-bx">
                            <div class="post-grid-bx-img" data-aos="fade-up">
                                <a href="{{$link}}" {{$linkNT}}><img src="{{wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) )}}" alt="{!! $post->post_title !!}" class="lozad"></a>
                            </div>
                        </div>
                        <div class="post-info">
                            <div class="title-block">
                                @if(trim($post->post_title)!="")
                                <h4 data-aos="fade-right"><a class="primary">{!! $post->post_title !!}</a></h4>
                                @endif
                                @if(trim($podPost->field('sub_title'))!="")
                                <h6 data-aos="zoom-in">{!! $podPost->field('sub_title') !!}</h6>
                                @elseif(trim($post->post_subtitle)!="")
                                  <h6 data-aos="zoom-in">{!! $post->post_subtitle !!}</h6>
                                @endif
                            </div>
                            <div class="content-block">
                                <p data-aos="fade-right" class="secondary">{!! $post->post_excerpt !!}</p>
                                <div data-aos="fade-up" class="btn-custom">
                                    <a href="{{$link}}" {{$linkNT}} class="button primary" data-text="LEARN MORE">
                                        <?php
                                            if($buttonText){?>
                                                <span><?= $buttonText ?></span>
                                                <?php
                                            }else{?>
                                                <span>Read More</span>
                                            <?php }
                                        ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div> 
    </section>
@endif