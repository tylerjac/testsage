@php $content = $content['data']; @endphp
<?php 
    if($content['padding_on_menu_slider'] == 1){
        $classpadding = 'padding_on_menu';
        $paddingslider = 'padding_top_slider';
    };
    if($content['slide_height']){
        $height = 'height: '.$content['slide_height'].'px;';
        $heightclass = "given-height";
    } else {
        $height = null;
    };
?>
@if(isset($content['hide_section_slider']) && $content['hide_section_slider']!=1)
    @if($content['slide'])
    <section class="banner header_slider <?= $classpadding ?> <?= $heightclass ?>">
        <div id="js-slider-header">
            @foreach($content['slide'] as $slide)
            <div class="slide-holder">
                <div <?php if($height){?> class="background-center item" <?php } else{?> class="background-center item <?= $classpadding?>" <?php } ?> style="<?php echo $height ?> background-image: url('{!!$slide['image']!!}')">
                    @if($content['slider_background_color'] )
                        <div class="overlay-cover" style="background-color: {!! $content['slider_background_color'] !!}; opacity: {!! $content['slider_opacity'] !!}"></div>
                    @endif
                    <div class="grid-container hero-info row align-middle align-center center">
                        @if(trim($slide['pre_heading'])!="" || trim($slide['heading'])!="")
                        <div class="hero-info-block">
                            <div class="ui-slide-in">
                                <div class="text-wrapper">
                                    @if(trim($slide['heading'])!="")
                                    <div class="text-center line"><span>{!! $slide['heading'] !!}</span></div>
                                    @endif
                                    @if($slide['pre_heading'])
                                    <div class="line"><h1 class="text-center">{!! $slide['pre_heading'] !!}</h1></div>
                                    @endif
                                    @if(is_array($slide['button']) && !empty($slide['button']))
                                    <div class="text-center btn-custom">
                                        <a href="{!! $slide['button']['url'] !!}" class="button primary" target="{!! $slide['button']['target'] !!}"><span>{!! $slide['button']['title'] !!}</span></a>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            @endforeach 
        </div>
    </section>
    @endif
@endif