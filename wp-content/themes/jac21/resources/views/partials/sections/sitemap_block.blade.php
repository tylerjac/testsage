@php $content = $content['data']; @endphp

@if(isset($content['hide_section']) && $content['hide_section']!=1)
    <section class="sitemap_block">
        <div class="row container c-wrap sitemap">
            @if($content['content'] != "")
               {!! $content['content'] !!}
            @endif
        </div>
    </section>
@endif