<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
    @include('partials.menu-fixed')
    <div class="wrapper">
      @php do_action('get_header') @endphp
      @include('partials.header')
      <div class="wrap" role="document">
        <div class="content">
          <main class="main">
            @yield('content')
          </main>
          @if (App\display_sidebar())
            <aside class="sidebar">
              @include('partials.sidebar')
            </aside>
          @endif
        </div>
      </div>
      @php do_action('get_footer') @endphp
      @include('partials.footer')
      @php wp_footer() @endphp
    </div>
    @include('partials.menu-fixed-off-canvas')
    <?php 
      if(get_option('company_info_google_analytics_key')){?>
        <script async src="https://www.googletagmanager.com/gtag/js?id=<?= get_option('company_info_google_analytics_key') ?>"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', '<?= get_option('company_info_google_analytics_key') ?>');
        </script>
        <?php
      }
    ?>
  </body>
</html>
