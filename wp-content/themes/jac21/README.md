# README #

Welcome to the pre-configured sage(v9.0.9) starter theme using latest [foundation](https://get.foundation/sites.html) with dependent plugins and fonts.

Plugins List,

* [ACF Pro](https://www.advancedcustomfields.com/pro/)
* [Pods](https://wordpress.org/plugins/pods/)
* [WooCommerce](https://wordpress.org/plugins/woocommerce/)
* [Yoast SEO](https://wordpress.org/plugins/wordpress-seo/)
* [Gravity Forms](https://www.gravityforms.com/)

Fonts List,

* [Typekit](https://github.com/typekit/webfontloader)
* [Font-Awesome Pro](https://fontawesome.com/pro)


## Requirements ##
Make sure all dependencies have been installed before moving on:

* [WordPress](https://wordpress.org/) >= 5.4
* [PHP](https://secure.php.net/manual/en/install.php) >= 7.2.5 (with [php-mbstring](https://secure.php.net/manual/en/book.mbstring.php) enabled)
* [Composer](https://getcomposer.org/download/)
* [Node.js](http://nodejs.org/) >= 12.0.0
* [Yarn](https://yarnpkg.com/en/docs/install)

## Theme installation ##
To install this base theme using command-prompt/Git-Bash from your WordPress themes directory, goto ```wp-content/themes/``` and clone this repository with the help of,
>
> git clone git@bitbucket.org:jacdev2/new-sage-theme.git
>
> OR
>
> git clone https://username@bitbucket.org/jacdev2/new-sage-theme.git
>

Once cloned, rename the folder with your project name and go inside it using ```cd your-cloned-theme```.

### Update `composer.json` file ###
Open the `composer.json` file in text-editor and update it with the following informations.

- For ACF Pro, On `line number 48` update variable `{%ACF_PRO_LICENCE_KEY%}` with your `ACF Pro` licence key.
- For Gravity-Forms,
> - On `line number 56` update variable `{%GRAVITY_FORM_VERSION%}` with your `Gravity Form` plugin version.
> - On `line number 60`,
>> - update `{%GRAVITY_FORM_VERSION%}` with your `Gravity Form` [plugin version](https://www.gravityforms.com/my-account/downloads/).
>> - update `{%ACCESSKEY%}` with your `AWSAccessKeyId`.
>> - update `{%EXPIREDATE%}` with your `EXPIRE DATE`.
>> - update `{%SIGNATURE%}` with your provided `Signature`.
>> - You will get all this information from Gravity-Form [account](https://www.gravityforms.com/my-account/).
> - On `line number 75` update variable `{%GRAVITY_FORM_VERSION%}` with your `Gravity Form` plugin version.

### Update `.npmrc` file ###
Open the `.npmrc` file in text-editor and update `{%FONT_AWESOME_PRO_TOKEN%}` with your `Font-Awesome Pro` token.

## Steps for further installation actions ##
* Remove the `composer.lock` file from directory and run the `composer install` command inside current directory.
* Now run the `yarn install` command to add dependencies from `package.json` file.

## Build commands ##
* `yarn start` — Compile assets when file changes are made, start Browsersync session
* `yarn build` — Compile and optimize the files in your assets directory
* `yarn build:production` — Compile assets for production

## For `Typekit` webfonts ##
* Goto `\wp-content\themes\your-cloned-theme\resources\assets\scripts\routes`, open `common.js` file and update with required webfonts.

## Extra Usefull Links ##
* ACF Pro
> - https://roots.io/guides/acf-pro-as-a-composer-dependency-with-encrypted-license-key/
* Font-Awesome Pro
> - https://fontawesome.com/how-to-use/on-the-web/setup/using-package-managers
> - https://dev.to/crowscript/installing-the-pro-version-of-font-awesome-5552
> - https://roots.io/guides/how-to-use-font-awesome-5-in-sage/
* Typekit Web-Font
> - https://wpvilla.in/sage-9-load-google-fonts/
> - https://github.com/typekit/webfontloader
* Gravity Form
> - https://docs.gravityforms.com/installation/
> - https://discourse.roots.io/t/best-practice-adding-gravity-forms-and-other-plugins/5320/6

