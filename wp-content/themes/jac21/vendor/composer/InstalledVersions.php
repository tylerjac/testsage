<?php











namespace Composer;

use Composer\Autoload\ClassLoader;
use Composer\Semver\VersionParser;






class InstalledVersions
{
private static $installed = array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => '38a858e5d584680eec1a66c7e6178d95aeb1e1cd',
    'name' => 'roots/sage',
  ),
  'versions' => 
  array (
    'advanced-custom-fields/advanced-custom-fields-pro' => 
    array (
      'pretty_version' => '5.7.12',
      'version' => '5.7.12.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'brain/hierarchy' => 
    array (
      'pretty_version' => '2.5.0',
      'version' => '2.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd5d46c6a44f84e59384b0d4d61b27cb5b91bc523',
    ),
    'composer/installers' => 
    array (
      'pretty_version' => 'v1.10.0',
      'version' => '1.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1a0357fccad9d1cc1ea0c9a05b8847fbccccb78d',
    ),
    'doctrine/inflector' => 
    array (
      'pretty_version' => '1.4.3',
      'version' => '1.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '4650c8b30c753a76bf44fb2ed00117d6f367490c',
    ),
    'gravityforms/gravityforms' => 
    array (
      'pretty_version' => '2.4.22.5',
      'version' => '2.4.22.5',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'illuminate/config' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => '61f4eb8145a1473577a9876471c92fa4de4718a7',
    ),
    'illuminate/console' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => 'de6b95229de443585f97953da4b02721465a2b2b',
    ),
    'illuminate/container' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => '779b56b37396b888414622d5667d3bcc205964ab',
    ),
    'illuminate/contracts' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => '66b653fd430bf06f59cef1112197d009bd02da84',
    ),
    'illuminate/events' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c702e65fe37458fece6ae87ce7906aaa614383d6',
    ),
    'illuminate/filesystem' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b25940b428c9fd284feaad20e176ace8d780973b',
    ),
    'illuminate/support' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e2fce24254b8f60a2f92a3ab485799b372625a06',
    ),
    'illuminate/view' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => '60fd8f12340417a4312e5d90961510333d4f1d46',
    ),
    'kylekatarnls/update-helper' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '429be50660ed8a196e0798e5939760f168ec8ce9',
    ),
    'nesbot/carbon' => 
    array (
      'pretty_version' => '1.25.3',
      'version' => '1.25.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ad6afecd38ce2d7f7bd1b5d47ffd8e93ebbd3ed8',
    ),
    'paragonie/random_compat' => 
    array (
      'pretty_version' => 'v9.99.99',
      'version' => '9.99.99.0',
      'aliases' => 
      array (
      ),
      'reference' => '84b4dfb120c6f9b4ff7b3685f9b8f1aa365a0c95',
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b7ce3b176482dbbc1245ebf52b181af44c2cf55f',
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '0f73288fd15629204f9d42b7055f72dacbe811fc',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/simple-cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
    ),
    'ramsey/uuid' => 
    array (
      'pretty_version' => '3.9.3',
      'version' => '3.9.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '7e1633a6964b48589b142d60542f9ed31bd37a92',
    ),
    'rhumsaa/uuid' => 
    array (
      'replaced' => 
      array (
        0 => '3.9.3',
      ),
    ),
    'roots/sage' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => '38a858e5d584680eec1a66c7e6178d95aeb1e1cd',
    ),
    'roots/sage-installer' => 
    array (
      'pretty_version' => '1.6.4',
      'version' => '1.6.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f832afcb913417f9d25525b7885f6350da36009a',
    ),
    'roots/sage-lib' => 
    array (
      'pretty_version' => '9.0.9',
      'version' => '9.0.9.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b9adfbd72faf997b226d5a9f06b8ea74260e72fd',
    ),
    'roundcube/plugin-installer' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'shama/baton' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'soberwp/controller' => 
    array (
      'pretty_version' => '2.1.2',
      'version' => '2.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '4604826307b4e552c5fb69888ac882c3139dde81',
    ),
    'squizlabs/php_codesniffer' => 
    array (
      'pretty_version' => '2.9.2',
      'version' => '2.9.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '2acf168de78487db620ab4bc524135a13cfe6745',
    ),
    'symfony/console' => 
    array (
      'pretty_version' => 'v4.4.19',
      'version' => '4.4.19.0',
      'aliases' => 
      array (
      ),
      'reference' => '24026c44fc37099fa145707fecd43672831b837a',
    ),
    'symfony/debug' => 
    array (
      'pretty_version' => 'v4.4.19',
      'version' => '4.4.19.0',
      'aliases' => 
      array (
      ),
      'reference' => 'af4987aa4a5630e9615be9d9c3ed1b0f24ca449c',
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => 'v4.4.19',
      'version' => '4.4.19.0',
      'aliases' => 
      array (
      ),
      'reference' => '25d79cfccfc12e84e7a63a248c3f0720fdd92db6',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.22.1',
      'version' => '1.22.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c6c942b1ac76c82448322025e084cadc56048b4e',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.22.1',
      'version' => '1.22.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '5232de97ee3b75b0360528dae24e73db49566ab1',
    ),
    'symfony/polyfill-php73' => 
    array (
      'pretty_version' => 'v1.22.1',
      'version' => '1.22.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a678b42e92f86eca04b7fa4c0f6f19d097fb69e2',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.22.1',
      'version' => '1.22.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dc3063ba22c2a1fd2f45ed856374d79114998f91',
    ),
    'symfony/process' => 
    array (
      'pretty_version' => 'v3.4.47',
      'version' => '3.4.47.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b8648cf1d5af12a44a51d07ef9bf980921f15fca',
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'v2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd15da7ba4957ffb8f1747218be9e1a121fd298a1',
    ),
    'symfony/translation' => 
    array (
      'pretty_version' => 'v4.4.19',
      'version' => '4.4.19.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e1d0c67167a553556d9f974b5fa79c2448df317a',
    ),
    'symfony/translation-contracts' => 
    array (
      'pretty_version' => 'v2.3.0',
      'version' => '2.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e2eaa60b558f26a4b0354e1bbb25636efaaad105',
    ),
    'symfony/translation-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'team-jac/jac-login-page' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => 'b0449d9e23b60146eacfbbdb81d453fa4365a3ab',
    ),
    'team-jac/jac-splash-page' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => 'b385e181f9792e4e54a4cc2628b9a391a10dcab9',
    ),
    'wpackagist-plugin/pods' => 
    array (
      'pretty_version' => '2.7.26',
      'version' => '2.7.26.0',
      'aliases' => 
      array (
      ),
      'reference' => 'tags/2.7.26',
    ),
    'wpackagist-plugin/woocommerce' => 
    array (
      'pretty_version' => '5.0.0',
      'version' => '5.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'tags/5.0.0',
    ),
    'wpackagist-plugin/wordpress-seo' => 
    array (
      'pretty_version' => '15.8',
      'version' => '15.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'tags/15.8',
    ),
  ),
);
private static $canGetVendors;
private static $installedByVendor = array();







public static function getInstalledPackages()
{
$packages = array();
foreach (self::getInstalled() as $installed) {
$packages[] = array_keys($installed['versions']);
}


if (1 === \count($packages)) {
return $packages[0];
}

return array_keys(array_flip(\call_user_func_array('array_merge', $packages)));
}









public static function isInstalled($packageName)
{
foreach (self::getInstalled() as $installed) {
if (isset($installed['versions'][$packageName])) {
return true;
}
}

return false;
}














public static function satisfies(VersionParser $parser, $packageName, $constraint)
{
$constraint = $parser->parseConstraints($constraint);
$provided = $parser->parseConstraints(self::getVersionRanges($packageName));

return $provided->matches($constraint);
}










public static function getVersionRanges($packageName)
{
foreach (self::getInstalled() as $installed) {
if (!isset($installed['versions'][$packageName])) {
continue;
}

$ranges = array();
if (isset($installed['versions'][$packageName]['pretty_version'])) {
$ranges[] = $installed['versions'][$packageName]['pretty_version'];
}
if (array_key_exists('aliases', $installed['versions'][$packageName])) {
$ranges = array_merge($ranges, $installed['versions'][$packageName]['aliases']);
}
if (array_key_exists('replaced', $installed['versions'][$packageName])) {
$ranges = array_merge($ranges, $installed['versions'][$packageName]['replaced']);
}
if (array_key_exists('provided', $installed['versions'][$packageName])) {
$ranges = array_merge($ranges, $installed['versions'][$packageName]['provided']);
}

return implode(' || ', $ranges);
}

throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}





public static function getVersion($packageName)
{
foreach (self::getInstalled() as $installed) {
if (!isset($installed['versions'][$packageName])) {
continue;
}

if (!isset($installed['versions'][$packageName]['version'])) {
return null;
}

return $installed['versions'][$packageName]['version'];
}

throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}





public static function getPrettyVersion($packageName)
{
foreach (self::getInstalled() as $installed) {
if (!isset($installed['versions'][$packageName])) {
continue;
}

if (!isset($installed['versions'][$packageName]['pretty_version'])) {
return null;
}

return $installed['versions'][$packageName]['pretty_version'];
}

throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}





public static function getReference($packageName)
{
foreach (self::getInstalled() as $installed) {
if (!isset($installed['versions'][$packageName])) {
continue;
}

if (!isset($installed['versions'][$packageName]['reference'])) {
return null;
}

return $installed['versions'][$packageName]['reference'];
}

throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}





public static function getRootPackage()
{
$installed = self::getInstalled();

return $installed[0]['root'];
}







public static function getRawData()
{
return self::$installed;
}



















public static function reload($data)
{
self::$installed = $data;
self::$installedByVendor = array();
}




private static function getInstalled()
{
if (null === self::$canGetVendors) {
self::$canGetVendors = method_exists('Composer\Autoload\ClassLoader', 'getRegisteredLoaders');
}

$installed = array();

if (self::$canGetVendors) {

foreach (ClassLoader::getRegisteredLoaders() as $vendorDir => $loader) {
if (isset(self::$installedByVendor[$vendorDir])) {
$installed[] = self::$installedByVendor[$vendorDir];
} elseif (is_file($vendorDir.'/composer/installed.php')) {
$installed[] = self::$installedByVendor[$vendorDir] = require $vendorDir.'/composer/installed.php';
}
}
}

$installed[] = self::$installed;

return $installed;
}
}
