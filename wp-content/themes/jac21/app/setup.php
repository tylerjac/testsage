<?php

namespace App;

use Roots\Sage\Container;
use Roots\Sage\Assets\JsonManifest;
use Roots\Sage\Template\Blade;
use Roots\Sage\Template\BladeProvider;

/**
 * Theme assets
 */
add_action('wp_enqueue_scripts', function () {
    wp_enqueue_style('sage/main.css', asset_path('styles/main.css'), false, null);
    wp_enqueue_script('sage/main.js', asset_path('scripts/main.js'), ['jquery'], null, true);

    if (is_single() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}, 100);

/**
 * Theme setup
 */
add_action('after_setup_theme', function () {
    /**
     * Enable features from Soil when plugin is activated
     * @link https://roots.io/plugins/soil/
     */
    add_theme_support('soil-clean-up');
    add_theme_support('soil-jquery-cdn');
    add_theme_support('soil-nav-walker');
    add_theme_support('soil-nice-search');
    add_theme_support('soil-relative-urls');

    /**
     * Enable plugins to manage the document title
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
     */
    add_theme_support('title-tag');

    /**
     * Register navigation menus
     * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
     */
    register_nav_menus([
        'primary_navigation' => __('Primary Navigation', 'sage')
    ]);
    register_nav_menus([
        'footer_navigation' => __('Footer Navigation', 'sage')
    ]);
    register_nav_menus([
        'secondary_navigation' => __('Secondary Navigation', 'sage')
    ]);
    register_nav_menus([
        'tertiary_navigation' => __('Tertiary Navigation', 'sage')
    ]);

    /**
     * Enable post thumbnails
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    /**
     * Enable HTML5 markup support
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
     */
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    /**
     * Enable selective refresh for widgets in customizer
     * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
     */
    add_theme_support('customize-selective-refresh-widgets');

    /**
     * Use main stylesheet for visual editor
     * @see resources/assets/styles/layouts/_tinymce.scss
     */
    add_editor_style(asset_path('styles/main.css'));
}, 20);

/**
 * Register sidebars
 */
add_action('widgets_init', function () {
    $config = [
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ];
    register_sidebar([
        'name'          => __('Primary', 'sage'),
        'id'            => 'sidebar-primary'
    ] + $config);
    register_sidebar([
        'name'          => __('Footer', 'sage'),
        'id'            => 'sidebar-footer'
    ] + $config);
});

/**
 * Updates the `$post` variable on each iteration of the loop.
 * Note: updated value is only available for subsequently loaded views, such as partials
 */
add_action('the_post', function ($post) {
    sage('blade')->share('post', $post);
});

/**
 * Setup Sage options
 */
add_action('after_setup_theme', function () {
    /**
     * Add JsonManifest to Sage container
     */
    sage()->singleton('sage.assets', function () {
        return new JsonManifest(config('assets.manifest'), config('assets.uri'));
    });

    /**
     * Add Blade to Sage container
     */
    sage()->singleton('sage.blade', function (Container $app) {
        $cachePath = config('view.compiled');
        if (!file_exists($cachePath)) {
            wp_mkdir_p($cachePath);
        }
        (new BladeProvider($app))->register();
        return new Blade($app['view']);
    });

    /**
     * Create @asset() Blade directive
     */
    sage('blade')->compiler()->directive('asset', function ($asset) {
        return "<?= " . __NAMESPACE__ . "\\asset_path({$asset}); ?>";
    });
});

/**
 * Add option pages.
 */
add_action('acf/init', function () {
    if (function_exists('acf_add_options_page')) {
        acf_add_options_page(array(
            'page_title' => __('Foundation Sage Theme Settings', 'jac'),
            'menu_title' => __('Theme Settings', 'jac'),
            'menu_slug' => 'theme-settings',
        ));
    }
});
add_action( 'admin_bar_menu', function( $wp_admin_bar ) {
  if (function_exists('acf_add_options_page')) {
	$wp_admin_bar->add_menu( array(
			'id'     => 'theme-settings',
			'parent' => null,
			'group'  => null,
			'title'  => '<span class="ab-icon wp-menu-image dashicons-before dashicons-admin-generic"></span>'.__( 'Theme Settings', 'foundation-sage-theme' ),
			'href'   => admin_url('admin.php?page=theme-settings'),
	) );
  }
}, 999 );


add_action( 'admin_bar_menu', function( $wp_admin_bar ) {
	$wp_admin_bar->add_menu( array(
			'id'     => 'jac-contact',
			'parent' => 'top-secondary',
			'group'  => null,
			'title'  => '(709) 754-0555',
			'href'   => 'tel:(709)754-0555',
      'meta' =>   array('target' => '_blank'),
	) );
}, 999 );

add_action( 'admin_bar_menu', function( $wp_admin_bar ) {
	$wp_admin_bar->add_menu( array(
			'id'     => 'support-jac',
			'parent' => 'top-secondary',
			'group'  => null,
			'title'  => 'Submit Ticket',
			'href'   => 'https://support.jac.co/portal/en/home',
      'meta' =>   array('target' => '_blank'),
	) );
}, 999 );


/* WP-ADMIN Login Logo Redirect To Site */
add_filter( 'login_headerurl', function() {
	return home_url(); 
});

/* WP-ADMIN Login Logo On Hover Title */
add_filter( 'login_headertitle', function() {
	return 'Powered by '.get_option('blogname');
});

/**
 * Inject Typekit Script into Head
 */
add_action( 'wp_head', function () {
  if (!empty(get_option('company_info_typekit_id'))) {
    ?>
    <script src="https://use.typekit.net/<?= get_option('company_info_typekit_id') ?>.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>
    <?php
  }
});


add_theme_support('featured-posts');

/**
 * subtitle support
 */
// add post subtitle field to DB
if (empty(get_option('subtitle_setup'))) {
  global $wpdb;
  $wpdb->query("ALTER TABLE `{$wpdb->prefix}posts` ADD `post_subtitle` text NOT NULL AFTER `post_title`");
  add_option('subtitle_setup', 1);
}
// add post subtitle field to backend
add_action('edit_form_before_permalink', function ($post) {
  if (post_type_supports($post->post_type, 'subtitle')) { ?>
    <div id="subtitlewrap">
      <label class="screen-reader-text" id="title-prompt-text" for="subtitle">Enter subtitle here</label>
      <input type="text" name="post_subtitle" size="30" value="<?php echo esc_attr( $post->post_subtitle ); ?>" id="subtitle" spellcheck="true" autocomplete="off" placeholder="Enter subtitle here" style="width: 100%;" />
    </div>
  <?php }
}, 10, 1);
// save post subtitle when save post
add_filter('wp_insert_post_data', function ( $data ) {
  if (isset($_POST['post_subtitle'])) $data['post_subtitle'] = $_POST['post_subtitle'];
  return $data;
}, 10);

add_filter( 'register_post_type_args', function ( $args, $post_type ) {
if ($post_type == 'post') {
    $args['menu_position'] = 25;
    $args['supports'] = array_merge($args['supports'], ['subtitle']);
  }

  if ($post_type =='page') {
    $args['supports'] = array_merge($args['supports'], ['subtitle']);
  }

  return $args;
}, 10, 2 );

if(strpos(get_bloginfo('url'),"jac.digital") != '' || strpos(get_bloginfo('url'),"jac.digital") != 'localhost'){
  add_action('pre_option_blog_public', 'false');
}