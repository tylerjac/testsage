<?php
namespace App\Controllers\Partials;

trait ThemeOptions
{

	public function ThemeOptions() {
		/* Header */
		$data['global_notice'] 			= get_field('to_h_global_notice','options');
		$data['hide_global_notice'] 	= get_field('to_h_hide_notice','options');
		$data['site_logo'] 				= get_field('to_h_site_logo','options');
		$data['site_logo_inner'] 		= get_field('to_h_site_logo2','options');
		$data['menu_align'] 			= get_field('to_h_menu_align','options');
		$data['is_sticky_header'] 		= get_field('to_h_sticky_header','options');

		/* Footer */
		$data['footer_bg_img']  		= get_field('to_f_background_image','options');
		$data['footer_form_title']  	= get_field('to_f_subscription_form_title','options');
		$data['footer_form'] 			= get_field('to_f_subscription_form','options');
		$data['footer_social_profiles'] = get_field('to_f_social_profiles','options');
		$data['footer_copyright_text'] 	= get_field('to_f_copyright_text','options');
		$data['footer_credit'] 			= get_field('to_f_credit','options');

		/* Search */
		$data['search_modal_opacity']   = get_field('search_modal_opacity','options');
		$data['search_modal_colour']  	= get_field('search_modal_colour','options');
		
		return $data;
    }

}