<?php
namespace App\Controllers\Partials;

trait PageContent
{
	public function PageContents() {
		$data = [];
		$flexible_content = get_field('page_content');
		if($flexible_content){
			foreach($flexible_content as $content) {
				if($content['acf_fc_layout']=='hero_section'){
					$this_content = [
						'layout' => $content['acf_fc_layout'],
						'data' => $content
					];
					array_push($data, $this_content);
				} else if($content['acf_fc_layout']=='breadcrumbs_banner') {
					$this_content = [
						'layout' => $content['acf_fc_layout'],
						'data' => $content
					];
					array_push($data, $this_content);
				} else if($content['acf_fc_layout']=='carousel_gallery') {
					$this_content = [
						'layout' => $content['acf_fc_layout'],
						'data' => $content
					];
					array_push($data, $this_content);
				} else if($content['acf_fc_layout']=='featured_post_selector') {
					$this_content = [
						'layout' => $content['acf_fc_layout'],
						'data' => $content
					];
					array_push($data, $this_content);
				} else if($content['acf_fc_layout']=='contact_form_with_map') {
					$this_content = [
						'layout' => $content['acf_fc_layout'],
						'data' => $content
					];
					array_push($data, $this_content);
				} else if($content['acf_fc_layout']=='video_banner') {
					$this_content = [
						'layout' => $content['acf_fc_layout'],
						'data' => $content
					];
					array_push($data, $this_content);
				} else if($content['acf_fc_layout']=='header_slider') {
					$this_content = [
						'layout' => $content['acf_fc_layout'],
						'data' => $content
					];
					array_push($data, $this_content);
				} else if($content['acf_fc_layout']=='sitemap_block') {
					$this_content = [
						'layout' => $content['acf_fc_layout'],
						'data' => $content
					];
					array_push($data, $this_content);
				}

				//echo "<pre>"; print_r($content);
			}
		}
		return $data;
	}
}
?>