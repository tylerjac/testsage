<?php
namespace App\Controllers\Partials;

trait Sitemap
{
	public function Sitemap() {
		$data = [];
		$content = get_field('sitemap');
		if($content) {
			return $content;
		}
	}
}