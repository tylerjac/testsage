<section class="content-section sitemap">
    <div class="row" style="max-width: 60em;">
        <?php $__currentLoopData = $sitemap; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $loopX): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php if(is_array($loopX['child_pages']) && !empty($loopX['child_pages'])): ?>
                <div class="sitemap-descendants colbar-full">
                    <h4 class="row border-tertiary full align-middle"><div class="arrow-right border-primary"></div><a class="relative tertiary font-futura"href="<?php echo e($loopX['parent_page']['url']); ?>" <?php if($loopX['parent_page']['target']): ?> target="_blank" <?php endif; ?>><?php echo $loopX['parent_page']['title']; ?></a></h4>
                    <?php if(is_array($loopX['child_pages']) && !empty($loopX['child_pages'])): ?>
                        <ul class="row full border-tertiary">
                            <?php $__currentLoopData = $loopX['child_pages']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subX): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li class="colbar-3"><div class="arrow-right border-primary"></div><a class="relative tertiary font-futura" href="<?php echo e($subX['sub_page']['url']); ?>" <?php if($subX['sub_page']['target']): ?> target="_blank" <?php endif; ?>><?php echo $subX['sub_page']['title']; ?></a></li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    <?php endif; ?>
                </div>
            <?php else: ?>
                <div class="sitemap-descendants colbar-4">
                    <h4 class="row full align-middle"><div class="arrow-right border-primary"></div><a class="relative tertiary font-futura"href="<?php echo e($loopX['parent_page']['url']); ?>" <?php if($loopX['parent_page']['target']): ?> target="_blank" <?php endif; ?>><?php echo $loopX['parent_page']['title']; ?></a></h4>
                </div>
            <?php endif; ?>
		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>            
</section>