<?php if($page_contents): ?>
	<?php $__currentLoopData = $page_contents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $content): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		
		<?php if($content['layout'] == 'hero_section'): ?>
			<?php echo $__env->make('partials.sections.hero-section', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>
		
		<?php if($content['layout'] == 'breadcrumbs_banner'): ?>
			<?php echo $__env->make('partials.sections.breadcrumbs', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>

		<?php if($content['layout'] == 'carousel_gallery'): ?>
			<?php echo $__env->make('partials.sections.carousel-gallery', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>

		<?php if($content['layout'] == 'featured_post_selector'): ?>
			<?php echo $__env->make('partials.sections.featured_post_selector', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>
        
		<?php if($content['layout'] == 'contact_form_with_map'): ?>
			<?php echo $__env->make('partials.sections.contact-form-map', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php endif; ?>

	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php else: ?>
	
<?php endif; ?>


<?php if(isset($sitemap) && is_array($sitemap) && !empty($sitemap)): ?>
	<?php echo $__env->make('partials.sections.sitemap', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php endif; ?>