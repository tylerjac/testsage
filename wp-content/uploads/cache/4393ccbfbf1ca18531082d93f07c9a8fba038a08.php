<?php $content = $content['data']; ?>
<?php 
    if($content['padding_on_menu_slider'] == 1){
        $classpadding = 'padding_on_menu';
        $paddingslider = 'padding_top_slider';
    };
    if($content['slide_height']){
        $height = 'height: '.$content['slide_height'].'px;';
        $heightclass = "given-height";
    } else {
        $height = null;
    };
?>
<?php if(isset($content['hide_section_slider']) && $content['hide_section_slider']!=1): ?>
    <?php if($content['slide']): ?>
    <section class="banner header_slider <?= $classpadding ?> <?= $heightclass ?>">
        <div id="js-slider-header">
            <?php $__currentLoopData = $content['slide']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slide): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="slide-holder">
                <div <?php if($height){?> class="background-center item" <?php } else{?> class="background-center item <?= $classpadding?>" <?php } ?> style="<?php echo $height ?> background-image: url('<?php echo $slide['image']; ?>')">
                    <?php if($content['slider_background_color'] ): ?>
                        <div class="overlay-cover" style="background-color: <?php echo $content['slider_background_color']; ?>; opacity: <?php echo $content['slider_opacity']; ?>"></div>
                    <?php endif; ?>
                    <div class="grid-container hero-info row align-middle align-center center">
                        <?php if(trim($slide['pre_heading'])!="" || trim($slide['heading'])!=""): ?>
                        <div class="hero-info-block">
                            <div class="ui-slide-in">
                                <div class="text-wrapper">
                                    <?php if(trim($slide['heading'])!=""): ?>
                                    <div class="text-center line"><span><?php echo $slide['heading']; ?></span></div>
                                    <?php endif; ?>
                                    <?php if($slide['pre_heading']): ?>
                                    <div class="line"><h1 class="text-center"><?php echo $slide['pre_heading']; ?></h1></div>
                                    <?php endif; ?>
                                    <?php if(is_array($slide['button']) && !empty($slide['button'])): ?>
                                    <div class="text-center btn-custom">
                                        <a href="<?php echo $slide['button']['url']; ?>" class="button primary" target="<?php echo $slide['button']['target']; ?>"><span><?php echo $slide['button']['title']; ?></span></a>
                                    </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
        </div>
    </section>
    <?php endif; ?>
<?php endif; ?>