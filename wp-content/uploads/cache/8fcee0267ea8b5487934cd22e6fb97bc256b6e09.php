<!doctype html>
<html <?php echo get_language_attributes(); ?>>
  <?php echo $__env->make('partials.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <body <?php body_class() ?>>
    <?php echo $__env->make('partials.menu-fixed', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="wrapper">
      <?php do_action('get_header') ?>
      <?php echo $__env->make('partials.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <div class="wrap" role="document">
        <div class="content">
          <main class="main">
            <?php echo $__env->yieldContent('content'); ?>
          </main>
          <?php if(App\display_sidebar()): ?>
            <aside class="sidebar">
              <?php echo $__env->make('partials.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </aside>
          <?php endif; ?>
        </div>
      </div>
      <?php do_action('get_footer') ?>
      <?php echo $__env->make('partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php wp_footer() ?>
    </div>
    <?php echo $__env->make('partials.menu-fixed-off-canvas', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php 
      if(get_option('company_info_google_analytics_key')){?>
        <script async src="https://www.googletagmanager.com/gtag/js?id=<?= get_option('company_info_google_analytics_key') ?>"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', '<?= get_option('company_info_google_analytics_key') ?>');
        </script>
        <?php
      }
    ?>
  </body>
</html>
