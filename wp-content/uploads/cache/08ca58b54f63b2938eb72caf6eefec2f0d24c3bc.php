<?php $content = $content['data']; ?>

<?php if(isset($content['hide_section']) && $content['hide_section']!=1): ?>
    <section class="sitemap_block">
        <div class="row container c-wrap sitemap">
            <?php if($content['content'] != ""): ?>
               <?php echo $content['content']; ?>

            <?php endif; ?>
        </div>
    </section>
<?php endif; ?>