
<footer class="footer" style="background-image: url(<?php echo e($theme_options['footer_bg_img']); ?>);">
  <div class="footer-top">
      <div class="grid-container row">
          <div data-aos="fade-right" class="column small-12 medium-6 large-4">
             <?php if( trim($theme_options['footer_form_title'])!="" ): ?>
              <span><?php echo $theme_options['footer_form_title']; ?></span>
            <?php endif; ?>
            <?php if( trim($theme_options['footer_form'])!="" ): ?>
              <?php echo $theme_options['footer_form']; ?>

            <?php endif; ?>
          </div>
          <div data-aos="fade-up" class="nav-mid column small-12 medium-6 large-5">
             <?php echo wp_nav_menu( array( 'menu' => 'Primary', 'container'=> false, 'menu_class'=> 'false' ) ); ?>

          </div>
          <div data-aos="fade-in" class="social-container column small-12 medium-6 large-3">
            <?php if(is_array($theme_options['footer_social_profiles'])): ?>
            <div class="sicon text-right">
               <p>FOLLOW US ON SOCIAL MEDIA</p>
                <ul class="text-right">
                  <?php if( trim($theme_options['footer_social_profiles']['facebook'])!="" ): ?>
                            <li><a href="<?php echo e($theme_options['footer_social_profiles']['facebook']); ?>" target="_blank"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                          <?php endif; ?>
                  <?php if( trim($theme_options['footer_social_profiles']['linkedin'])!="" ): ?>
                    <li><a href="<?php echo e($theme_options['footer_social_profiles']['linkedin']); ?>" target="_blank"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a></li>
                          <?php endif; ?>
                  <?php if( trim($theme_options['footer_social_profiles']['instagram'])!="" ): ?>
                    <li><a href="<?php echo e($theme_options['footer_social_profiles']['instagram']); ?>" target="_blank"><i class="fab fa-instagram" aria-hidden="true"></i></a></li>
                          <?php endif; ?>
                  <?php if( trim($theme_options['footer_social_profiles']['twitter'])!="" ): ?>
                    <li><a href="<?php echo e($theme_options['footer_social_profiles']['twitter']); ?>" target="_blank"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                          <?php endif; ?>
                  <?php if( trim($theme_options['footer_social_profiles']['location'])!="" ): ?>
                    <li><a href="<?php echo e($theme_options['footer_social_profiles']['location']); ?>" target="_blank"><i class="fal fa-map-marker-alt" aria-hidden="true"></i></a></li>
                  <?php endif; ?>
                </ul>
            </div> 
          </div>
		<?php endif; ?>
      </div>
  </div>
  <div class="copyright">
      <div class="grid-container">
          <div class="copyright-info">
              <div class="row full align-justify align-middle">
                  <div class="column small-12 medium-6 large-4">
                      <?php if(trim($theme_options['footer_copyright_text'])!=""): ?> <p><?php echo $theme_options['footer_copyright_text']; ?></p> <?php endif; ?>
                  </div>
                  <div class="column small-12 medium-6 large-4">
                      <div class="footer-nvabar">
						            <?php echo wp_nav_menu( array( 'menu' => 'footer-menu', 'container'=> false, 'menu_class'=> 'false' ) ); ?>

                      </div>
                  </div>
                  <div class="column small-12 medium-6 large-4">
                    <?php if(is_array($theme_options['footer_credit']) && !empty($theme_options['footer_credit'])): ?>
                    <div class="footer-right">
                        <?php echo $theme_options['footer_credit']['credit_text']; ?> 
                          <?php if($theme_options['footer_credit']['icon'] == ""): ?>
                            <?php credit(); ?>
                          <?php elseif( $theme_options['footer_credit']['link_type'] == 'icon' ): ?>
                             <p><a href="<?php echo e($theme_options['footer_credit']['icon_link']); ?>" target="_blank"><img src="<?php echo e($theme_options['footer_credit']['icon']); ?>" alt="<?php echo e($theme_options['footer_credit']['credit_text']); ?>" class=""></a></p>
                          <?php elseif( $theme_options['footer_credit']['link_type'] == 'text-link' ): ?>
                          <?php if(is_array($theme_options['footer_credit']['credit_link']) && !empty($theme_options['footer_credit']['credit_link'])): ?>
                             <p><span><a href="<?php echo e($theme_options['footer_credit']['credit_link']['url']); ?>" <?php if($theme_options['footer_credit']['credit_link']['target']!=""): ?> target="_blank" <?php endif; ?> ><?php echo e($theme_options['footer_credit']['credit_link']['title']); ?></a></span></p>
                          <?php endif; ?>
                        <?php endif; ?>
                      
                    </div>
                    <?php endif; ?>
                  </div>
              </div>
          </div>                    
      </div>
  </div>
</footer>