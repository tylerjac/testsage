<?php $content = $content['data']; ?>
<?php 
    if($content['padding_on_menu_video'] == 1){
        $classpadding = 'padding_on_menu';
    };
?>
<?php if(isset($content['hide_section']) && $content['hide_section']!=1): ?>
    <section class="banner <?= $classpadding?>" style="overflow: hidden;">
        <video <?php if($content['video_poster_image']['url']){?> poster="<?= $content['video_poster_image']['url']?>" <?php } ?> loop autoplay muted playsinline style="position: absolute; left: 0; top: 0; right:0; bottom: 0; min-width: 100%; min-height: 100%; width: auto; height: auto; background-size:cover; background-position:center;">
            <source src="<?= $content['video_link'] ?>">
        </video> 
        <?php if($content['show_scroll_down'] == 1): ?>
        <div class="down-arrow bounce">
            <a class="fa fa-arrow-down fa-2x" href="javascript:void(0);" id="down"></a>
        </div>
        <?php endif; ?>
    </section>
<?php endif; ?>