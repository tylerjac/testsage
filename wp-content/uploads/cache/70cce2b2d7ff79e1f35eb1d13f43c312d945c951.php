<?php
/*
The below code is just an example. Modify the markup as needed.
Change the "js-menu-open" class to "js-menu-open-left" if you
want off-canvas to open from the left. Don't forget to add
class "left" to nav.menu-off-canvas as well.
*/
?>
<nav class="menu-fixed">
  <?php if(trim($theme_options['global_notice'])!=""): ?>
    <?php if(trim($theme_options['hide_global_notice'])==1 && (is_home() || is_front_page())): ?>
    <?php else: ?>
    <div class="header-top bg-white">
        <p class="primary" style="text-align: center; padding:0.5em; margin:0; ">
          <i class="fal fa-info-circle"></i> <?php echo $theme_options['hide_global_notice']; ?> <?php echo $theme_options['global_notice']; ?>

        </p>
    </div>
    <?php endif; ?>
  <?php endif; ?>
  <div class="topbar bg-gray-darker border-primary">
    <div class="grid-container flex-container align-right">
      <div class="menu-other">
        <?php
          if (has_nav_menu('secondary_navigation')) :
            wp_nav_menu(['theme_location' => 'secondary_navigation', 'menu_class' => 'menu-secondary nolist']);
          endif;
        ?>
        <li class="menu-item menu-log">
            <?php
            if(is_user_logged_in()){?>
                <a class="main-link text-uppercase" href="<?php echo wp_logout_url(); ?>">Logout</a>
                <?php
            } else {
                $linkaccount = get_the_permalink(get_id_by_slug('account'));?>
                <a class="main-link text-uppercase" href="<?= $linkaccount?>">Login</a>
                <?php
            }
            ?>
        </li>
      </div>
    </div><!-- container -->
  </div><!-- topbar -->

  <div class="bottombar bg-primary knockout">
    <div class="grid-container flex-container align-justify">
      <a class="brand" href="<?= esc_url(home_url('/')); ?>" title="<?php bloginfo('name'); ?>">
         <?php if( trim($theme_options['site_logo'])!="" ): ?>
            <?php echo file_get_contents($theme_options['site_logo']); ?>
          <?php else: ?> <?php echo file_get_contents(get_template_directory().'/assets/images/logo.svg'); ?>
          <?php endif; ?>
      </a>

      <div class="menu-center">
        <?php
          if (has_nav_menu('primary_navigation')) :
            wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'menu-primary nolist']);
          endif;
        ?>
      </div>
      <div class="menu-right">
        <ul class="menu-side nolist">
         <span class="menu-search js-popup-search"><i class="fa fa-search"></i></span>
          <li class="menu-open bar-container">
            <span><i></i><i></i><i></i></span>
          </li>
        </ul>
      </div>
      <ul class="menu-utility nolist">
        <li class="menu-open js-menu-open">
          <span><i></i><i></i><i></i></span>
        </li>
      </ul>

    </div><!-- container -->
  </div><!-- bottom-bar -->

  <div class="holder-dropdown container">
    <div class="dropdown-menu bg-tertiary">
      <a class="button-close"><i class="far fa-times-circle"></i></a>
      <?php
         if (has_nav_menu('tertiary_navigation')) :
          wp_nav_menu(['theme_location' => 'tertiary_navigation', 'menu_class' => 'menu-tertiary nolist']);
        endif;
      ?>
      <li class="menu-item menu-log">
        <?php
          if(is_user_logged_in()){?>
            <a class="main-link text-uppercase" href="<?php echo wp_logout_url(); ?>">Logout</a>
            <?php
          } else {
            $linkaccount = get_the_permalink(get_id_by_slug('account'));?>
            <a class="main-link text-uppercase" href="<?= $linkaccount?>">Login</a>
            <?php
          }
        ?>
      </li>
      <div class="contact-dropdown row full align-justify">
        <?php 
          $address1 = get_option('company_info_address');
          $address2 = get_option('company_info_address_two');
          $mailingAddress = get_option('company_info_mailing_address');
          $mailingCity = get_option('company_info_mailing_city');
          $mailingProvince = get_option('company_info_mailing_province');
          $mailingPostal = get_option('company_info_mailing_postal');
          $city = get_option('company_info_city');
          $country = get_option('company_info_country');
          $province = get_option('company_info_province');
          $phone = get_option('company_info_phone');
          $postal = get_option('company_info_postal');
          $fax = get_option('company_info_fax');
          $tollfree = get_option('company_info_tollfree');
        ?>
        <div class="small-12 medium-7 flex-one">
          <div class="col-one">
            <?php
              if($province && $city){?>
                <h4>Head office: <?= $city ?>, <?= $province ?></h4>
              <?php }
              if($phone){?>
                <p>Tel: <?= $phone ?></p>
              <?php } 
              if($fax){?>
                <p>Fax: <?= $fax ?></p>
              <?php }
              if($tollphone){?>
                <p>Tollphone: <?= $tollphone ?></p>
              <?php }
            ?>
          </div>
          <?php
             if($mailingAddress){?>
                <div class="col-one">
                  <h4>Mailing Address:</h4>
                  <?php
                    if($mailingAddress){?>
                      <p><?= $mailingAddress ?></p>
                    <?php }
                    if($mailingCity && $mailingProvince){?>
                      <p><?= $mailingCity ?>, <?= $mailingProvince ?></p>
                    <?php } 
                    if($mailingPostal){?>
                      <p> <?= $mailingPostal ?></p>
                    <?php }
                  ?>
                </div>
                <?php
             }
            ?>
        </div>
        <div class="small-12 medium-4 flex-one">
          <div class="col-one">
            <h4>Courier Address:</h4>
            <?php
            if($address2){?>
              <p><?= $address2 ?></p>
            <?php }
            if($address1){?>
              <p><?= $address1 ?></p>
            <?php }
            if($city && $province){?>
              <p><?= $city ?>, <?= $province ?></p>
            <?php } 
            if($postal){?>
              <p> <?= $postal ?></p>
            <?php }
          ?>
          </div>
        </div>
      </div>
      <p class="social-text">FOLLOW US ON SOCIAL MEDIA</p>
      <ul class="social nolist">
        <?php if( trim($theme_options['footer_social_profiles']['facebook'])!="" ): ?>
                  <li><a href="<?php echo e($theme_options['footer_social_profiles']['facebook']); ?>" target="_blank"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                <?php endif; ?>
        <?php if( trim($theme_options['footer_social_profiles']['linkedin'])!="" ): ?>
          <li><a href="<?php echo e($theme_options['footer_social_profiles']['linkedin']); ?>" target="_blank"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a></li>
                <?php endif; ?>
        <?php if( trim($theme_options['footer_social_profiles']['instagram'])!="" ): ?>
          <li><a href="<?php echo e($theme_options['footer_social_profiles']['instagram']); ?>" target="_blank"><i class="fab fa-instagram" aria-hidden="true"></i></a></li>
                <?php endif; ?>
        <?php if( trim($theme_options['footer_social_profiles']['twitter'])!="" ): ?>
          <li><a href="<?php echo e($theme_options['footer_social_profiles']['twitter']); ?>" target="_blank"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                <?php endif; ?>
        <?php if( trim($theme_options['footer_social_profiles']['location'])!="" ): ?>
          <li><a href="<?php echo e($theme_options['footer_social_profiles']['location']); ?>" target="_blank"><i class="fal fa-map-marker-alt" aria-hidden="true"></i></a></li>
        <?php endif; ?>
      </ul>
    </div>
  </div>
</nav><!--/.sticky-nav-->