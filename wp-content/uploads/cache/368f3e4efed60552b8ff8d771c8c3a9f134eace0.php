<?php $content = $content['data']; ?>
<?php 
    if($content['padding_on_menu'] == 1){
        $classpadding = 'padding_on_menu';
    }; 
?>
<?php if(isset($content['hide_section']) && $content['hide_section']!=1): ?>
    <section class="banner <?= $classpadding?>" style="background-image: url(<?php echo e($content['background_image']); ?>);">
        <?php if($content['content_align']=='Left'): ?>
            <div class="grid-container hero-info row align-middle align-left left">
                <?php if(trim($content['main_heading'])!="" || trim($content['sub_heading'])!=""): ?>
                <div class="hero-info-block">
                    <div class="ui-slide-in">
                    <div class="text-wrapper">
                        <?php if(trim($content['main_heading'])!=""): ?>
                        <div class="line"><h1><?php echo $content['main_heading']; ?></h1></div>
                        <?php endif; ?>
                        <?php if(trim($content['sub_heading'])!=""): ?>
                        <div class="text-left line"><span><?php echo $content['sub_heading']; ?></span></div>
                        <?php endif; ?>
                        <?php if(is_array($content['button']) && !empty($content['button'])): ?>
                        <div class="text-left btn-custom">
                            <a href="<?php echo e($content['button']['url']); ?>" class="button primary" data-text="<?php echo e($content['button']['title']); ?>" <?php if($content['button']['target']!=''): ?> target="_blank" <?php endif; ?> ><span><?php echo e($content['button']['title']); ?></span></a>
                        </div>
                        <?php endif; ?>
                    </div>
                    </div>
                </div>
                <?php endif; ?>
            </div> 
        <?php endif; ?>
        <?php if($content['content_align']=='Right'): ?>
            <div class="grid-container hero-info row align-middle align-right right">
                <?php if(trim($content['main_heading'])!="" || trim($content['sub_heading'])!=""): ?>
                <div class="hero-info-block">
                    <div class="ui-slide-in">
                    <div class="text-wrapper">
                        <?php if(trim($content['main_heading'])!=""): ?>
                        <div class="text-right line"><h1><?php echo $content['main_heading']; ?></h1></div>
                        <?php endif; ?>
                        <?php if(trim($content['sub_heading'])!=""): ?>
                        <div class="text-right line"><span><?php echo $content['sub_heading']; ?></span></div>
                        <?php endif; ?>
                        <?php if(is_array($content['button']) && !empty($content['button'])): ?>
                        <div class="text-right btn-custom">
                            <a href="<?php echo e($content['button']['url']); ?>" class="button primary" data-text="<?php echo e($content['button']['title']); ?>" <?php if($content['button']['target']!=''): ?> target="_blank" <?php endif; ?> ><span><?php echo e($content['button']['title']); ?></span></a>
                        </div>
                        <?php endif; ?>
                    </div>
                    </div>
                </div>
                <?php endif; ?>
            </div> 
        <?php endif; ?>
        <?php if($content['content_align']=='Center'): ?>
            <div class="grid-container hero-info align-middle align-center center">
                <?php if(trim($content['main_heading'])!="" || trim($content['sub_heading'])!=""): ?>
                <div class="hero-info-block">
                    <div class="ui-slide-in">
                    <div class="text-wrapper">
                        <?php if(trim($content['main_heading'])!=""): ?>
                        <div class="text-center line"><h1><?php echo $content['main_heading']; ?></h1></div>
                        <?php endif; ?>
                        <?php if(trim($content['sub_heading'])!=""): ?>
                        <div class="text-center line">
                            <span><?php echo $content['sub_heading']; ?></span>
                        </div>
                        <?php endif; ?>
                        <?php if(is_array($content['button']) && !empty($content['button'])): ?>
                        <div class="text-center btn-custom">
                            <a href="<?php echo e($content['button']['url']); ?>" class="button primary" data-text="<?php echo e($content['button']['title']); ?>" <?php if($content['button']['target']!=''): ?> target="_blank" <?php endif; ?> ><span><?php echo e($content['button']['title']); ?></span></a>
                        </div>
                        <?php endif; ?>
                    </div>
                    </div>
                </div>
                <?php endif; ?>
            </div> 
        <?php endif; ?>
        <?php if($content['show_scroll_down'] == 1): ?>
        <div class="down-arrow bounce">
            <a class="fa fa-arrow-down fa-2x" href="javascript:void(0);" id="down"></a>
        </div>
        <?php endif; ?>
    </section>
<?php endif; ?>