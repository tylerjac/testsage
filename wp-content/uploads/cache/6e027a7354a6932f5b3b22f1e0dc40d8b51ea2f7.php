<?php $content = $content['data']; ?>
<?php 
    if($content['padding_on_menu_breadcrumb'] == 1){
        $classpadding = 'padding_on_menu_breadcrumb';
    }; 
?>
<?php if(isset($content['hide_section']) && $content['hide_section']!=1): ?>
<section class="content-section breadcrumbs <?= $classpadding ?>">
  <img src="<?php echo e($content['background_image']); ?>" alt="<?php echo e($content['page_title']); ?> - <?php echo e(get_bloginfo()); ?>">
  <?php if(trim($content['page_title'])!="" || trim($content['sub_title'])!=""): ?>
  <div class="breadcrumbs-info">
    <?php if(trim($content['sub_title'])!=""): ?>
    <h6><?php echo $content['sub_title']; ?></h6>
    <?php endif; ?>
	<?php if(trim($content['page_title'])!=""): ?>
	<h2><?php echo $content['page_title']; ?></h2>
	<?php endif; ?>
  </div>
  <?php endif; ?>
</section>
<?php endif; ?>