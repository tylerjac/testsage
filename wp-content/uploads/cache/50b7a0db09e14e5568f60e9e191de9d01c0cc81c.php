<section class="sitemap">
    <div class="grid-container">
        <?php $__currentLoopData = $sitemap; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $loopX): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<div class="sitemap-descendants">
			<h4><a href="<?php echo e($loopX['parent_page']['url']); ?>" <?php if($loopX['parent_page']['target']): ?> target="_blank" <?php endif; ?>><?php echo $loopX['parent_page']['title']; ?></a></h4>
			<?php if(is_array($loopX['child_pages']) && !empty($loopX['child_pages'])): ?>
            <ul>
				<?php $__currentLoopData = $loopX['child_pages']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subX): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><a href="<?php echo e($subX['sub_page']['url']); ?>" <?php if($subX['sub_page']['target']): ?> target="_blank" <?php endif; ?>><?php echo $subX['sub_page']['title']; ?></a></li>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
			<?php endif; ?>
        </div>
		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>            
</section>