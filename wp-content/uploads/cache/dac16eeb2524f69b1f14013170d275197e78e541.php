<?php $content = $content['data']; ?>
<?php if(isset($content['hide_section']) && $content['hide_section']!=1): ?>
    <section class="content-section featured-post-grid bg-gray-lighter">
        <div class="grid-container full">
            <div class="row">
                <?php $__currentLoopData = $content['featured_post']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php $podPost = pods($post->post_type, $post->ID); $linkNT = ''; ?>
                    <?php if($podPost->field('external_link')==1 || $podPost->field('linkage_link')): ?> 
                        <?php if($podPost->field('redirect_link')): ?>
                        <?php $link = $podPost->field('redirect_link'); ?>
                        <?php elseif($podPost->field('linkage_link')): ?>
                        <?php $link = $podPost->field('linkage_link'); ?>
                       <?php endif; ?>

                        <?php if($podPost->field('redirect_link_ntab')==1 || $podPost->field('new_link_window')==1): ?>
                        <?php $linkNT = 'target=_blank'; ?>
                        <?php endif; ?>

                        <?php if($podPost->field('button_text') != ""): ?>
                        <?php $buttonText = $podPost->field('button_text'); ?>
                        <?php endif; ?>
                    <?php else: ?> 
                        <?php $link = get_the_permalink($post->ID); ?>
                    <?php endif; ?>
                    <div class="column large-6 medium-12">
                        <div class="post-grid-bx">
                            <div class="post-grid-bx-img" data-aos="fade-up">
                                <a href="<?php echo e($link); ?>" <?php echo e($linkNT); ?>><img src="<?php echo e(wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) )); ?>" alt="<?php echo $post->post_title; ?>" class="lozad"></a>
                            </div>
                        </div>
                        <div class="post-info">
                            <div class="title-block">
                                <?php if(trim($post->post_title)!=""): ?>
                                <h4 data-aos="fade-right"><a class="primary"><?php echo $post->post_title; ?></a></h4>
                                <?php endif; ?>
                                <?php if(trim($podPost->field('sub_title'))!=""): ?>
                                <h6 data-aos="zoom-in"><?php echo $podPost->field('sub_title'); ?></h6>
                                <?php elseif(trim($post->post_subtitle)!=""): ?>
                                  <h6 data-aos="zoom-in"><?php echo $post->post_subtitle; ?></h6>
                                <?php endif; ?>
                            </div>
                            <div class="content-block">
                                <p data-aos="fade-right" class="secondary"><?php echo $post->post_excerpt; ?></p>
                                <div data-aos="fade-up" class="btn-custom">
                                    <a href="<?php echo e($link); ?>" <?php echo e($linkNT); ?> class="button primary" data-text="LEARN MORE">
                                        <?php
                                            if($buttonText){?>
                                                <span><?= $buttonText ?></span>
                                                <?php
                                            }else{?>
                                                <span>Read More</span>
                                            <?php }
                                        ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div> 
    </section>
<?php endif; ?>