<!-- <article <?php post_class() ?>>
  <header>
    <h2 class="entry-title"><a href="<?php echo e(get_permalink()); ?>"><?php echo get_the_title(); ?></a></h2>
    <?php echo $__env->make('partials/entry-meta', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  </header>
  <div class="entry-summary">
    <?php the_excerpt() ?>
  </div>
</article> -->

<?php if(get_field('linkage_link')): ?>
<?php $link = get_field('linkage_link'); ?>
<?php elseif(get_field('redirect_link')): ?>
<?php $link = get_field('redirect_link'); ?>
<?php else: ?>
<?php $link = get_permalink(); ?>
<?php endif; ?>

<?php if(get_field('redirect_link_ntab')==1 || get_field('new_link_window')==1): ?>
<?php $linkNT = 'target=_blank'; ?>
<?php endif; ?>

<article <?php post_class() ?>>
  <div class="bg-gray-light bg-img" href="<?php echo e(get_permalink()); ?>">
      <?php if(get_post_thumbnail_id( get_the_ID())): ?>
      <img src="<?php echo e(wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) )); ?>" alt="<?php echo $post->post_title; ?>" class="lozad">
      <?php else: ?>
      <i class="fal fa-camera-retro"></i>
      <?php endif; ?>
      <?php the_excerpt() ?>
  </div>
    <!-- <?php if(get_post_type() === 'post'): ?>
      <?php echo $__env->make('partials/entry-meta', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?> -->
  <div class="entry-summary">
    <h2 class="text-center entry-title"><a class="white" <?php echo e($linkNT); ?> href="<?php echo e($link); ?>"><?php echo get_the_title(); ?></a></h2>
    <?php the_excerpt() ?>
  </div>
</article>
