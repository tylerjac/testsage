<?php
  $flexible_content = get_field('page_content');
  if($flexible_content){
    foreach($flexible_content as $content) {
      if($content['acf_fc_layout'] == 'carousel_gallery'){?>
        <script src="<?= get_stylesheet_directory_uri() .'/views/fancybox/jquery.fancybox.min.js';?>"></script>
      <?php }
    }
  }
  $popupselector = get_field('popup_selector');
  if($popupselector){
    $podPost = pods('popup', $popupselector->ID); 
    if($podPost->field('content_text_color')){
      $contenttextColor = $podPost->field('content_text_color');
      $contentreset = "style-reset";
    }
    if($podPost->field('content_background_color')){
      $contentbgColor = $podPost->field('content_background_color');
      $buttonColor = 'style="border-color: '.$contenttextColor.';"';
      $iconColor = 'style="color: '.$contenttextColor.';"';
    } 
    if($podPost->field('modal_opacity')){
      $modalOpacity = 'opacity: '.$podPost->field('modal_opacity').';';
    }
    if($podPost->field('modal_background_color')){
      $modalbgColor = 'background-color: '.$podPost->field('modal_background_color').';';
    }
    if($podPost->field('content_opacity')){
      $contentOpacity = 'opacity: '.$podPost->field('content_opacity').';';
    }
    if($podPost->field('center_content') == 1){
      $centerContent = "text-center";
    } else{
      $centerContent = null;
    }
    if($podPost->field('show_image_only') == 1){
      $showImageOnly = "padding: 0;";
    } else{
      $showImageOnly = null;
    }
    if($contentbgColor){
      $contentbgColor = 'background: '.$contentbgColor.';';
    } 
    if($podPost->field('max_width')){
      $maxWidthModal = 'max-width: '.$podPost->field('max_width').'px;';
    }
    if($podPost->field('youtube_video_id')){
      $videoLink = $podPost->field('youtube_video_id');
      $featpopup = null;
    } elseif($podPost->field('local_video_link')){
      $videoLink = $podPost->field('local_video_link');   
      $featpopup = null;
    } elseif($podPost->field('show_image_only') == 1){
      $featpopup = null;
    } else {
      $videoLink = null;
      $featpopup =  get_post_thumbnail_id($popupselector->ID);
    }
    if($podPost->field('video_poster_image')){
      $videoposter = $podPost->field('video_poster_image');
    }
    if($podPost->field('external_link')==1) {
    	$link = $podPost->field('redirect_link');
    }
    if($podPost->field('redirect_link_ntab')==1){
    	$linkNT = 'target=_blank';
    }

    // $linkNT = '';
    // if($podPost->field('external_link')==1) {
    //     if($podPost->field('redirect_link_ntab')==1){
    //         $linkNT = 'target=_blank';
    //     }else{
    //         $linkNT = null;
    //     }
    // }   ?>
    <div id="global-popup" class="global-modal modal">
        <div class="overlay" style="<?=$modalbgColor?> <?=$modalOpacity?>"></div>
        <?php
          if(!empty($videoLink) || $podPost->field('show_image_only') == 1){?>
            <a class="white button global-close" <?php echo $buttonColor ?>><i <?php echo $iconColor ?> class="fas fa-times"></i></a>
            <?php
          } else{}
        ?>
        <div class="background-center modal-content bg-white <?= $centerContent?>" <?php if($videoLink){?> style="<?= $maxWidthModal ?> background-color: transparent; padding:0;"<?php }elseif($featpopup){?>style="<?= $maxWidthModal ?> background-image: url(<?= wp_get_attachment_image_src( $featpopup, 'full')[0]?>)" <?php } else{?> style="<?= $showImageOnly ?><?= $maxWidthModal ?>"<?php }?>>
            <?php
              if($podPost->field('show_image_only') == 1){}
              elseif(empty($videoLink)){?>
                <div class="overlay-image" style="<?= $contentOpacity?> <?= $contentbgColor ?>"></div>
                <a class="white button global-close" <?php echo $buttonColor ?>><i <?php echo $iconColor ?> class="fas fa-times"></i></a>
                <?php
              }
            ?>
            <?php
              if($podPost->field('youtube_video_id')){?>
                <div class="content-div" >
                  <iframe style="width:100%; height: 600px;" width="420" height="600" src="https://www.youtube.com/embed/<?= $videoLink?>">
                  </iframe>
                </div>
                <?php 
              } elseif($podPost->field('local_video_link')){?>
                <div class="content-div" >
                  <video <?php if($videoposter){?> poster="<?= $videoposter['guid'] ?>" <?php } ?> controls loop autoplay muted style="max-width: 100%; min-height: 100%; width: auto; height: auto; background-size:cover; background-position:center">
                    <source src="<?= $podPost->field('local_video_link')?>">
                  </video>
                </div>
                <?php 
              } elseif($podPost->field('show_image_only') == 1){
                if(!empty($podPost->field('show_image_only'))){?> 
                  <a <?php if($link){?> href="<?= $link ?>"<?php } ?> <?= $linkNT ?>><img src="<?= wp_get_attachment_image_src( get_post_thumbnail_id($popupselector->ID), 'full')[0] ?>"/></a>
                  <?php
                }
              } else{?>
                <div class="content-div <?= $contentreset ?>" >
                    <?php 
                      if(!empty($popupselector->post_content)){?>
                        <div <?php if($contenttextColor){?>style="color: <?= $contenttextColor?>" <?php } ?>>
                          <?php echo do_shortcode($popupselector->post_content)?>
                        </div>
                        <?php
                      }
                    ?>
                </div>
                <?php
              }
            ?>
        </div>
    </div>
    <?php wp_reset_postdata();
  }
?>

<footer class="footer" style="background-image: url(<?php echo e($theme_options['footer_bg_img']); ?>);">
  <div class="footer-top">
      <div class="grid-container row">
          <div data-aos="fade-right" class="column small-12 medium-6 large-4">
             <?php if( trim($theme_options['footer_form_title'])!="" ): ?>
              <span><?php echo $theme_options['footer_form_title']; ?></span>
            <?php endif; ?>
            <?php if( trim($theme_options['footer_form'])!="" ): ?>
              <?php echo $theme_options['footer_form']; ?>

            <?php endif; ?>
          </div>
          <div data-aos="fade-up" class="nav-mid border-primary column small-12 medium-6 large-5">
             <?php echo wp_nav_menu( array( 'menu' => 'Primary', 'container'=> false, 'menu_class'=> 'false' ) ); ?>

          </div>
          <div data-aos="fade-in" class="social-container column small-12 medium-6 large-3">
            <?php if(is_array($theme_options['footer_social_profiles'])): ?>
            <div class="sicon text-right">
               <p class="white">FOLLOW US ON SOCIAL MEDIA</p>
                <ul class="text-right">
                  <?php if( trim($theme_options['footer_social_profiles']['facebook'])!="" ): ?>
                            <li><a class="white hover-primary" href="<?php echo e($theme_options['footer_social_profiles']['facebook']); ?>" target="_blank"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                          <?php endif; ?>
                  <?php if( trim($theme_options['footer_social_profiles']['linkedin'])!="" ): ?>
                    <li><a class="white hover-primary" href="<?php echo e($theme_options['footer_social_profiles']['linkedin']); ?>" target="_blank"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a></li>
                          <?php endif; ?>
                  <?php if( trim($theme_options['footer_social_profiles']['instagram'])!="" ): ?>
                    <li><a class="white hover-primary" href="<?php echo e($theme_options['footer_social_profiles']['instagram']); ?>" target="_blank"><i class="fab fa-instagram" aria-hidden="true"></i></a></li>
                          <?php endif; ?>
                  <?php if( trim($theme_options['footer_social_profiles']['twitter'])!="" ): ?>
                    <li><a class="white hover-primary" href="<?php echo e($theme_options['footer_social_profiles']['twitter']); ?>" target="_blank"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                          <?php endif; ?>
                  <?php if( trim($theme_options['footer_social_profiles']['location'])!="" ): ?>
                    <li><a class="white hover-primary" href="<?php echo e($theme_options['footer_social_profiles']['location']); ?>" target="_blank"><i class="fal fa-map-marker-alt" aria-hidden="true"></i></a></li>
                  <?php endif; ?>
                </ul>
            </div> 
          </div>
		<?php endif; ?>
      </div>
  </div>
  <div class="copyright bg-primary">
      <div class="grid-container">
          <div class="copyright-info">
              <div class="row full align-justify align-middle">
                  <div class="column small-12 medium-6 large-4">
                      <?php if(trim($theme_options['footer_copyright_text'])!=""): ?> <p><?php echo $theme_options['footer_copyright_text']; ?></p> <?php endif; ?>
                  </div>
                  <div class="column small-12 medium-6 large-4">
                      <div class="footer-nvabar">
						            <?php echo wp_nav_menu( array( 'menu' => 'footer-menu', 'container'=> false, 'menu_class'=> 'false' ) ); ?>

                      </div>
                  </div>
                  <div class="column small-12 medium-6 large-4">
                    <?php if(is_array($theme_options['footer_credit']) && !empty($theme_options['footer_credit'])): ?>
                    <div class="footer-right">
                        <?php echo $theme_options['footer_credit']['credit_text']; ?> 
                          <?php if($theme_options['footer_credit']['icon'] == ""): ?>
                            <?php credit(); ?>
                          <?php elseif( $theme_options['footer_credit']['link_type'] == 'icon' ): ?>
                             <p><a href="<?php echo e($theme_options['footer_credit']['icon_link']); ?>" target="_blank"><img src="<?php echo e($theme_options['footer_credit']['icon']); ?>" alt="<?php echo e($theme_options['footer_credit']['credit_text']); ?>" class=""></a></p>
                          <?php elseif( $theme_options['footer_credit']['link_type'] == 'text-link' ): ?>
                          <?php if(is_array($theme_options['footer_credit']['credit_link']) && !empty($theme_options['footer_credit']['credit_link'])): ?>
                             <p><span><a href="<?php echo e($theme_options['footer_credit']['credit_link']['url']); ?>" <?php if($theme_options['footer_credit']['credit_link']['target']!=""): ?> target="_blank" <?php endif; ?> ><?php echo e($theme_options['footer_credit']['credit_link']['title']); ?></a></span></p>
                          <?php endif; ?>
                        <?php endif; ?>
                      
                    </div>
                    <?php endif; ?>
                  </div>
              </div>
          </div>                    
      </div>
  </div>
</footer>