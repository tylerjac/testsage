<?php $content = $content['data']; ?>
<?php if(isset($content['hide_section']) && $content['hide_section']!=1): ?>

<?php if( is_array($content['gallery']) && !empty($content['gallery']) ): ?>
	<?php if($content['slider_navigation'] == 'dots'): ?>
<section data-aos="fade-up" class="content-section awards-carousel">
  <div class="grid-container" style="padding: 0;">
    <div class="owl-carousel owl-theme" id="awards-slider">
      <?php $__currentLoopData = $content['gallery']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $img): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	  <div class="item">
        <a href="<?php echo e($img['url']); ?>" data-lity><img src="<?php echo e($img['url']); ?>" class="lozad" alt="<?php echo($img['alt']!='' ? $img['alt'] : 'Foundation Sage Theme'); ?>"></a>
      </div>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
  </div>
</section>
	<?php elseif($content['slider_navigation'] == 'arrow'): ?>
<section data-aos="fade-up" class="content-section culture-bx-slider">
  <div class="grid-container" style="padding: 0;">
    <div class="owl-carousel owl-theme" id="arrowSlider">
      <?php $__currentLoopData = $content['gallery']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $img): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	  <div class="item">
		 <a href="<?php echo e($img['url']); ?>" data-lity><img src="<?php echo e($img['url']); ?>" class="lozad" alt="<?php echo($img['alt']!='' ? $img['alt'] : 'Foundation Sage Theme'); ?>"></a>
      </div> 
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>               
    </div>
  </div>            
</section>
	<?php endif; ?>
<?php endif; ?>

<?php endif; ?>