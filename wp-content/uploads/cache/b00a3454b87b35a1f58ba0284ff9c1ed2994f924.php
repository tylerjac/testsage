<!doctype html>
<html class="splash-page no-js" lang="">
  <head>
        <?php 
    gravity_form_enqueue_scripts(2, true); ?>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Under Development | <?= get_bloginfo(); ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?= get_stylesheet_directory_uri() .'/views/splash/splash.css';?>" />
    <script src="<?= get_stylesheet_directory_uri() .'/views/splash/jquery-min.js';?>"></script>
    <script src="<?= get_stylesheet_directory_uri() .'/views/splash/slick-min.js';?>"></script>
    <script src="<?= get_stylesheet_directory_uri() .'/views/splash/splash.js';?>"></script>
    <script src="https://use.typekit.net/<?= get_option('company_info_typekit_id') ?>.js"></script>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.15.2/css/all.css" integrity="sha384-yJpxAFV0Ip/w63YkZfDWDTU6re/Oc3ZiVqMa97pi8uPt92y0wzeK3UFM2yQRhEom" crossorigin="anonymous">
  </head>
  <body>
    <div class="wrapper">
      <?php 
        $logo = get_field('logo','options');
        if(get_field('slides','options')!="") {
          $slides = get_field('slides','options');
        }
        if(!empty($slides)){?>
          <section class="banner_splash">
            <div id="js-splash-header">
              <?php
                foreach($slides as $slide){
                  $slidebackground = $slide['slide_background_color'];
                  $slideopacity = $slide['slide_background_opacity'];
                  ?>
                  <div class="col" style="background-position: center; background-size: cover; background-image: url(<?= $slide['background_image'] ?>)">
                    <div class="overlay-box" style="opacity: <?= $slideopacity?>; background-color: <?= $slidebackground?>;"></div>
                    <div class="slide-box">
                      <?php 
                        if($slide['hide_logo'] !== 1){?>
                          <img class="logo" src="<?= $logo ?>" alt="logo"/>
                        <?php }
                        if($slide['title']){?>
                          <h1> <?php echo $slide['title'] ?></h1>
                        <?php }
                        if($slide['subtitle']){?>
                          <?php echo wpautop($slide['subtitle']) ?>
                        <?php }
                        if($slide['content']){?>
                          <?php echo do_shortcode($slide['content']) ?>
                        <?php }
                      ?>
                    </div>
                  </div>
                  <?php
                }
              ?>
            </div>
          </section>
          <?php
        }
      ?>
    </div><!--/.wrapper-->
  </body>
</html>

