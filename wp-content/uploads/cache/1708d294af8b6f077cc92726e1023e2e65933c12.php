<?php $content = $content['data']; ?>
<?php if(isset($content['hide_section']) && $content['hide_section']!=1): ?>
<section class="connect">
	<?php echo $content['background_map']; ?>

	<div class="container">
        <div class="connect-info">
            <?php if(trim($content['location'])!="" || trim($content['contact'])!="" || trim($content['hours'])!=""): ?>
            <div class="connect-info-left">
                <?php if(trim($content['location'])!=""): ?>
                <div class="connect-info-left-row">
                    <span>LOCATION</span>
                    <?php echo $content['location']; ?>

                </div>
                <?php endif; ?>
                <?php if(trim($content['contact'])!=""): ?>
                <div class="connect-info-left-row">
                    <span>CONTACT</span>
                    <?php echo $content['contact']; ?>

                </div>
                <?php endif; ?>
                <?php if(trim($content['hours'])!=""): ?>
                <div class="connect-info-left-row">
                    <span>HOURS</span>
                    <?php echo $content['hours']; ?>

                </div>
                <?php endif; ?>
            </div>
            <?php endif; ?>

            <div class="connect-info-right">
                <div class="container title-black">
                    <?php if(trim($content['heading'])!=""): ?>
                    <h3><?php echo $content['heading']; ?></h3>
                    <?php endif; ?>
                    <?php if(trim($content['sub_heading'])!=""): ?>
                    <p><?php echo $content['sub_heading']; ?></p>
                    <?php endif; ?>
                    <?php if(trim($content['form'])!=""): ?>
                        <?php echo $content['form']; ?>

                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>