<?php $content = $content['data']; ?>
<?php if(isset($content['hide_section']) && $content['hide_section']!=1): ?>

<?php if($content['content_align']=='Left'): ?>
  <section class="our-team" style="background-image: url(<?php echo e($content['background_image']); ?>);">
    <div class="grid-container full">
      <div class="row ">
        <div class="column small-12 medium-6 large-7">
          <?php if(trim($content['heading'])!="" || trim($content['sub_heading'])!=""): ?>
		  <div class="title-white">
            <?php if(trim($content['heading'])!=""): ?>
			<h2><?php echo $content['heading']; ?></h2>
			<?php endif; ?>
            <?php if(trim($content['sub_heading'])!=""): ?>
			<h6><?php echo $content['sub_heading']; ?></h6>
			<?php endif; ?>
          </div>
          <?php endif; ?>
		  <div class="content">
            <?php if(trim($content['description'])!=""): ?>
			<?php echo $content['description']; ?>

            <?php endif; ?>
			<?php if(is_array($content['button']) && !empty($content['button'])): ?>
			<div class="btn-custom">
              <a href="<?php echo e($content['button']['url']); ?>" class="btn-white-black" data-text="<?php echo e($content['button']['title']); ?>" <?php if($content['button']['target']!=''): ?> target="_blank" <?php endif; ?> ><span><?php echo e($content['button']['title']); ?></span></a>
            </div>
			<?php endif; ?>
			<?php if(trim($content['instagram_id'])!=""): ?>
			<div class="instalink">
                <a href="<?php echo e('//www.instagram.com/' . $content['instagram_id']); ?>" target="_blank"><i class="fab fa-instagram"></i><?php echo e('@'. $content['instagram_id']); ?></a>
            </div>
			<?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>

<?php if($content['content_align']=='Right'): ?>
  <section class="our-story" style="background-image: url(<?php echo e($content['background_image']); ?>);">
    <div class="grid-container full h-100">
      <div class="row align-right">
        <div class="column small-12 medium-5 large-7">
          <div class="our-story-info">
            <?php if(trim($content['heading'])!="" || trim($content['sub_heading'])!=""): ?>
			<div class="title-white">
              <?php if(trim($content['heading'])!=""): ?>
			  <h2><?php echo $content['heading']; ?></h2>
			  <?php endif; ?>
			  <?php if(trim($content['sub_heading'])!=""): ?>
			  <h6><?php echo $content['sub_heading']; ?></h6>
			  <?php endif; ?>
            </div>
			<?php endif; ?>
            <div class="content">
              <?php if(trim($content['description'])!=""): ?>
			  <?php echo $content['description']; ?>

              <?php endif; ?>
			  <?php if(is_array($content['button']) && !empty($content['button'])): ?>
              <div class="btn-custom">
                <a href="<?php echo e($content['button']['url']); ?>" class="btn-gray" data-text="<?php echo e($content['button']['title']); ?>" <?php if($content['button']['target']!=''): ?> target="_blank" <?php endif; ?> ><span><?php echo e($content['button']['title']); ?></span></a>
              </div>
			  <?php endif; ?>
			  <?php if(trim($content['instagram_id'])!=""): ?>
              <div class="instalink">
                <a href="<?php echo e('//www.instagram.com/' . $content['instagram_id']); ?>" target="_blank"><i class="fab fa-instagram"></i><?php echo e('@'. $content['instagram_id']); ?></a>
              </div>
			  <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>

<?php endif; ?>