var ugm_autocomplete = [];

jQuery(function($) {

	var marker_added = false;
	var marker;

	$(document).ready(function(){
		if(typeof wpgmza_vgm_localized.vgm_from_shortcode !== 'undefined' && parseInt(wpgmza_vgm_localized.vgm_from_shortcode) === 1){
			$(document.body).trigger("standalone_vgm.wpgmza");
		}
	});

	$(document.body).on("init.wpgmza standalone_vgm.wpgmza", function(event) {
		
		//if (typeof window.google !== "undefined" && typeof window.google.maps !== "undefined" && window.google.maps.Geocoder !== "undefined") {
			
		var geocoder;
		if(WPGMZA.Geocoder)
			geocoder = WPGMZA.Geocoder.createInstance();
		else
			geocoder = new google.maps.Geocoder();
		
		var touch_fix = ("ontouchstart" in window) || /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || jQuery(window).width() < 1000 ? 'click' : 'rightclick'; //Add mobile touch/tap support
		
		function initMapUGM(entry, legacy) {
			var mapId = false;
			var map = false;

			if(!legacy){
				if(typeof wpgmza_vgm_localized.vgm_from_shortcode !== 'undefined' && parseInt(wpgmza_vgm_localized.vgm_from_shortcode) === 1){
					map = null;
					mapId = entry;
				} else {
					map = WPGMZA.maps[entry];
					mapId = WPGMZA.maps[entry].id;
				}
			} else {
				map = MYMAP[entry].map;
				mapId = entry;
			}

			if(mapId === false || map === false){
				return;
			}

			var ugmElementExists = document.getElementById('wpgmza_ugm_add_address_' + mapId);

			if (ugmElementExists !== null && window.WPGMZA && WPGMZA.settings && WPGMZA.settings.engine == "google-maps") {
				
				ugm_autocomplete[mapId] = new google.maps.places.Autocomplete( (document.getElementById('wpgmza_ugm_add_address_' + mapId) ), {fields: ["name", "formatted_address"], types: ['geocode']});
				google.maps.event.addListener(ugm_autocomplete[mapId], 'place_changed', function() {
					ugmFillInAddress(mapId);
				});
			}
			
			// NB: Plaster for custom fields not submitted, need a proper solution (server side) at some point
			$("form.wpgmaps_user_form [data-ajax-name]").each(function(index, el) {
				
				var name = $(el).attr("data-ajax-name");
				$(el).attr("name", name);
				$(el).removeAttr("data-ajax-name");
				
			});

			var callback = function(event) {

				if(window.WPGMZA && event.target instanceof WPGMZA.Marker){
					return;	// Clicking on existing marker
				}
				
				var updateAddressField = function(event) {
					var value;
					if(event.latLng instanceof WPGMZA.LatLng || (window.google && window.google.maps && event.latLng instanceof google.maps.LatLng)){
						value = event.latLng.toString().replace(/[()]/g, "");
					} else {
						value = event.latLng.lat + ", " + event.latLng.lng;
					}
					
					jQuery("#wpgmza_ugm_add_address_"+mapId).val(value);
					
				};
				
				var latLng, marker;
				
				if(window.google && window.google.maps && event.latLng instanceof google.maps.LatLng){
					latLng = {
						lat: event.latLng.lat(),
						lng: event.latLng.lng()
					};
				} else {
					latLng = event.latLng;
				}
				
				if (!map.ugmMarker) {
					
					var options = {
						position: latLng,
						draggable: true,
						userCreated : true
					};
					
					if(map instanceof WPGMZA.Map)
						options.map = map;
						
					marker = WPGMZA.Marker.createInstance(options);
					
					if(window.google && window.google.maps && map instanceof google.maps.Map)
						marker.googleMarker.setMap(map);
					
					if(marker instanceof WPGMZA.Marker)
						marker.on("dragend", updateAddressField);
					else
						google.maps.event.addListener(marker, "dragend", updateAddressField);
					
					map.ugmMarker = marker;
					
					marker_added = true;
					
				} else {
					marker = map.ugmMarker;
				}
				
				marker.setPosition(latLng);
				updateAddressField(event);
			};
			
			/*if(window.google)
				google.maps.event.addListener(map, touch_fix, callback);
			else
				map.on(touch_fix, callback);*/
			
			if(map instanceof WPGMZA.Map){
				map.on(touch_fix, callback);
			} else {
				if(map){
					google.maps.event.addListener(map, touch_fix, callback);
				}
			}
			
			// Prevent click event on polygons
			if(map){
				var map = map;
			
				if(window.google) {
					if(typeof WPGM_Path_Polygon !== "undefined" && typeof WPGM_Path_Polygon[entry] !== "undefined"){
                    	for(var poly_id in WPGM_Path_Polygon){
                        	WPGM_Path_Polygon[poly_id].setOptions({clickable: false});
						}
					}
				} else {
					map.polygons.forEach(function(polygon) {
						polygon.setOptions({
							clickable: false
						});
					});
				}
			}
		}
		 
		jQuery('#wpgmza_ugm_addmarker').click(function(){
			var $ = jQuery;
			
			form = document.forms['wpgmaps_ugm'];
			
			var gdprConsent = $(form).find("input[name='wpgmza_ugm_gdpr_consent']");
			if(gdprConsent.length && !gdprConsent.prop("checked"))
			{
				alert(WPGMZA.localized_strings.you_must_check_gdpr_consent);
				return;
			}
			
			if($("input[name='gdpr-agreement']").length && !$("input[name='gdpr-agreement']").prop("checked"))
			{
				alert("You must agree to the GDPR terms");
				return;
			}
				
			var isChecked = jQuery('#wpgmza_ugm_spm:checked').val()?true:false;
			if (!isChecked) { alert(wpgmza_vgm_localized.vgm_human_error_string); return; }
			var thismapid = jQuery("#wpgmza_ugm_map_id").val();
			jQuery('#wpgmza_ugm_addmarker').hide();
			jQuery('#wpgmza_ugm_addmarker_loading').show();
			var wpgm_address = '0';
			if (document.getElementsByName('wpgmza_ugm_add_address').length > 0) { wpgm_address = jQuery('#wpgmza_ugm_add_address_'+thismapid).val(); }

			/* first check if user has added a GPS co-ordinate */
			checker = wpgm_address.split(",");
			var wpgm_lat = "";
			var wpgm_lng = "";
			wpgm_lat = checker[0];
			wpgm_lng = checker[1];
			checker1 = parseFloat(checker[0]);
			checker2 = parseFloat(checker[1]);
			if ((wpgm_lat.match(/[a-zA-Z]/g) === null && wpgm_lng && wpgm_lng.match(/[a-zA-Z]/g) === null) && checker.length === 2 && (checker1 != NaN && (checker1 <= 90 || checker1 >= -90)) && (checker2 != NaN && (checker2 <= 90 || checker2 >= -90))) {
				jQuery("#wpgmza_ugm_lat").val(wpgm_lat);
				jQuery("#wpgmza_ugm_lng").val(wpgm_lng);
				form.submit();
				return true;
				
			} else {			
				try{
					geocoder.geocode( { 'address': wpgm_address}, function(results, status) {
						var success = (window.WPGMZA && WPGMZA.Geocoder ? WPGMZA.Geocoder.SUCCESS : google.maps.GeocoderResult.OK);
						
						if (status === success) {
							var wpgm_lat, wpgm_lng;
							
							wpgm_gps = String(results[0].geometry.location);
							
							if(typeof results[0].geometry.location == "object")
							{
								wpgm_lat = results[0].geometry.location.lat;
								wpgm_lng = results[0].geometry.location.lng;
							}
							else
							{
								var latlng1 = wpgm_gps.replace('(','');
								var latlng2 = latlng1.replace(')','');
								var latlngStr = latlng2.split(',',2);
								wpgm_lat = parseFloat(latlngStr[0]);
								wpgm_lng = parseFloat(latlngStr[1]);
							}
							

							jQuery("#wpgmza_ugm_lat").val(wpgm_lat);
							jQuery("#wpgmza_ugm_lng").val(wpgm_lng);




							form.submit();
							return true;

						} else {
							alert('The address you used could not be geocoded. Please use another address: ' + status);
									jQuery('#wpgmza_ugm_addmarker').show();
									jQuery('#wpgmza_ugm_addmarker_loading').hide();
									return false;
									}
					});
				}catch(e) {
					alert(e.message);
				}
			}
		
		});
		
		if(!window.wpgmaps_localize && !WPGMZA.maps){
			return;
		}
		
		if(WPGMZA.maps){
			if(WPGMZA.maps.length > 0){
				for (var entry in WPGMZA.maps) {
					initMapUGM(entry, false);	
				}
			} else {
				if(typeof wpgmza_vgm_localized.vgm_from_shortcode !== 'undefined' && parseInt(wpgmza_vgm_localized.vgm_from_shortcode) === 1){
					//From Shortcode 
					var wpgmza_vgm_shortcode_mapid = $('#wpgmza_ugm_map_id').val();
					initMapUGM(parseInt(wpgmza_vgm_shortcode_mapid), false);	

				}
			}
		} else {
			//Legacy
			for (var entry in wpgmaps_localize) {
				initMapUGM(entry, true);	
			}
		}
		
		function showSubmissionMessage()
		{
			var $ = jQuery;
			
			var value = Cookies.get('wpgmza_ugm_submission_result');
			if(!value)
				return;

			var decoded = atob(value);
			var cookie = JSON.parse(decoded);
			
			var data = cookie.marker;
			
			var map = WPGMZA.getMapByID(data.map_id);
			var marker;
			
			console.log(map);
			
			if(!map){
				if(typeof wpgmza_vgm_localized.vgm_from_shortcode === 'undefined' && parseInt(wpgmza_vgm_localized.vgm_from_shortcode) !== 1){
					return;
				}
			}
			
			if(map){
				$(map.element).before($("<div class='wpgmza-ugm-submission-message'>" + cookie.message + "</div>"));
				
				if(data.approved == 1){
					marker = map.getMarkerByID(data.id);
				} else {
					marker = WPGMZA.Marker.createInstance(data);
					map.addMarker(marker);
				}
				
				if(WPGMZA.settings.zoom_after_ugm_submission == 1){
					marker.panIntoView();
					marker.openInfoWindow();
					map.setZoom(13);
				}
			} else {
				$('.wpgmaps_user_form').before($("<div class='wpgmza-ugm-submission-message'>" + cookie.message + "</div>"));
			}
			
			
			
			Cookies.remove('wpgmza_ugm_submission_result');
		}
		
		showSubmissionMessage();
		
	});
});

function ugmFillInAddress(mid) {
  
  var place = ugm_autocomplete[mid].getPlace();
}

