<?php
/*
Plugin Name: WP Google Maps - Visitor Generated Markers
Plugin URI: http://www.wpgmaps.com
Description: This is an add-on for WP Google Maps that allows your visitors to create and input markers on your maps.
Version: 3.32
Author: WP Google Maps
Author URI: http://www.wpgmaps.com
*/

/*
 * 3.32 :- 2021-06-03 :- Low priority 
 * Fixed issue where script variables were not being localized the way WordPress core expects
 * Fixed issue where VGM marker would not be shown if 'hide markers until search is done' is enabled
 * Fixed issue where VGM GDPR consent checkbox could not be enabled
 * 
 * 3.31 :- 2021-02-17 :- Medium priority
 * Fixed issue where global setting toggles would not save correctly when disabled
 * Fixed issue where standalone VGM shortcode would not initialize JavaScript, preventing form submissions entirely 
 * Fixed issue where 'Add marker' button was using an admin button style, causing issues with text colors
 * Fixed issue with script loading with standalone VGM form
 * Added option for modern form layouts, this will convert table layout to a more responsive layout overall
 *
 * 3.30 :- 2020-11-20 :- Medium priority
 * Add core 8.1 compatibility
 * Added support for WP5.6 
 * Added support for jQuery 3
 *
 * 3.23 :- 2020-10-14 :- Medium priority
 * Fixed bug where multiple categories could not be submitted with checkbox mode enabled
 * Fixed Custom fields in the form are clunky
 * 
 * 3.22
 * Filter wpgmza_ugm_submission_email_fields now receives marker instance
 * Fixed SQL error in wpgmza_register_ugm_version when adding approval column
 *
 * 3.21 :- 2020-04-07 :- Medium priority
 * Dropped call to deprecated function wpgmaps_filter
 * Fixed only one category assigned to submitted markers when multiple categories have been selected by the user
 * Fixed custom field data not being submitted
 *
 * 3.20 :- 2020-02-12 :- Medium priority
 * Fixed incorrect name on category dropdown causing category to be lost
 * Fixed fatal error when only one category is selected for submission
 *
 * 3.19 :- 2020-01-24 :- Medium priority
 * Increased maximum length of link field to 2083 characters
 * Fixed category selection controls broken when running Pro >= 8.0.19
 *
 * 3.18 :- 2019-07-11 :- High priority
 * Closed potential security vulnerability
 * 
 * 3.17 :- 2019-07-02 :- Medium priority
 * Fixed JS error with separate form
 * Fixed separate form submission not working
 *
 * 3.16 :- 2019-06-06 :- Medium priority
 * Fixed cannot insert marker when description disabled
 * Fixed cannot submit marker anonymously
 * 
 * 3.15 - 2019-06-03 :- Medium priority
 * Fixed fatal error calling getNoticeHTML on certain installations
 *
 * 3.14 - 2019-05-29 :- Medium priority
 * Added action wpgmza_ugm_marker_submitted
 * Now compatible with jQuery 3.x.x
 * Dropped deprecated window load function
 * Fixed false positives on "Possible file upload attack"
 *
 * 3.13 - 2019-05-23 :- Medium priority
 * Added description show/hide setting
 * Added form text override settings
 * Added filter wpgmza_ugm_submission_email_fields
 * Added code to center zoom on new UGM marker
 * Optimized Google autocomplete to only request title and formatted address
 * Fixed Marker not visible till after refresh
 * Fixed GDPR consent check not working on front end
 * Fixed translation functions not being properly applied to UGM approval / notification email
 *
 * 3.12 - 2019-04-16 :- Medium priority
 * Fixed wpgmaps_localize is undefined in Gutenberg editor
 * Fixed administrator e-mail not being saved by settings page
 *
 * 3.11 - 2019-04-08 :- Medium priority
 * Fixed infowindows not opening on maps with VGM form enabled
 *
 * 3.10 - 2019-04-02 :- Medium priority
 * Added classnames to front end form rows
 * Changed marker_added check to map.ugmMarker
 * Fixed right click functionality not working for OpenLayers
 *
 * 3.09 - 2018-12-20 :- Low priority
 * Added link field to UGM form
 * Fixed standalone form not submitting markers
 *
 * 3.08 - 2018-07-24 :- Medium priority
 * Fixed incompatibilities with Pro 5 breaking right click marker
 *
 * 3.07 - 2018-07-05 :- Low priority
 * Added support for new GDPR settings
 * Fixed deprecated jQuery window load function
 * Fixed shortcode echo instead of returning
 * Fixed "are you human" checkbox
 * Fixed "no address specified" failing silently by adding try catch block
 *
 * 3.06 - 2018-06-11
 * Fixed apostraphe in "marker title" translation breaking HTML attribute
 *
 * 3.05 - 2018-06-08
 * Improved support for touch screen devices
 * Solved issues with OpenLayers
 *
 * 3.04 - 2018-06-03
 * Fixed undefined variable ugm_display notice
 *
 * 3.03 - 2018-05-30
 * Fixed geocode result coordinates not interpreted properly
 *
 * 3.02 - 2018-05-25
 * Added compatibility with OpenLayers engine
 * GDPR compliance
 *
 * 3.01 - 2018-04-05
 * Added support for custom fields
 *
 * 3.00 - 2017-01
 * New Feature:  Auto-Complete Added to User Form
 * Bug Fix: Right Clicking on Polygon now add's marker to map
 * Bug Fix: Changed jQuery event from 'document ready' to 'window load' as this was causing issues with the JS
 * Moved the settings into its own tab within the map builder
 * Created a shortcode that allows the separation of the HTML form and the map
 * You can now disable the display of the VGM form by using disable_vgm_form='1' within the shortcode (handy when having the VGM form and map on separate page)
 * Added a notice that informs a user that their markers have been added and that it will be pending approval (relevant to the appropriate setting)
 * The map will now center and zoom in on a users marker once it has been added (and the auto approval setting is enabled)
 * 
 * 2.8 - 2016-07-15 - Medium priority
 * Removed the saved email address from within the localized JS array on the front end
 * New Feature:  Mobile touch/tap support added. Users can now add a marker from there mobile device (Device less than 1000px)
 * 
 * 2.7 - 2016-01-07 - Low priority
 * Settings bug fix
 * 
 * 2.6 - 2015-  - Low priority
 * Rocketscript fix (Cloudfare)
 * 
 * 2.5
 * New feature: You can now choose if you want to be notified via email of new user submitted markers
 * 
 * 2.4 2015-01-27
 * Added the ability for users to right click on the map to add a marker - and to drag it
 * Added an option to enable/disable auto-approval of visitor generated markers
 * 
 * 2.3 2014-11-04
 * User generated markers must now be approved before being put live
 * Bug fixes
 * 
 * 2.2 2014-09-29
 * Fixed the bug that wasnt saving categories for user generated markers
 * Code improvements (PHP warnings)
 * 
 * 2.1
 * Code improvements (PHP warnings)
 * 
 * 2.0
 * Code improvements
 * Users can now upload images with their markers
 * 
 * 1.9 Small bug fix
 * 
 * 1.8
 * Fixed a conflict with the NextGen plugin
 * 
 * 1.7
 * Fixed an error output bug
 * 
 * 1.6
 * Fixed a bug that stopped you from selecting "registered users"
 * 
 * 1.5
 * JS now included in it's own file and not in-line
 * Users can now select a category for their marker - you also have the option to enable or disable this
 * Coming soon:
 *  * Ability for users to add images
 *  * Ability for users to add icons
 * 
 * 1.4
 * Added user access roles (Who can add markers)
 * 
 * 1.3
 * This version allows the plugin to update itself moving forward
 * 
 * 1.2
 * Fixed a bug that was stopping the plugin from working on IIS servers
 * 
 * 1.1
 * Fixed a bug whereby users that weren't logged in couldnt add a marker
 *
 * 1.0
 * Released: 28 August 2012
 *
 *
 *
 */

global $wpgmza_ugm_version;
global $wpgmza_ugm_marker_added_string;

// $wpgmza_ugm_version = "3.10";
$wpgmza_ugm_string = "ugm";

$subject = file_get_contents(__FILE__);
if(preg_match('/Version:\s*(.+)/', $subject, $m))
	$wpgmza_ugm_version = trim($m[1]);

register_activation_hook( __FILE__, 'wpgmaps_ugm_activate' );
register_deactivation_hook( __FILE__, 'wpgmaps_ugm_deactivate' );
add_action('init', 'wpgmza_register_ugm_version');
//add_action('admin_head', 'wpgmaps_head_ugm');
// add_action('init', 'wpgmaps_user_head_ugm');

function wpgmaps_ugm_activate() { wpgmza_cURL_response_ugm("activate"); }
function wpgmaps_ugm_deactivate() { wpgmza_cURL_response_ugm("deactivate"); }

add_action('wp_ajax_ugm_add_marker', 'wpgmaps_action_callback_ugm');
add_action('wp_ajax_nopriv_ugm_add_marker', 'wpgmaps_action_callback_ugm');

function wpgmza_register_ugm_version() {
    global $wpgmza_ugm_version;
    global $wpgmza_ugm_string;
    if (!get_option('WPGMZA_UGM')) {
        add_option('WPGMZA_UGM',array("version" => $wpgmza_ugm_version, "version_string" => $wpgmza_ugm_string));
    }
    
    $current_version = get_option('WPGMZA_UGM');
    if ($current_version['version'] != $wpgmza_ugm_version) {
        /* versions differ */
        
        // check if "approved" column exists in marker table
        global $wpgmza_tblname;
        global $wpdb;
        $results = $wpdb->get_results("DESC $wpgmza_tblname");
        $founded = 0;
        foreach ($results as $row ) {
            if ($row->Field == "approved") {
                $founded++;
            }
        }
        if ($founded < 1) { echo $wpdb->query("ALTER TABLE $wpgmza_tblname ADD COLUMN approved TINYINT(1) DEFAULT '1'"); }
    }
    
}


function wpgmza_cURL_response_ugm($action) {
    if (function_exists('curl_version')) {
        global $wpgmza_ugm_version;
        global $wpgmza_ugm_string;
        $request_url = "http://www.wpgmaps.com/api/rec.php?action=$action&dom=".$_SERVER['HTTP_HOST']."&ver=".$wpgmza_ugm_version.$wpgmza_ugm_string;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $request_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
    }

}

add_shortcode( 'wpgmza_vgm_form', 'wpgmaps_tag_vgm_form' );
/**
 * Handle the VGM Form shortcode
 * The shortcode attributes are identified and the relevant data is localized and the JS file enqueued
 * @param  array    $atts   array of shortcode attributes
 * @return void
 */
function wpgmaps_tag_vgm_form( $atts ) {
    global $wpgmza_version;
    extract( shortcode_atts( array(
        'link_to_map' => '1',
        'redirect_to' => ''
    ), $atts ) );

    
    $ret_msg = "";
    if ( isset( $atts['link_to_map'] ) ) { $wpgmza_link_to_map = $atts['link_to_map']; } 
    if ( isset( $atts['redirect_to'] ) ) { $wpgmza_redirect_page = $atts['redirect_to']; } else { $wpgmza_redirect_page = ''; }
    if (!isset( $atts['link_to_map'] ) || $atts['link_to_map'] == '') { return 'Attribute "link_to_map" missing. Please add this attribute'; }
    
    return wpgmaps_ugm_user_form( $wpgmza_link_to_map, $wpgmza_redirect_page, true );

}



add_filter("wpgmaps_filter_pro_map_editor_tabs","wpgmaps_vgm_filter_control_pro_map_editor_tabs",10,1);
function wpgmaps_vgm_filter_control_pro_map_editor_tabs($content) {
    $content .= "<li style='margin-right: 3px;'><a href=\"#tabs-vgm\">".__("VGM","wp-google-maps")."</a></li>";
    return $content;
}



add_filter("wpgmaps_filter_pro_map_editor_tab_content","wpgmaps_vgm_filter_control_pro_map_editor_tab_content",10,1);
function wpgmaps_vgm_filter_control_pro_map_editor_tab_content($content) {
	
	global $wpgmza;
	
    $map_id = !empty($_GET['map_id']) ? sanitize_text_field($_GET['map_id']) : false;

    if(!empty($map_id)){
        $res = wpgmza_get_map_data($map_id);

        if ($res->ugm_enabled) { $wpgmza_ugm_enabled[$res->ugm_enabled] = "SELECTED"; } else { $wpgmza_ugm_enabled[2] = "SELECTED"; }
        if ($res->ugm_category_enabled) { $wpgmza_ugm_category_enabled[$res->ugm_category_enabled] = "SELECTED"; } else { $wpgmza_ugm_category_enabled[2] = "SELECTED"; }
        if ($res->ugm_access) { $wpgmza_ugm_access[$res->ugm_access] = "SELECTED"; } else { $wpgmza_ugm_access[2] = "SELECTED"; }

        for ($i=0;$i<3;$i++) {
            if (!isset($wpgmza_ugm_enabled[$i])) { $wpgmza_ugm_enabled[$i] = ""; }
            if (!isset($wpgmza_ugm_category_enabled[$i])) { $wpgmza_ugm_category_enabled[$i] = ""; }
            if (!isset($wpgmza_ugm_access[$i])) { $wpgmza_ugm_access[$i] = ""; }
        }
        
        $map_other_settings = maybe_unserialize($res->other_settings);
        if (isset($map_other_settings['wpgmza_ugm_upload_images'])) { $wpgmza_ugm_upload_images[intval($map_other_settings['wpgmza_ugm_upload_images'])] = "SELECTED"; } else { $wpgmza_ugm_upload_images[2] = "SELECTED"; }
        if (!empty($map_other_settings['wpgmza_ugm_link_enabled'])) { $wpgmza_ugm_link_enabled[intval($map_other_settings['wpgmza_ugm_link_enabled'])] = "SELECTED"; } else { $wpgmza_ugm_link_enabled[2] = "SELECTED"; }
    	if (!empty($map_other_settings['wpgmza_ugm_desc_enabled'])) { $wpgmza_ugm_desc_enabled[intval($map_other_settings['wpgmza_ugm_desc_enabled'])] = "SELECTED"; } else { $wpgmza_ugm_desc_enabled[1] = "SELECTED"; }

        for ($i=0;$i<3;$i++) {
            if (!isset($wpgmza_ugm_upload_images[$i])) { $wpgmza_ugm_upload_images[$i] = ""; }
            if (!isset($wpgmza_ugm_link_enabled[$i])) { $wpgmza_ugm_link_enabled[$i] = ""; }
    	    if (!isset($wpgmza_ugm_desc_enabled[$i])) { $wpgmza_ugm_desc_enabled[$i] = ""; }
        }

    	$wpgmza_ugm_form_header = !empty($map_other_settings['wpgmza_ugm_form_header']) ? stripslashes($map_other_settings['wpgmza_ugm_form_header']) : '';
    	$wpgmza_ugm_form_title = !empty($map_other_settings['wpgmza_ugm_form_title']) ? stripslashes($map_other_settings['wpgmza_ugm_form_title']) : '';
    	$wpgmza_ugm_form_title_ph = !empty($map_other_settings['wpgmza_ugm_form_title_ph']) ? stripslashes($map_other_settings['wpgmza_ugm_form_title_ph']) : '';
    	$wpgmza_ugm_form_address = !empty($map_other_settings['wpgmza_ugm_form_address']) ? stripslashes($map_other_settings['wpgmza_ugm_form_address']) : '';
    	$wpgmza_ugm_form_address_ph = !empty($map_other_settings['wpgmza_ugm_form_address_ph']) ? stripslashes($map_other_settings['wpgmza_ugm_form_address_ph']) : '';
    	$wpgmza_ugm_form_address_help = !empty($map_other_settings['wpgmza_ugm_form_address_help']) ? stripslashes($map_other_settings['wpgmza_ugm_form_address_help']) : '';
    	$wpgmza_ugm_form_desc = !empty($map_other_settings['wpgmza_ugm_form_desc']) ? stripslashes($map_other_settings['wpgmza_ugm_form_desc']) : '';
    	$wpgmza_ugm_form_link = !empty($map_other_settings['wpgmza_ugm_form_link']) ? stripslashes($map_other_settings['wpgmza_ugm_form_link']) : '';
    	$wpgmza_ugm_form_link_ph = !empty($map_other_settings['wpgmza_ugm_form_link_ph']) ? stripslashes($map_other_settings['wpgmza_ugm_form_link_ph']) : '';
    	$wpgmza_ugm_form_image = !empty($map_other_settings['wpgmza_ugm_form_image']) ? stripslashes($map_other_settings['wpgmza_ugm_form_image']) : '';
    	$wpgmza_ugm_form_category = !empty($map_other_settings['wpgmza_ugm_form_category']) ? stripslashes($map_other_settings['wpgmza_ugm_form_category']) : '';
    	
        $content .= "<div id='tabs-vgm'>";
        $content .= "
                <h2 style=\"padding-top:0; margin-top:0;\">".__("Visitor Generated Markers - Settings","wp-google-maps")."</h2>
                <p></p>
                        <table>
                        <input type=\"hidden\" name=\"wpgmza_map_id\" id=\"wpgmza_map_id\" value=\"".$map_id."\" />
                        <input type=\"hidden\" name=\"wpgmza_save_ugm_settings\" id=\"wpgmza_save_ugm_settings\" value=\"1\" />
                        
                            <tr style='margin-bottom:10px;'>
                                <td>".__("Enable Visitor Generated Markers?","wp-google-maps")."</td>
                                <td>
                                    <select id='wpgmza_ugm_enbaled' name='wpgmza_ugm_enbaled'>
                                        <option value=\"1\" ".$wpgmza_ugm_enabled[1].">".__("Yes","wp-google-maps")."</option>
                                        <option value=\"2\" ".$wpgmza_ugm_enabled[2].">".__("No","wp-google-maps")."</option>
                                    </select>
                                </td>
                             </tr>
                            <tr style='margin-bottom:10px;'>
                                <td>".__("Who can add markers?","wp-google-maps")."</td>
                                <td>
                                    <select id='wpgmza_ugm_access' name='wpgmza_ugm_access'>
                                        <option value=\"1\" ".$wpgmza_ugm_access[1].">".__("Everyone","wp-google-maps")."</option>
                                        <option value=\"2\" ".$wpgmza_ugm_access[2].">".__("Registered Users","wp-google-maps")."</option>
                                    </select>
                                </td>
                             </tr>
                            <tr style='margin-bottom:20px;'>
                                <td>".__("Allow users to add a description?","wp-google-maps")."</td>
                                <td>
                                    <select id='wpgmza_ugm_desc_enabled' name='wpgmza_ugm_desc_enabled'>
                                        <option value=\"1\" ".$wpgmza_ugm_desc_enabled[1].">".__("Yes","wp-google-maps")."</option>
                                        <option value=\"2\" ".$wpgmza_ugm_desc_enabled[2].">".__("No","wp-google-maps")."</option>
                                    </select>
                                </td>
                             </tr>
                            <tr style='margin-bottom:20px;'>
                                <td>".__("Allow users to select a marker category?","wp-google-maps")."</td>
                                <td>
                                    <select id='wpgmza_ugm_category_enbaled' name='wpgmza_ugm_category_enbaled'>
                                        <option value=\"1\" ".$wpgmza_ugm_category_enabled[1].">".__("Yes","wp-google-maps")."</option>
                                        <option value=\"2\" ".$wpgmza_ugm_category_enabled[2].">".__("No","wp-google-maps")."</option>
                                    </select>
                                </td>
                             </tr>
                            <tr style='margin-bottom:20px;'>
                                <td>".__("Allow users to upload images?","wp-google-maps")."</td>
                                <td>
                                    <select id='wpgmza_ugm_upload_images' name='wpgmza_ugm_upload_images'>
                                        <option value=\"1\" ".$wpgmza_ugm_upload_images[1].">".__("Yes","wp-google-maps")."</option>
                                        <option value=\"2\" ".$wpgmza_ugm_upload_images[2].">".__("No","wp-google-maps")."</option>
                                    </select>
                                </td>
                             </tr>
                             <tr style='margin-bottom:20px;'>
                                <td>".__("Allow users to add a link to markers?","wp-google-maps")."</td>
                                <td>
                                    <select id='wpgmza_ugm_link_enabled' name='wpgmza_ugm_link_enabled'>
                                        <option value=\"2\" ".$wpgmza_ugm_link_enabled[2].">".__("No","wp-google-maps")."</option>
                                        <option value=\"1\" ".$wpgmza_ugm_link_enabled[1].">".__("Yes","wp-google-maps")."</option>
                                    </select>
                                </td>
                             </tr>
                             <tr style='margin-bottom:20px;'>
                             <td>".__("Zoom in after UGM submission","wp-google-maps")."</td>
                             <td>
                                 <select id='wpgmza_zoom_after_ugm_submission' name='zoom_after_ugm_submission'>
                                     <option value=\"0\" " . (empty($map_other_settings['zoom_after_ugm_submission']) || $map_other_settings['zoom_after_ugm_submission'] == '0' ? 'selected="selected"' : '') . ">".__("No","wp-google-maps")."</option>
                                     <option value=\"1\" " . (!empty($map_other_settings['zoom_after_ugm_submission']) && $map_other_settings['zoom_after_ugm_submission'] == 1 ? 'selected="selected"' : '') . ">".__("Yes","wp-google-maps")."</option>
                                 </select>
                             </td>
                          </tr>
                             
                         </table>
                         <h2>".__('Form Text Overrides','wp-google-maps')."</h2>
                         <table>
                         	<tr>
                         		<td>".__('Add your own marker', 'wp-google-maps')."</td>
                         		<td><input type='text' size='45' id='wpgmza_ugm_form_header' name='wpgmza_ugm_form_header' value='".esc_attr($wpgmza_ugm_form_header)."'></td>
    						</tr>
    						<tr>
                         		<td>".__('Marker Title', 'wp-google-maps')."</td>
                         		<td><input type='text' size='45' id='wpgmza_ugm_form_title' name='wpgmza_ugm_form_title' value='".esc_attr($wpgmza_ugm_form_title)."'></td>
    						</tr>
    						<tr>
                         		<td>".__('Marker Title (Placeholder)', 'wp-google-maps')."</td>
                         		<td><input type='text' size='45' id='wpgmza_ugm_form_title_ph' name='wpgmza_ugm_form_title_ph' value='".esc_attr($wpgmza_ugm_form_title_ph)."'></td>
    						</tr>
    						<tr>
                         		<td>".__('Marker Address or GPS Location', 'wp-google-maps')."</td>
                         		<td><input type='text' size='45' id='wpgmza_ugm_form_address' name='wpgmza_ugm_form_address' value='".esc_attr($wpgmza_ugm_form_address)."'></td>
    						</tr>
    						<tr>
                         		<td>".__('Marker Address (Placeholder)', 'wp-google-maps')."</td>
                         		<td><input type='text' size='45' id='wpgmza_ugm_form_address_ph' name='wpgmza_ugm_form_address_ph' value='".esc_attr($wpgmza_ugm_form_address_ph)."'></td>
    						</tr>
    						<tr>
                         		<td>".__('Or click on the map and drag to add a marker', 'wp-google-maps')."</td>
                         		<td><input type='text' size='45' id='wpgmza_ugm_form_address_help' name='wpgmza_ugm_form_address_help' value='".esc_attr($wpgmza_ugm_form_address_help)."'></td>
    						</tr>
    						<tr>
                         		<td>".__('Marker Description', 'wp-google-maps')."</td>
                         		<td><input type='text' size='45' id='wpgmza_ugm_form_desc' name='wpgmza_ugm_form_desc' value='".esc_attr($wpgmza_ugm_form_desc)."'></td>
    						</tr>
    						<tr>
                         		<td>".__('Marker Link', 'wp-google-maps')."</td>
                         		<td><input type='text' size='45' id='wpgmza_ugm_form_link' name='wpgmza_ugm_form_link' value='".esc_attr($wpgmza_ugm_form_link)."'></td>
    						</tr>
    						<tr>
                         		<td>".__('URL (Placeholder)', 'wp-google-maps')."</td>
                         		<td><input type='text' size='45' id='wpgmza_ugm_form_link_ph' name='wpgmza_ugm_form_link_ph' value='".esc_attr($wpgmza_ugm_form_link_ph)."'></td>
    						</tr>
    						<tr>
                         		<td>".__('Image', 'wp-google-maps')."</td>
                         		<td><input type='text' size='45' id='wpgmza_ugm_form_image' name='wpgmza_ugm_form_image' value='".esc_attr($wpgmza_ugm_form_image)."'></td>
    						</tr>
    						<tr>
                         		<td>".__('Marker Category', 'wp-google-maps')."</td>
                         		<td><input type='text' size='45' id='wpgmza_ugm_form_category' name='wpgmza_ugm_form_category' value='".esc_attr($wpgmza_ugm_form_category)."'></td>
    						</tr>
    					</table>
        ";
        $content .= "</div>";
    }
    return $content;  
}




function wpgmza_ugm_addon_display_mapspage() {
    /* deprecated in 3.00 */
    return;


}


add_action( "wpgooglemaps_hook_save_map" , "wpgmza_pro_vgm_filter_control_save_map", 10, 1 );
function wpgmza_pro_vgm_filter_control_save_map($map_id) {
   if (isset($_POST['wpgmza_save_ugm_settings'])){
	   
        global $wpdb;
        global $wpgmza_tblname_maps;

        $map_id = $_POST['wpgmza_map_id'];
        $ugm_enabled = esc_attr($_POST['wpgmza_ugm_enbaled']);
        $ugm_category_enabled = esc_attr($_POST['wpgmza_ugm_category_enbaled']);
        $ugm_access = esc_attr($_POST['wpgmza_ugm_access']);
        
        $res = wpgmza_get_map_data($map_id);
        $other_settings = maybe_unserialize($res->other_settings);
        $other_settings['wpgmza_ugm_upload_images'] = intval($_POST['wpgmza_ugm_upload_images']);
        $other_settings['wpgmza_ugm_link_enabled'] = sanitize_text_field( $_POST['wpgmza_ugm_link_enabled'] );
	    $other_settings['wpgmza_ugm_desc_enabled'] = sanitize_text_field( $_POST['wpgmza_ugm_desc_enabled'] );
	    $other_settings['wpgmza_ugm_form_header'] = sanitize_text_field( $_POST['wpgmza_ugm_form_header'] );
 	    $other_settings['wpgmza_ugm_form_title'] = sanitize_text_field( $_POST['wpgmza_ugm_form_title'] );
	    $other_settings['wpgmza_ugm_form_title_ph'] = sanitize_text_field( $_POST['wpgmza_ugm_form_title_ph'] );
	    $other_settings['wpgmza_ugm_form_address'] = sanitize_text_field( $_POST['wpgmza_ugm_form_address'] );
	    $other_settings['wpgmza_ugm_form_address_ph'] = sanitize_text_field( $_POST['wpgmza_ugm_form_address_ph'] );
	    $other_settings['wpgmza_ugm_form_address_help'] = sanitize_text_field( $_POST['wpgmza_ugm_form_address_help'] );
	    $other_settings['wpgmza_ugm_form_desc'] = sanitize_text_field( $_POST['wpgmza_ugm_form_desc'] );
	    $other_settings['wpgmza_ugm_form_link'] = sanitize_text_field( $_POST['wpgmza_ugm_form_link'] );
	    $other_settings['wpgmza_ugm_form_link_ph'] = sanitize_text_field( $_POST['wpgmza_ugm_form_link_ph'] );
	    $other_settings['wpgmza_ugm_form_image'] = sanitize_text_field( $_POST['wpgmza_ugm_form_image'] );
	    $other_settings['wpgmza_ugm_form_category'] = sanitize_text_field( $_POST['wpgmza_ugm_form_category'] );
		
		$other_settings['zoom_after_ugm_submission'] = $_POST['zoom_after_ugm_submission'];
		
	   $other_settings = maybe_serialize($other_settings);
        
        
        
        $rows_affected = $wpdb->query( $wpdb->prepare(
                "UPDATE $wpgmza_tblname_maps SET
                ugm_enabled = %d,
                ugm_category_enabled = %d,
                ugm_access = %d,
                other_settings = %s
                WHERE id = %d",

                $ugm_enabled,
                $ugm_category_enabled,
                $ugm_access,
                $other_settings,
                $map_id)
        );

   }

}
/**
 * Control the POST data from the VGM settings form
 * @return string
 * @deprecated from 3.00 - replace with wpgmza_pro_vgm_filter_control_save_map
 */
function wpgmaps_head_ugm() {
	
	if (!current_user_can('administrator')) {
	   return false;
	}
	
   if (isset($_POST['wpgmza_save_ugm_settings'])){

        global $wpdb;
        global $wpgmza_tblname_maps;

        $map_id = $_POST['wpgmza_map_id'];
        $ugm_enabled = esc_attr($_POST['wpgmza_ugm_enbaled']);
        $ugm_category_enabled = esc_attr($_POST['wpgmza_ugm_category_enbaled']);
        $ugm_access = esc_attr($_POST['wpgmza_ugm_access']);
        
        $res = wpgmza_get_map_data($map_id);
        $other_settings = maybe_unserialize($res->other_settings);
        $other_settings['wpgmza_ugm_upload_images'] = intval($_POST['wpgmza_ugm_upload_images']);
	    $other_settings['wpgmza_ugm_link_enabled'] = sanitize_text_field( $_POST['wpgmza_ugm_link_enabled'] );
	    $other_settings['wpgmza_ugm_desc_enabled'] = sanitize_text_field( $_POST['wpgmza_ugm_desc_enabled'] );
	    $other_settings['wpgmza_ugm_form_header'] = sanitize_text_field( $_POST['wpgmza_ugm_form_header'] );
	    $other_settings['wpgmza_ugm_form_title'] = sanitize_text_field( $_POST['wpgmza_ugm_form_title'] );
	    $other_settings['wpgmza_ugm_form_title_ph'] = sanitize_text_field( $_POST['wpgmza_ugm_form_title_ph'] );
	    $other_settings['wpgmza_ugm_form_address'] = sanitize_text_field( $_POST['wpgmza_ugm_form_address'] );
	    $other_settings['wpgmza_ugm_form_address_ph'] = sanitize_text_field( $_POST['wpgmza_ugm_form_address_ph'] );
	    $other_settings['wpgmza_ugm_form_address_help'] = sanitize_text_field( $_POST['wpgmza_ugm_form_address_help'] );
	    $other_settings['wpgmza_ugm_form_desc'] = sanitize_text_field( $_POST['wpgmza_ugm_form_desc'] );
	    $other_settings['wpgmza_ugm_form_link'] = sanitize_text_field( $_POST['wpgmza_ugm_form_link'] );
	    $other_settings['wpgmza_ugm_form_link_ph'] = sanitize_text_field( $_POST['wpgmza_ugm_form_link_ph'] );
	    $other_settings['wpgmza_ugm_form_image'] = sanitize_text_field( $_POST['wpgmza_ugm_form_image'] );
	    $other_settings['wpgmza_ugm_form_category'] = sanitize_text_field( $_POST['wpgmza_ugm_form_category'] );
		
        $other_settings = maybe_serialize($other_settings);
        
        
        
        $rows_affected = $wpdb->query( $wpdb->prepare(
                "UPDATE $wpgmza_tblname_maps SET
                ugm_enabled = %d,
                ugm_category_enabled = %d,
                ugm_access = %d,
                other_settings = %s
                WHERE id = %d",

                $ugm_enabled,
                $ugm_category_enabled,
                $ugm_access,
                $other_settings,
                $map_id)
        );

    echo "
    <div class='updated'>
        Your User Generated Marker Settings have been saved.
    </div>
    ";
   }
}


/**
 * Generate the VGM HTML form for the front end
 * @param  intval  $mapid           The MAP ID that this form links to
 * @param  boolean $redirect_string If this is set, the form will redirect to this page once the marker has been inserted
 * @param  boolean $from_shortcode  If this is set to true (only when called from the short code), then certain elements will change
 * @return string                   The HTML form
 */
function wpgmaps_ugm_user_form( $mapid, $redirect_string = false, $from_shortcode = false ) {
    global $wpdb;
    global $wpgmza_tblname_maps;
    global $current_user;
	global $wpgmza;
	global $wpgmzaGDPRCompliance;
    
	$ugm_display = false;
    $mapid = intval( $mapid );
	
    $results = $wpdb->get_results("
        SELECT *
        FROM $wpgmza_tblname_maps
        WHERE `id` = '$mapid' LIMIT 1
    ");
	
	if($redirect_string)
		$redirect_to = esc_html($redirect_string);
	else
		$redirect_to = $_SERVER['REQUEST_URI'];
	
    if (!$redirect_string)
        $redirect_string_html = '';
    else
        $redirect_string_html = '<input type="hidden" name="redirect_to" value="'.esc_html( $redirect_string ).'" />';

	if($wpgmza && $wpgmza->settings->engine == 'open-layers'){
		wp_enqueue_script('jquery-ui');
		wp_enqueue_script('jquery-ui-draggable');
	}

    foreach ( $results as $result ) {
        
        $other_settings = maybe_unserialize($result->other_settings);
        if (isset($other_settings['wpgmza_ugm_upload_images'])) { $upload_images = $other_settings['wpgmza_ugm_upload_images']; } else { $upload_images = false; }
        if (isset($other_settings['wpgmza_ugm_link_enabled'])) { $link_enabled = $other_settings['wpgmza_ugm_link_enabled']; } else { $link_enabled = false; }
	    if (isset($other_settings['wpgmza_ugm_desc_enabled'])) { $desc_enabled = $other_settings['wpgmza_ugm_desc_enabled']; } else { $desc_enabled = 1; }
        $wpgmza_ugm_form_header = !empty($other_settings['wpgmza_ugm_form_header']) ? stripslashes($other_settings['wpgmza_ugm_form_header']) : __('Add your own marker', 'wp-google-maps');
	    $wpgmza_ugm_form_title = !empty($other_settings['wpgmza_ugm_form_title']) ? stripslashes($other_settings['wpgmza_ugm_form_title']) : __('Marker Title', 'wp-google-maps');
	    $wpgmza_ugm_form_title_ph = !empty($other_settings['wpgmza_ugm_form_title_ph']) ? stripslashes($other_settings['wpgmza_ugm_form_title_ph']) : __('Marker Title', 'wp-google-maps');
	    $wpgmza_ugm_form_address = !empty($other_settings['wpgmza_ugm_form_address']) ? stripslashes($other_settings['wpgmza_ugm_form_address']) : __('Marker Address or GPS Location', 'wp-google-maps');
	    $wpgmza_ugm_form_address_ph = !empty($other_settings['wpgmza_ugm_form_address_ph']) ? stripslashes($other_settings['wpgmza_ugm_form_address_ph']) : __('Marker Address', 'wp-google-maps');
	    $wpgmza_ugm_form_address_help = !empty($other_settings['wpgmza_ugm_form_address_help']) ? stripslashes($other_settings['wpgmza_ugm_form_address_help']) : __('Or right-click on the map and drag to add a marker', 'wp-google-maps');
	    $wpgmza_ugm_form_desc = !empty($other_settings['wpgmza_ugm_form_desc']) ? stripslashes($other_settings['wpgmza_ugm_form_desc']) : __('Marker Description', 'wp-google-maps');
	    $wpgmza_ugm_form_link = !empty($other_settings['wpgmza_ugm_form_link']) ? stripslashes($other_settings['wpgmza_ugm_form_link']) : __('Marker Link', 'wp-google-maps');
	    $wpgmza_ugm_form_link_ph = !empty($other_settings['wpgmza_ugm_form_link_ph']) ? stripslashes($other_settings['wpgmza_ugm_form_link_ph']) : __('URL', 'wp-google-maps');
	    $wpgmza_ugm_form_image = !empty($other_settings['wpgmza_ugm_form_image']) ? stripslashes($other_settings['wpgmza_ugm_form_image']) : __('Image', 'wp-google-maps');
	    $wpgmza_ugm_form_category = !empty($other_settings['wpgmza_ugm_form_category']) ? stripslashes($other_settings['wpgmza_ugm_form_category']) : __('Marker Category', 'wp-google-maps');

        $ugm_access = $result->ugm_access;
        if (isset($ugm_access) && $ugm_access == 2) { // only registered users can add markers
            if ( is_user_logged_in() ) {
            
                $userRole = ($current_user->roles);
                switch($userRole) {
                    
                    case ('administrator'||'editor'||'contributor'||'author'||'subscriber'):
                            $ugm_display = true;
                    break;
                    default:
                    break;
                }
            } else {
                $ugm_display = false;
            }
        } else {
            $ugm_display = true;
        }
    }
    if ($ugm_display) {
		
		$wpgmza_general_settings = $wpgmza->settings;
        //$wpgmza_general_settings = get_option('WPGMZA_OTHER_SETTINGS');
		
        $wpgmza_autoapprove_checked = '0';
        if (isset($wpgmza_general_settings['wpgmza_settings_ugm_autoapprove'])) { $wpgmza_settings_ugm_autoapprove = $wpgmza_general_settings['wpgmza_settings_ugm_autoapprove']; } else { $wpgmza_settings_ugm_autoapprove = "yes"; }
        if ($wpgmza_settings_ugm_autoapprove == "yes") { $wpgmza_autoapprove_checked = "0"; } else { $wpgmza_autoapprove_checked = "1"; }
        

        $r_msg = "";

        $r_msg .= "<h2 style=\"clear:both;\">".esc_html($wpgmza_ugm_form_header)."</h2>";
        $r_msg .= " <form action='" . get_admin_url() . "admin-post.php' method=\"POST\"  name=\"wpgmaps_ugm\" class=\"wpgmaps_user_form\" enctype=\"multipart/form-data\">";
		
		$r_msg .= "
						<input type='hidden' name='action' value='wpgmza_ugm_process_post'/>
						<input type='hidden' name='redirect_to' value='$redirect_to'/>
		";
		
        $r_msg .= "     <input type=\"hidden\" name=\"wpgmza_ugm_map_id\" id=\"wpgmza_ugm_map_id\" value=\"$mapid\" />";
        $r_msg .= "     <input type=\"hidden\" name=\"wpgmza_ugm_lat\" id=\"wpgmza_ugm_lat\" value=\"\" />";
        $r_msg .= "     <input type=\"hidden\" name=\"wpgmza_ugm_lng\" id=\"wpgmza_ugm_lng\" value=\"\" />";
        $r_msg .= "     <input type=\"hidden\" name=\"wpgmza_approval\" id=\"wpgmza_approval\" value=\"$wpgmza_autoapprove_checked\" />";
        $r_msg .= "     <table class='wpgmza_table' style='border:0 !important; width:100%;'>";
        $r_msg .= "         <tr class='wpgmza-title'>";
        $r_msg .= "             <td valign=\"top\">".esc_html($wpgmza_ugm_form_title)."</td>";
        $r_msg .= "             <td><input id='wpgmza_ugm_add_title' name='wpgmza_ugm_add_title' type='text' size='35' maxlength='200' value='' placeholder='" . esc_attr($wpgmza_ugm_form_title_ph) . "' /></td>";
        $r_msg .= "         </tr>";
        $r_msg .= "         <tr class='wpgmza-address'>";
        $r_msg .= "             <td valign=\"top\">".esc_html($wpgmza_ugm_form_address)."</td>";
        $r_msg .= "             <td>";
        $r_msg .= "                 <input id='wpgmza_ugm_add_address_".$mapid."' name='wpgmza_ugm_add_address' type='text' size='35' maxlength='200' value='' placeholder='".esc_attr($wpgmza_ugm_form_address_ph)."' />";
        if (!$from_shortcode)
            $r_msg .= "                 <p class='desc'>".esc_html($wpgmza_ugm_form_address_help)."</p>";

        $r_msg .= "             </td>";
	    $r_msg .= "         </tr>";
        if ($desc_enabled == 1) {
            $r_msg .= "         <tr class='wpgmza-description'>";
            $r_msg .= "             <td valign=\"top\">".esc_html($wpgmza_ugm_form_desc)."</td>";
            $r_msg .= "             <td><textarea id='wpgmza_ugm_add_desc' name='wpgmza_ugm_add_desc'  rows='3' cols='37'></textarea> </td>";
            $r_msg .= "         </tr>";
        }
        if ($link_enabled == 1) {
	        $r_msg .= "         <tr class='wpgmza-link'>";
	        $r_msg .= "             <td valign=\"top\">".esc_html($wpgmza_ugm_form_link)."</td>";
	        $r_msg .= "             <td><input id='wpgmza_ugm_add_link' name='wpgmza_ugm_add_link' type='url' size='35' maxlength='2083' value='' placeholder='".esc_attr($wpgmza_ugm_form_link_ph)."' /> </td>";
	        $r_msg .= "         </tr>";
        }
        if ($upload_images == 1) {
        $r_msg .= "         <tr class='wpgmza-image'>";
        $r_msg .= "             <td valign=\"top\">".esc_html($wpgmza_ugm_form_image)."</td>";
        $r_msg .= "             <td><input type='file' id='wpgmza_ugm_add_file' name='wpgmza_ugm_add_file' /></td>";
        $r_msg .= "         </tr>";
        }
        if ($result->ugm_category_enabled == 2) { } else { 
             
             if (isset($wpgmza_general_settings['wpgmza_settings_filterbycat_type'])) { $filterbycat_type = $wpgmza_general_settings['wpgmza_settings_filterbycat_type']; } else { $filterbycat_type = false; }
             if (!$filterbycat_type) { $filterbycat_type = 1; }
             
            
            $r_msg .= "         <tr class='wpgmza-categories'>";
            $r_msg .= "             <td valign=\"top\">".esc_html($wpgmza_ugm_form_category)."</td>";
			
			$r_msg .= "<td>";
			
			if(class_exists('WPGMZA\\Map') && class_exists('WPGMZA\\CategoryFilterWidget'))
			{
				$map	= \WPGMZA\Map::createInstance($mapid);
				
				if($map->categoryFilterWidget instanceof \WPGMZA\CategoryFilterWidget\Checkboxes)
				{
					$inputs = $map->categoryFilterWidget->document->querySelectorAll("input.wpgmza_checkbox");
					
					foreach($inputs as $el)
						$el->setAttribute('name', 'wpgmza_cat_checkbox[]');
				}
				
				$html	= $map->categoryFilterWidget->html;
				
				$html	= preg_replace('/wpgmza_filter_select/', 'wpgmza_category', $html);
                $html   = preg_replace('/name="wpgmza_cat_checkbox"/', 'name="wpgmza_cat_checkbox[]"', $html);

				
				$r_msg	.= $html;
			}
			else
			{
				if($filterbycat_type == "1")
				{
					$r_msg .= "<select name=\"wpgmza_category\" id=\"wpgmza_category\">".wpgmza_pro_return_category_select_list($mapid)."</select>";
				}
				else
				{
					$r_msg .= wpgmza_pro_return_category_checkbox_list($mapid,false,true);
				}
			}
			
			$r_msg .= "</td>";
			
            $r_msg .= "         </tr>";
        }
		
		if(class_exists('WPGMZA\\CustomMarkerFields'))
			$r_msg .= WPGMZA\CustomMarkerFields::adminHtml(true);

		if(class_exists('WPGMZA\\GDPRCompliance') && !empty($wpgmza_general_settings['wpgmza_gdpr_require_consent_before_vgm_submit']))
		{
			$module = (isset($wpgmza->gdprCompliance) ? $wpgmza->gdprCompliance : $wpgmzaGDPRCompliance);
			
			if(!$module)
				$module = new WPGMZA\GDPRCompliance();
			
			$html = $module->getNoticeHTML();
			
			$r_msg .= '<tr>';
			$r_msg .= '<td>' . __('GDPR Agreement', 'wp-google-maps') . '</td>';
			$r_msg .= '<td>' . $html . '</td>';
			$r_msg .= '</tr>';
		}
		
        $r_msg .= "         <tr>";
        $r_msg .= "             <td valign=\"top\"></td>";
        $r_msg .= "             <td><input id='wpgmza_ugm_spm' name='wpgmza_ugm_spm' type='checkbox' value='1'  /> ".__("Please tick this box to prove you are human","wp-google-maps")."</td>";
        $r_msg .= "         </tr>";
        $r_msg .= "         <tr>";
        $r_msg .= "             <td valign=\"top\"></td>";
        $r_msg .= "             <td><input type=\"button\" id=\"wpgmza_ugm_addmarker\" name=\"wpgmza_ugm_addmarker\" value=\"".__("Add marker","wp-google-maps")."\" /><span id=\"wpgmza_ugm_addmarker_loading\"  style=\"display:none;\">".__("Adding","wp-google-maps")."...</span></td>";
        $r_msg .= "         </tr>";
        $r_msg .= "     </table>";
        $r_msg .= " ";
        $r_msg .= " ";
        $r_msg .= " ";
        $r_msg .= " ";
        $r_msg .= "</form>";

		global $wpgmza;
		
		$wpgmza->loadScripts(true);

        global $wpgmza_ugm_version;
        
		//wp_enqueue_script('wpgmaps_ugm_core', plugins_url('/js/ugm-core.js', __FILE__ ), array(), $wpgmza_ugm_version.'vgm' , false
		
		wp_enqueue_script('wpgmaps_ugm_core', plugin_dir_url(__FILE__) . 'js/ugm-core.js');

        wp_register_style( 'wpgmaps-vgm-style', plugins_url('css/wpgmza_vgm.css', __FILE__), array(), $wpgmza_ugm_version.'vgm', false );
        wp_enqueue_style( 'wpgmaps-vgm-style' );


        if(!empty($wpgmza->settings->wpgmza_vgm_form_style_modern)){
            wp_register_style( 'wpgmaps-vgm-style-modern', plugins_url('css/wpgmza_vgm_modern.css', __FILE__), array(), $wpgmza_ugm_version.'vgm', false );
            wp_enqueue_style( 'wpgmaps-vgm-style-modern' );
        }


        $vgm_human_error_string = __("Please prove that you are human by checking the checkbox above","wp-google-maps");
        $ajax_nonce_ugm = wp_create_nonce("wpgmza_ugm");
        
        /*
         * Restructured in 3.32
         *
         * Reason: From WP Core 5.7 arrays are required for localization
        */

        /*
        wp_localize_script( 'wpgmaps_ugm_core', 'vgm_human_error_string', $vgm_human_error_string);
        wp_localize_script( 'wpgmaps_ugm_core', 'wpgmaps_nonce', $ajax_nonce_ugm);
        if ($from_shortcode) { $from_shortc = "1"; } else { $from_shortc = "0"; }
        wp_localize_script( 'wpgmaps_ugm_core', 'vgm_from_shortcode', $from_shortc);
        */

        if ($from_shortcode) { $from_shortc = "1"; } else { $from_shortc = "0"; }


        $scriptVariables = array(
            'vgm_human_error_string' => $vgm_human_error_string,
            'wpgmaps_nonce' => $ajax_nonce_ugm,
            'vgm_from_shortcode' => $from_shortc 
        );

        /*
         * We need to find a better way, for now it's a short term patch (2021-06-01)
        */
        wp_localize_script( 'wpgmaps_ugm_core', 'wpgmza_vgm_localized', $scriptVariables);
		
        return apply_filters('wpgmza_ugm_form_raw_html', $r_msg);
    }

}

add_action('init', 'wpgmza_ugm_process_post', 1);

function wpgmza_ugm_process_post()
{
	global $wpdb;
	global $wpgmza_tblname;
	global $wpgmza;

	if(!isset($_POST['wpgmza_ugm_spm']))
		return;
	
	$table_name = $wpdb->prefix . "wpgmza";
	// wpgmaps_filter($_POST);
                
	$wpgmza_settings = $wpgmza->settings;
	
	$desc = (empty($_POST['wpgmza_ugm_add_desc']) ? "" : $_POST['wpgmza_ugm_add_desc']);
	$title = (empty($_POST['wpgmza_ugm_add_title']) ? "" : $_POST['wpgmza_ugm_add_title']);
	
	if($wpgmza->settings->wpgmza_settings_ugm_striptags == "yes")
	{
		$desc = strip_tags($desc);
		$title = strip_tags($title);
	}
	
	if ($wpgmza->settings->wpgmza_settings_ugm_autoapprove == "yes")
	{
		$approved_setting = 1;
	}  
	else
	{
		$approved_setting = 0;
	}
	
	if(isset($_FILES['wpgmza_ugm_add_file']) && $_FILES['wpgmza_ugm_add_file'] != '0')
	{
		if(!function_exists( 'wp_handle_upload' ))
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
		
		$uploadedfile = $_FILES['wpgmza_ugm_add_file'];
		
		$upload_overrides = array(
			'test_form' => false
		);
		
		$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
		
		if ( $movefile && isset($movefile['error']) )
			$movefile['url'] = "";
	}
	else
		$movefile['url'] = "";

	if (isset($_POST['wpgmza_cat_checkbox']))
	{
		if(is_array($_POST['wpgmza_cat_checkbox']))
			$cat = implode(",",$_POST['wpgmza_cat_checkbox']);
		else
			$cat = $_POST['wpgmza_cat_checkbox'];
	}
	else if (isset($_POST['wpgmza_category']))
		$cat = $_POST['wpgmza_category'];
	else
		$cat = 0;

	$link = '';
	if (isset($_POST['wpgmza_ugm_add_link']))
	{
		$link = sanitize_text_field($_POST['wpgmza_ugm_add_link']);
		
		if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$link))
			$link = '';
	}

	/* identify LAT and LNG (do we already have it or must we geocode it via PHP?) */
	if (isset($_POST['wpgmza_ugm_lat']) && isset($_POST['wpgmza_ugm_lng']) && $_POST['wpgmza_ugm_lat'] != '' && $_POST['wpgmza_ugm_lng'] != '')
	{
		$wpgm_lat = sanitize_text_field( $_POST['wpgmza_ugm_lat'] );
		$wpgm_lng = sanitize_text_field( $_POST['wpgmza_ugm_lng'] );
	}
	else
	{
		$latlng = wpgmaps_curl_geocode( sanitize_text_field( $_POST['wpgmza_ugm_add_address'] ) );
		$wpgm_lat = $latlng['lat'];
		$wpgm_lng = $latlng['lng'];
	}
	  
	$map_id = intval( sanitize_text_field( $_POST['wpgmza_ugm_map_id'] ) );
	
	$ins_array = array( 
		'map_id' => $map_id,
		'title' => $title, 
		'address' => sanitize_text_field( $_POST['wpgmza_ugm_add_address'] ), 
		'description' => $desc, 
		'lat' => $wpgm_lat, 
		'lng' => $wpgm_lng, 
		'infoopen' => '', 
		'anim' => '', 
		'link' => $link, 
		'pic' => $movefile['url'], 
		'category' => $cat,
		'approved' => $approved_setting
	);


	$rows_affected = $wpdb->insert( $table_name, $ins_array );

	$lastid = $wpdb->insert_id;
	
	if(class_exists('WPGMZA\\ProMarker'))
		$marker = WPGMZA\Marker::createInstance($lastid);
	
	if(!empty($marker) && isset($_POST['wpgmza_cat_checkbox']) && is_array($_POST['wpgmza_cat_checkbox']))
		$marker->categories = $_POST['wpgmza_cat_checkbox'];
	
	// Categories
	if(class_exists('WPGMZA\\Category'))
	{
		WPGMZA\Category::rebuildTableFromLegacyField(array(
			'marker' => (object)array(
				'id' => $lastid
			)
		));
	}
		
	// Custom fields
	if(class_exists('WPGMZA\\CustomMarkerFields'))
	{
		// $custom_fields = new WPGMZA\CustomMarkerFields($lastid);
		
		foreach($_POST as $key => $value)
		{
			$m = null;
			
			if(!preg_match('/^(wpgmza-custom-field-|custom_field_)(\d+)$/', $key, $m))
				continue;
			
			$field_id = (int)$m[2];
			// $custom_fields->{$field_id} = $value;
			
			$marker->customFields->{$field_id} = $value;
		}
	}

	/* check if we need to email someone about this marker */
	if (isset($wpgmza_settings['wpgmza_settings_ugm_email_new_marker'])) { $wpgmza_settings_ugm_email_new_marker = $wpgmza_settings['wpgmza_settings_ugm_email_new_marker']; } else { $wpgmza_settings_ugm_email_new_marker = ""; }
	if (isset($wpgmza_settings['wpgmza_settings_ugm_email_address'])) { $wpgmza_settings_ugm_email_address = $wpgmza_settings['wpgmza_settings_ugm_email_address']; } else { $wpgmza_settings_ugm_email_address = get_option( 'admin_email' ); }

	if ($wpgmza_settings_ugm_email_new_marker == "yes") {
		/* getm ap data so we can reference it in the email */
		$res = wpgmza_get_map_data($_POST['wpgmza_ugm_map_id']);
		$map_title = $res->map_title;
		$map_id = $res->id;
		$marker_title = $title;
		$marker_address = $_POST['wpgmza_ugm_add_address'];
		$marker_desc = $desc;
		$marker_category = $cat;

		$marker_link = get_admin_url()."admin.php?page=wp-google-maps-menu&action=edit&map_id=".$map_id;

		$to = $wpgmza_settings_ugm_email_address;
		$subject =	sprintf( __("A new marker has been submitted for your map (%s)", "wp-google-maps"), $map_title );
		$body =		__('A new marker has been submitted for your map', 'wp-google-maps') . "\n\n";
		$body .=	sprintf( __("Map: %s", 'wp-google-maps'), $map_title ) . "\n";
		$body .=	sprintf( __("Marker title: %s", 'wp-google-maps'), $marker_title ) . "\n";
		$body .=	sprintf( __("Marker address: %s", 'wp-google-maps'), $marker_address ) . "\n";
		$body .=	sprintf( __("Marker description: %s",'wp-google-maps'), $marker_desc ) . "\n";
		$body .=	sprintf( __("Marker category: %s",'wp-google-maps'), $marker_category ) . "\n\n";
		
		if ($approved_setting == 1) { 
			$body .= sprintf( __("To edit or view the marker, please click the following link: %s", 'wp-google-maps'), $marker_link ) . "\n";
		} else { 
			$body .= sprintf( __("To approve, edit or delete the marker, please click the following link: %s", 'wp-google-maps'), $marker_link ) . "\n";
		}
		
		$headers[] = 'From: '.get_option('blogname').' <'.$wpgmza_settings_ugm_email_address.'>';
		
		$fields = array(
			'to'		=> $to,
			'subject'	=> $subject,
			'body'		=> $body,
			'headers'	=> $headers
		);
		
		$fields = apply_filters('wpgmza_ugm_submission_email_fields', $fields, $marker);
		
		$to			= $fields['to'];
		$subject	= $fields['subject'];
		$body		= $fields['body'];
		$headers	= $fields['headers'];
		   
		@wp_mail( $to, $subject, $body, $headers );
	}

	wpgmaps_update_xml_file($_POST['wpgmza_ugm_map_id']);
	
	do_action('wpgmza_ugm_marker_submitted', $marker);
	
	$cookie = array(
		'marker' => $marker
	);
	
	if($approved_setting == 0)
		$cookie['message'] = __( "Your marker is pending approval.", "wp-google-maps" );
	else
		$cookie['message'] = __( "Marker successfully submitted.", "wp-google-maps" );

	$string = json_encode($cookie);
	$value = base64_encode($string);

	setcookie('wpgmza_ugm_submission_result', $value, time() + 600, '/');

	// Redirect to this page.
	wp_safe_redirect($_POST['redirect_to']);
}


function wpgmaps_curl_geocode( $address ) {
     // check if cURL is available
      
    if( get_option( 'wpgmza_google_maps_api_key' ) ){
        $googlekey = get_option( 'wpgmza_google_maps_api_key' );
        
        if (!$googlekey) { 
            return false;
        } else {

        $address = utf8_encode( urlencode( stripslashes( $address ) ) );

        $url = "https://maps.googleapis.com/maps/api/geocode/json?sensor=true&key=". $googlekey ."&address=" . $address;


        // get the json response
        $resp_json = file_get_contents($url);
         
        // decode the json
        $resp = json_decode($resp_json, true);


        // probably should do some error checking here
        if ($resp !== null) {
            if ($resp['status'] != "OK") { 
                /* it failed */
                return false;
            }
        } else {
            return false;
        }
        $lat = $resp['results'][0]['geometry']['location']['lat'];
        $lng = $resp['results'][0]['geometry']['location']['lng'];
       

        return array('lat' => $lat, 'lng' => $lng);
        }
    }

       
}

add_filter( "wpgooglemaps_filter_map_div_output", "wpgooglemaps_filter_map_div_output_vgm_marker_added", 10, 2 );
function wpgooglemaps_filter_map_div_output_vgm_marker_added( $content, $map_id = false ) {
    global $wpgmza_ugm_marker_added_string;

    if ( strlen( $wpgmza_ugm_marker_added_string ) > 0 ) {



        return $wpgmza_ugm_marker_added_string.PHP_EOL.$content;

    } else {
        return $content;
    }
}

/**
 * This changes the global setting variable should the user add "new_window_link='1'" to the short code
 */
add_action( "wpgooglemaps_hook_user_js_after_localize","wpgooglemaps_ugm_hook_user_js_after_localize", 10, 1);
function wpgooglemaps_ugm_hook_user_js_after_localize( $res ) {
    global $wpgmza_override;
    if (isset($wpgmza_override['ugm_marker'])) {
        wp_localize_script( 'wpgmaps_core', 'wpgmaps_localize_focus_marker', $wpgmza_override['ugm_marker'] );
        
    }
    return;
}



$wpgmaps_ugm_api_url = 'http://ccplugins.co/apid-wpgmaps/';
$wpgmaps_ugm_plugin_slug = basename(dirname(__FILE__));


add_filter('pre_set_site_transient_update_plugins', 'wpgmaps_ugm_check_for_plugin_update');

function wpgmaps_ugm_check_for_plugin_update($checked_data) {
	global $wpgmaps_ugm_api_url, $wpgmaps_ugm_plugin_slug, $wp_version, $wpgmza_ugm_version;
	
	//Comment out these two lines during testing.
	if (empty($checked_data->checked))
		return $checked_data;
        
	$args = array(
		'slug' => $wpgmaps_ugm_plugin_slug,
		'version' => $wpgmza_ugm_version,
	);
	$request_string = array(
			'body' => array(
				'action' => 'basic_check', 
				'request' => serialize($args),
				'api-key' => md5(get_bloginfo('url'))
			),
			'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo('url')
		);
	
	// Start checking for an update
	$raw_response = wp_remote_post($wpgmaps_ugm_api_url, $request_string);
        
    $response = false;
	if (!is_wp_error($raw_response) && ($raw_response['response']['code'] == 200))
		@$response = unserialize($raw_response['body']);
	
	if (is_object($response) && !empty($response)) // Feed the update data into WP updater
		$checked_data->response[basename( __DIR__ ) . '/' . $wpgmaps_ugm_plugin_slug . '.php'] = $response;
	
	return $checked_data;
}



add_filter('plugins_api', 'wpgmaps_ugm_plugin_api_call', 10, 3);

function wpgmaps_ugm_plugin_api_call($def, $action, $args) {
	global $wpgmaps_ugm_plugin_slug, $wpgmaps_ugm_api_url, $wp_version, $wpgmza_ugm_version;
	
	if (!isset($args->slug) || ($args->slug != $wpgmaps_ugm_plugin_slug))
		return false;
	
	// Get the current version
	$args->version = $wpgmza_ugm_version;
	
	$request_string = array(
			'body' => array(
				'action' => $action, 
				'request' => serialize($args),
				'api-key' => md5(get_bloginfo('url'))
			),
			'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo('url')
		);
	
	$request = wp_remote_post($wpgmaps_ugm_api_url, $request_string);
	
	if (is_wp_error($request)) {
		$res = new WP_Error('plugins_api_failed', __('An Unexpected HTTP Error occurred during the API request.</p> <p><a href="?" onclick="document.location.reload(); return false;">Try again</a>'), $request->get_error_message());
	} else {
		$res = unserialize($request['body']);
		
		if ($res === false)
			$res = new WP_Error('plugins_api_failed', __('An unknown error occurred'), $request['body']);
	}
	
	return $res;
}






function wpgmaps_ugm_user_javascript() {
    $ajax_nonce_ugm = wp_create_nonce("wpgmza_ugm");
    echo "var wpgmaps_nonce = '$ajax_nonce_ugm';";
    
}

add_filter("wpgmza_global_settings_tabs", "wpgmza_vgm_settings_page_tab", 10, 1);
function wpgmza_vgm_settings_page_tab($content){
    $content .= "<li><a href='#tabs-vgm'>".__("VGM","wp-google-maps")."</a></li>";
    return $content;
}

add_filter("wpgmza_global_settings_tab_content", "wpgmaps_ugm_settings_page_tab_content", 10, 1);
function wpgmaps_ugm_settings_page_tab_content($content){
    $content .= "<div id='tabs-vgm'>" . wpgmaps_ugm_settings_page() . "</div>";
    return $content;
}

function wpgmaps_ugm_settings_page() {
	
	global $wpgmza;
	
    //$wpgmza_settings = get_option("WPGMZA_OTHER_SETTINGS");
	$wpgmza_settings = $wpgmza->settings;
	
    if (isset($wpgmza_settings['wpgmza_settings_ugm_striptags'])) { $wpgmza_settings_ugm_striptags = $wpgmza_settings['wpgmza_settings_ugm_striptags']; } else { $wpgmza_settings_ugm_striptags = ""; }
    if (isset($wpgmza_settings['wpgmza_settings_ugm_autoapprove'])) { $wpgmza_settings_ugm_autoapprove = $wpgmza_settings['wpgmza_settings_ugm_autoapprove']; } else { $wpgmza_settings_ugm_autoapprove = "yes"; }
    if (isset($wpgmza_settings['wpgmza_settings_ugm_email_new_marker'])) { $wpgmza_settings_ugm_email_new_marker = $wpgmza_settings['wpgmza_settings_ugm_email_new_marker']; } else { $wpgmza_settings_ugm_email_new_marker = ""; }
    if (isset($wpgmza_settings['wpgmza_settings_ugm_email_address'])) { $wpgmza_settings_ugm_email_address = $wpgmza_settings['wpgmza_settings_ugm_email_address']; } else { $wpgmza_settings_ugm_email_address = get_option( 'admin_email' ); }
    if ($wpgmza_settings_ugm_striptags == "yes") { $wpgmza_striptags_checked = "checked='checked'"; } else { $wpgmza_striptags_checked = ""; }
    if ($wpgmza_settings_ugm_autoapprove == "yes") { $wpgmza_autoapprove_checked = "checked='checked'"; } else { $wpgmza_autoapprove_checked = ""; }
    if ($wpgmza_settings_ugm_email_new_marker == "yes") { $wpgmza_settings_ugm_email_new_marker_checked = "checked='checked'"; } else { $wpgmza_settings_ugm_email_new_marker_checked = ""; }

    
    $version_warning = "";
    
    global $wpgmza_pro_version;
    if (floatval($wpgmza_pro_version) < 5.50) {
        $version_warning = __("Please <a href='./update-core.php'>update</a> your Pro Add-on to at least version 5.50 to allow for these settings","wp-google-maps");
    }

        return "
            <h3>".__("Visitor Generated Marker Settings","wp-google-maps")."</h3>
                <p style='font-weight:bold; color:red;'>$version_warning</p>
                <table class='form-table'>
                    <tr>
                         <td width='200' valign='top'>".__("Visitor Marker Input Settings","wp-google-maps").":</td>
                         <td>
                                <input name='wpgmza_settings_map_striptags' type='checkbox' id='wpgmza_settings_map_striptags' value='yes' $wpgmza_striptags_checked /> ".__("Strip all HTML tags in descriptions and titles","wp-google-maps")."<br />
                                <input name='wpgmza_settings_ugm_autoapprove' type='checkbox' id='wpgmza_settings_ugm_autoapprove' value='yes' $wpgmza_autoapprove_checked /> ".__("Automatically approve visitor generated markers","wp-google-maps")."<br />
                                <input name='wpgmza_settings_ugm_email_new_marker' type='checkbox' id='wpgmza_settings_ugm_email_new_marker' value='yes' $wpgmza_settings_ugm_email_new_marker_checked /> ".__("Email me whenever there is a new marker","wp-google-maps")." &nbsp; | &nbsp;".__("Email address:","wp-google-maps")." <input name='wpgmza_settings_ugm_email_address' type='text' id='wpgmza_settings_ugm_email_address' value='$wpgmza_settings_ugm_email_address' /> <br />
                        </td>
                    </tr>

                    <tr>
                        <td width='200' valign='top'>".__("Form Style","wp-google-maps").":</td>
                        <td>
                            <input name='wpgmza_vgm_form_style_modern' id='wpgmza_vgm_form_style_modern' type='checkbox' /> " . __("Modern Form Layout (beta)", "wp-google-maps") . "
                        </td>
                    </tr>

                    <tr>
                        <td width='200' valign='top'>".__("GDPR Compliance","wp-google-maps").":</td>
                        <td>
                            <input name='wpgmza_gdpr_require_consent_before_vgm_submit' id='wpgmza_gdpr_require_consent_before_vgm_submit' type='checkbox' /> " . __("Require consent before user submission", "wp-google-maps") . "
                        </td>
                    </tr>
                </table>
            ";



}

add_filter("wpgmza_filter_localize_settings","wpgmza_filter_vgm_control_localize_settings",10,1);
/**
 * Added to remove the admin email address that we save to the standard wpgmza_settings option
 * @param   array $wpgmza_settings
 * @return  array
 * @version 2.8
 * 
 */
function wpgmza_filter_vgm_control_localize_settings($wpgmza_settings) {
    if (isset($wpgmza_settings['wpgmza_settings_ugm_email_address'])) {
        $wpgmza_settings['wpgmza_settings_ugm_email_address'] = "";
    }
    return $wpgmza_settings;
}

add_filter('wpgmza_gdpr_settings_tab_content', function($document) {
	
	$container = $document->querySelector('#wpgmza-gdpr-compliance');
	
	$container->import('<div id="wpgmza-gdpr-vgm-compliance">
	
		<h2>
			' . __('VGM Add-on Compliance', 'wp-google-maps') . '
		</h2>
		
		<fieldset>
			<label for="wpgmza_gdpr_require_consent_before_vgm_submit">
				' . __('Require consent before user submission', 'wp-google-maps') . '
				<i class="fa fa-question-circle" 
					title="' . __('If you collect personally identifying information through your Visitor Generated Markers form, you should check this box to require the user to consent to the GDPR notice before submitting their marker.', 'wp-google-maps') . '"/>
			</label>
			<input name="wpgmza_gdpr_require_consent_before_vgm_submit" type="checkbox"/>
		</fieldset>
		
	</div>');
	
	
	
	$node = $document->querySelector("#wpgmza-gdpr-vgm-compliance");
	$after = $document->querySelector("#wpgmza-gpdr-general-compliance");
	$container->insertAfter($node, $after);
	
	return $document;
	
});

add_filter('wpgmza_admin_marker_datatable_action_buttons_sql', function($sql) {
	
	$id_placeholder = \WPGMZA\AdminMarkerDataTable::ID_PLACEHOLDER;
	
	$regexSuffix = '/AS\s+`[\w_]+`\s*$/i';
	
	if(preg_match($regexSuffix, $sql, $m))
		$suffix = $m[0];
	
	$sql = preg_replace($regexSuffix, '', $sql);
	
	return "CONCAT('', $sql) $suffix";
	
});

add_filter("wpgmza_global_settings_save_redux", "wpgmza_vgm_81_settings_filter", 10, 1);
function wpgmza_vgm_81_settings_filter($data){
    $toggleControls = array(
        'wpgmza_settings_map_striptags',
        'wpgmza_settings_ugm_autoapprove',
        'wpgmza_settings_ugm_email_new_marker',
    );

    foreach ($toggleControls as $propKey) {
        if(empty($_POST[$propKey])){
            $data[$propKey] = false; 
        }
    }

    return $data;

}